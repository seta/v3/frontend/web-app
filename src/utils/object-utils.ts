/**
 * Checks if two objects are deeply equal.
 *
 * @param a - The first object to compare.
 * @param b - The second object to compare.
 * @returns `true` if the objects are deeply equal, `false` otherwise.
 */
export const deepEqual = (a: unknown, b: unknown): boolean => {
  if (a === b) {
    return true
  }

  if (a === null || b === null || typeof a !== 'object' || typeof b !== 'object') {
    return false
  }

  const keysA = Object.keys(a)
  const keysB = Object.keys(b)

  if (keysA.length !== keysB.length) {
    return false
  }

  for (const key of keysA) {
    if (!keysB.includes(key)) {
      return false
    }

    if (!deepEqual(a[key], b[key])) {
      return false
    }
  }

  return true
}
