import type { FiltersState, FiltersStateBase } from '~/pages/SearchPageNew/types/filters-reducer'

export const toFilterStateBase = (filters: FiltersState): FiltersStateBase => ({
  inForce: filters.inForce,
  multipleChunks: filters.multipleChunks,
  selectedYearsBoundaries: filters.selectedYearsBoundaries,
  selectedLabels: filters.selectedLabels,
  selectedSources: filters.selectedSources
})
