import type { Document } from '~/types/search/documents'

import eventBus from './event-bus'

export const libraryEvents = eventBus<{
  findSimilarDocs: (document: Document) => void
}>()
