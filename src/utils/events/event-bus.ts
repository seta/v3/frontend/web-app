type EventHandler<T = never> = (data: T) => void
type EventMap = Record<string | symbol, EventHandler>

interface EventBus<T extends EventMap> {
  on<K extends keyof T>(type: K, handler: T[K]): void
  off<K extends keyof T>(type: K, handler: T[K]): void
  emit<K extends keyof T>(type: K, data: Parameters<T[K]>[0]): void
}

type EventRegistry<E> = Partial<Record<keyof E, E[keyof E][]>>

type EventBusConfig = {
  onError: (error: unknown) => void
}

/**
 * Use the event bus to communicate between different parts of the application without a context.
 *
 * Define the events channels you need and use them to emit and listen to their specific events.
 *
 * @example
 *
 * // Create an events channel
 * const searchEvents = eventBus<{
 *  search: (embeddings?: EmbeddingInfo[]) => void
 * }>()
 *
 * // Register an event listener
 * searchEvents.on('search', (embeddings) => { console.log(embeddings) })
 *
 * // Emit an event
 * searchEvents.emit('search', embeddings)
 *
 * // Unregister an event listener
 * searchEvents.off('search', handler)
 */
const eventBus = <E extends EventMap>(config?: EventBusConfig): EventBus<E> => {
  const { onError } = config ?? {}
  const eventRegistry: EventRegistry<E> = {}

  const on: EventBus<E>['on'] = (type, handler) => {
    if (!eventRegistry[type]) {
      eventRegistry[type] = []
    }

    eventRegistry[type]?.push(handler)
  }

  // TODO: Allow removing all handlers for a given event type
  const off: EventBus<E>['off'] = (type, handler) => {
    if (eventRegistry[type]) {
      eventRegistry[type] = eventRegistry[type]?.filter(h => h !== handler)
    }
  }

  const emit: EventBus<E>['emit'] = (type, data) => {
    const events = eventRegistry[type]

    if (!events) {
      return
    }

    events.forEach(handler => {
      try {
        handler(data)
      } catch (error) {
        onError?.(error)
      }
    })
  }

  return {
    on,
    off,
    emit
  }
}

export default eventBus
