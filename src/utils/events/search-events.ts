import type { SearchValue } from '~/pages/SearchPageNew/types/search'

import type { EmbeddingInfo } from '~/types/embeddings'

import eventBus from './event-bus'

export const searchEvents = eventBus<{
  setSearch: (search: SearchValue) => void
  setDocuments: (documents: EmbeddingInfo[]) => void
  clearQuery: () => void
}>()
