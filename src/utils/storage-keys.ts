export const STORAGE_KEY = {
  SEARCH: 'seta-search-value',
  ENRICH: 'seta-search-enrich',
  UPLOADS: 'seta-search-uploads',
  TEXT_UPLOAD: 'seta-search-text-upload',
  STAGED_DOCS: 'seta-search-staged-docs',
  EXPORT: 'seta-library-export',
  SIDEBAR: 'seta-sidebar',
  FILTERS: 'seta-filters',
  GRAPH_HISTORY: 'seta-graph-history',
  SEARCH_TAB: 'seta-search-tab'
} as const

export type StorageKey = (typeof STORAGE_KEY)[keyof typeof STORAGE_KEY]
