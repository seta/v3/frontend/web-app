import type { Token } from '~/pages/SearchPageNew/types/token'

import type { SavedSearchCreate } from '~/types/saved-search/saved-search'
import { SavedSearchType } from '~/types/saved-search/saved-search'
import { pluralize, toSentenceCase } from '~/utils/string-utils'

const getValueName = (tokens: Token[], value: string): string => {
  if (tokens.length === 1) {
    const trimmedValue = value.trim()

    return value.startsWith('"') ? trimmedValue : `"${trimmedValue}"`
  }

  return `${tokens.length} keywords`
}

export const generateSearchName = (savedSearch: SavedSearchCreate): string => {
  const { name, search: { search, filters } = {} } = savedSearch

  if (name) {
    return name
  }

  if (!search || !filters) {
    return '' // This is a folder, not a search
  }

  const nameParts: string[] = []

  if (search.tokens.length) {
    const valueName = getValueName(search.tokens, search.value)

    nameParts.push(valueName)

    if (search.enrichedStatus.enriched) {
      nameParts.push('auto-enriched')
    }
  }

  if (search.embeddings?.length) {
    nameParts.push(
      `${search.embeddings.length} ${pluralize('attachment', search.embeddings.length)}`
    )
  }

  if (filters.inForce) {
    nameParts.push('in force')
  }

  if (filters.multipleChunks) {
    nameParts.push('multiple chunks')
  }

  if (filters.selectedYearsBoundaries) {
    nameParts.push(`${filters.selectedYearsBoundaries.min}-${filters.selectedYearsBoundaries.max}`)
  }

  if (filters.selectedSources?.length) {
    if (filters.selectedSources.length === 1) {
      nameParts.push(filters.selectedSources[0].key)
    } else {
      nameParts.push(`${filters.selectedSources.length} sources`)
    }
  }

  if (filters.selectedLabels?.length) {
    if (filters.selectedLabels.length === 1) {
      nameParts.push(filters.selectedLabels[0].name)
    } else {
      nameParts.push(`${filters.selectedLabels.length} annotations`)
    }
  }

  return toSentenceCase(nameParts.join(', ')) || 'Empty search'
}

export const getSaveSearchPayload = (savedSearch: SavedSearchCreate): SavedSearchCreate => {
  return {
    ...savedSearch,
    type: savedSearch.type ?? SavedSearchType.Search,
    name: savedSearch.name ?? generateSearchName(savedSearch)
  }
}
