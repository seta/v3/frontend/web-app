import type { Document, DocumentBrief } from '~/types/search/documents'
import type { StagedDocument } from '~/types/search/staged-documents'

/**
 * Converts a `Document` to a `DocumentBrief`.
 *
 * @param document - The document to be converted.
 * @returns A `DocumentBrief` object containing a subset of the original document's properties.
 */
export const toDocumentBrief = (document: Document): DocumentBrief => {
  const { _id, source, title, abstract, chunk_text, chunk_number } = document

  return {
    _id,
    source,
    title,
    abstract,
    chunk_text,
    chunk_number
  }
}

/**
 * Converts a `Document` to a `StagedDocument`.
 *
 * @param document - The document to be converted.
 * @param documentBrief - An optional brief version of the document. If not provided, it will be generated from the document.
 * @returns A `StagedDocument` object containing the id, title, link, and original document brief.
 */
export const toStagedDocument = (
  document: Document,
  documentBrief?: DocumentBrief
): StagedDocument => {
  const { _id, title, source, link_origin } = document

  const originalDocument = documentBrief ?? toDocumentBrief(document)

  return {
    id: _id,
    title,
    source,
    link: link_origin,
    originalDocument
  }
}
