import { useState, useRef, useCallback, useEffect } from 'react'

const useAutoReset = (initialValue: boolean, delay = 1000) => {
  const [value, setValue] = useState(initialValue)
  const timeoutRef = useRef<number | null>(null)

  const clear = useCallback(() => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current)
    }
  }, [])

  const set = useCallback(() => {
    clear()

    setValue(!initialValue)

    timeoutRef.current = setTimeout(() => {
      setValue(initialValue)
    }, delay)
  }, [clear, initialValue, delay])

  useEffect(() => {
    return () => {
      clear()
    }
  }, [clear])

  return [value, set] as const
}

export default useAutoReset
