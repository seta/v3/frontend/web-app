import type { DependencyList, EffectCallback } from 'react'
import { useEffect, useRef } from 'react'

/**
 * A custom hook that defers the execution of an effect callback by a specified timeout.\
 * If the dependencies change before the timeout, the timeout is reset and the effect is not executed until the new timeout has passed.
 *
 * @param effect - The effect callback function to be executed after the timeout.
 * @param deps - The dependency list that determines when the effect should be re-executed.
 * @param timeout - The delay in milliseconds before the effect is executed - defaults to 300ms.
 *
 * @example
 * useDeferredEffect(() => {
 *   // Your effect logic here
 * }, [dependency1, dependency2], 500);
 */
const useDeferredEffect = (effect: EffectCallback, deps: DependencyList, timeout = 300) => {
  const timeoutRef = useRef<number | null>(null)

  useEffect(() => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current)
    }

    timeoutRef.current = window.setTimeout(() => {
      effect()
      timeoutRef.current = null
    }, timeout)

    return () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current)
      }

      timeoutRef.current = null
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, deps)
}

export default useDeferredEffect
