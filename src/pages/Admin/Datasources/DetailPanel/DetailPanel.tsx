import { Grid, Group, Title, Text } from '@mantine/core'

import type { DatasourceScopes } from '~/api/types/datasource-types'

import ThemeList from './components/ThemeList'

import ContactInfo from '../../common/components/Contact/Contact'
import ScopesList from '../DatasourcesTable/components/ManageScopes/components/ScopesList'

type Contact = {
  person?: string
  email?: string
  website?: string
}
type Props = {
  contactDetails?: Contact
  themes: string[]
  scopes?: DatasourceScopes[]
  organisation?: string
  description?: string
}

const DetailPanel = ({ scopes, contactDetails, themes, organisation, description }: Props) => {
  return (
    <Grid sx={{ justifyContent: 'center' }}>
      <Grid.Col md={12} lg={11}>
        <Group>
          <Title order={6}>Description:</Title>
          <Text>{description}</Text>
        </Group>
      </Grid.Col>
      <Grid.Col md={6} lg={6}>
        <Group spacing="lg" sx={{ alignItems: 'baseline' }}>
          <Group display="grid" w="40%">
            <Title order={6}>Contact Person:</Title>
            <ContactInfo
              person={contactDetails?.person}
              website={contactDetails?.website}
              email={contactDetails?.email}
            />
          </Group>
          <Group display="grid">
            <Title order={6}>Themes:</Title>
            {themes.length > 0 ? <ThemeList themes={themes} width="auto" /> : ''}
          </Group>
          <Group display="grid">
            <Title order={6}>Organisation:</Title>
            <Text>{organisation}</Text>
          </Group>
        </Group>
      </Grid.Col>
      <Grid.Col md={6} lg={5}>
        <Group>
          <Title order={6}>Scopes:</Title>
          {scopes && scopes?.length > 0 ? (
            <ScopesList scopes={scopes} />
          ) : (
            'No scopes for this data source.'
          )}
        </Group>
      </Grid.Col>
    </Grid>
  )
}

export default DetailPanel
