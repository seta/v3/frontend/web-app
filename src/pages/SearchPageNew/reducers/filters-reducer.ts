import type { ReferenceInfo, YearsRangeValue } from '~/pages/SearchPageNew/types/filters'

import { STORAGE_KEY } from '~/utils/storage-keys'
import { storage } from '~/utils/storage-utils'

import type { FiltersState, FiltersAction, FiltersStateBase } from '../types/filters-reducer'
import { DEFAULT_FILTERS_STATE } from '../utils/filters-constants'
import {
  extractStateForStorage,
  resetState,
  updateStateWithAggregations
} from '../utils/filters-utils'

const filtersStorage = storage<FiltersStateBase>(STORAGE_KEY.FILTERS)

const itemsSorter = (a: ReferenceInfo, b: ReferenceInfo) =>
  a.fullPath?.localeCompare(b.fullPath ?? '') ?? 0

const filtersReducer = (state: FiltersState, action: FiltersAction): FiltersState => {
  switch (action.type) {
    case 'set_aggregations': {
      const newState = updateStateWithAggregations(state, action.aggregations)

      filtersStorage.write(extractStateForStorage(newState))

      return newState
    }

    case 'toggle_inForce':
      return { ...state, inForce: !state.inForce }

    case 'toggle_multipleChunks':
      return { ...state, multipleChunks: !state.multipleChunks }

    case 'update_yearsRange':
      return {
        ...state,
        yearsRange: action.value,
        selectedYearsBoundaries: { min: action.value[0], max: action.value[1] }
      }

    case 'reset_yearsRange':
      return {
        ...state,
        yearsRange: DEFAULT_FILTERS_STATE.yearsRange,
        selectedYearsBoundaries: DEFAULT_FILTERS_STATE.selectedYearsBoundaries
      }

    case 'update_labels':
      return { ...state, selectedLabels: action.labels }

    case 'update_sources':
      return { ...state, selectedSources: action.items.sort(itemsSorter) }

    case 'set_all': {
      const yearsRange: YearsRangeValue = [
        action.state.selectedYearsBoundaries?.min ?? 0,
        action.state.selectedYearsBoundaries?.max ?? 0
      ]

      return { ...DEFAULT_FILTERS_STATE, ...action.state, yearsRange }
    }

    case 'clear_all':
      return resetState(state)

    default:
      return state
  }
}

export default filtersReducer
