import { useCallback, useEffect, useState } from 'react'
import { Flex } from '@mantine/core'
import { useQueryClient } from '@tanstack/react-query'

import Page from '~/components/Page'
import ConceptsGraphTab from '~/pages/SearchPageNew/components/SearchResultsTabs/components/ConceptsGraphTab/ConceptsGraphTab'
import { hasDocumentOptions } from '~/pages/SearchPageNew/utils/filters-utils'

import { type DocumentsOptions, queryKey as documentsQueryKey } from '~/api/search/documents'
import type { Crumb } from '~/types/breadcrumbs'
import { searchEvents } from '~/utils/events/search-events'
import { getSearchQueryAndTerms, getTermsFromTokens } from '~/utils/search-utils'

import SearchResults from './components/SearchResults'
import { DocumentsTab } from './components/SearchResultsTabs'
import SearchSuggestionInput from './components/SearchSuggestionInput'
import SidebarContent from './components/SidebarContent'
import { DocumentsQueryProvider } from './contexts/documents-query-context'
import { EnrichLoadingProvider } from './contexts/enrich-loading-context'
import { FiltersProvider } from './contexts/filters-context'
import { SearchResultsProvider } from './contexts/search-results-context'
import { StagedDocumentsProvider } from './contexts/staged-documents-context'
import { UploadDocumentsProvider } from './contexts/upload-documents-context'
import * as S from './styles'
import type { SearchData, SearchState, SearchValue } from './types/search'
import { EnrichType } from './types/search'

const EMPTY_SEARCH: SearchData = {
  tokens: [],
  enrichedStatus: { enriched: false, type: EnrichType.Ontology },
  embeddings: []
}

const SearchPageNew = () => {
  const [internalValue, setInternalValue] = useState<SearchData | null>(null)
  const [query, setQuery] = useState<SearchState>(null)
  const [searchOptions, setSearchOptions] = useState<DocumentsOptions | undefined>(undefined)
  const [enrichLoading, setEnrichLoading] = useState(false)
  const [page, setPage] = useState(1)

  const [searchPressed, setSearchPressed] = useState(false)

  const queryClient = useQueryClient()

  useEffect(() => {
    // Reset the internal value when the search is cleared via the search events
    const clearInternalQuery = () => {
      setSearchPressed(false)
      setInternalValue(null)
    }

    searchEvents.on('clearQuery', clearInternalQuery)

    return () => {
      searchEvents.off('clearQuery', clearInternalQuery)
    }
  }, [])

  const updateSearchData = useCallback((value: SearchData | null) => {
    setInternalValue(value)

    setQuery(prev => ({
      ...prev,
      // Update the terms when the tokens change
      terms: getTermsFromTokens(value?.tokens),
      value: prev?.value ?? ''
    }))
  }, [])

  const processSearchData = useCallback(async (search: SearchData | null) => {
    const { tokens, enrichedStatus, embeddings } = search ?? EMPTY_SEARCH

    setEnrichLoading(true)

    const { query: value, terms } = await getSearchQueryAndTerms(tokens, enrichedStatus)

    setEnrichLoading(false)

    setPage(1)
    setQuery({ value, terms, embeddings })
  }, [])

  const handleSearch = async ({ tokens, enrichedStatus, embeddings }: SearchValue) => {
    setSearchPressed(true)

    await processSearchData({ tokens, enrichedStatus, embeddings })

    // Refresh the documents list if the query is stale
    queryClient.refetchQueries({ queryKey: [documentsQueryKey.root], stale: true, type: 'active' })
  }

  const handleApplyFilters = useCallback(
    async (value: DocumentsOptions) => {
      // Consider the search as pressed only if there are document options - if the query is not empty
      setSearchPressed(hasDocumentOptions(value))
      setSearchOptions(value)

      await processSearchData(internalValue)
    },
    [internalValue, processSearchData]
  )

  const sidebar = <SidebarContent />

  const breadcrumbs: Crumb[] = [
    {
      title: 'Search',
      path: '/search'
    }
  ]

  return (
    <DocumentsQueryProvider
      query={query}
      searchOptions={searchOptions}
      setSearchData={updateSearchData}
    >
      <FiltersProvider onApplyFilters={handleApplyFilters}>
        <Page sidebarContent={sidebar} breadcrumbs={breadcrumbs} collapsibleSidebar>
          <EnrichLoadingProvider loading={enrichLoading}>
            <UploadDocumentsProvider>
              <Flex direction="column" align="center" css={S.searchWrapper}>
                <SearchSuggestionInput onSearch={handleSearch} />

                <SearchResultsProvider>
                  <StagedDocumentsProvider>
                    <SearchResults>
                      <DocumentsTab
                        page={page}
                        searchPressed={searchPressed}
                        onPageChange={setPage}
                      />

                      <ConceptsGraphTab />
                    </SearchResults>
                  </StagedDocumentsProvider>
                </SearchResultsProvider>
              </Flex>
            </UploadDocumentsProvider>
          </EnrichLoadingProvider>
        </Page>
      </FiltersProvider>
    </DocumentsQueryProvider>
  )
}

export default SearchPageNew
