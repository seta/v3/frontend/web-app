import { useMemo, useState } from 'react'

import SaveSearchModal from '~/pages/SearchPageNew/components/saved-search/SaveSearchModal'
import type { FiltersState } from '~/pages/SearchPageNew/types/filters-reducer'
import type { SearchValue } from '~/pages/SearchPageNew/types/search'

import { useSaveSearchMutation } from '~/api/search/saved-searches'
import type { SavedSearchCreate } from '~/types/saved-search/saved-search'
import { SavedSearchType } from '~/types/saved-search/saved-search'
import { toFilterStateBase } from '~/utils/filter-utils'
import { notifications } from '~/utils/notifications'
import { generateSearchName } from '~/utils/saved-search-utils'

type Args = {
  searchValue: SearchValue
  filtersState: FiltersState
}

const useSaveSearchModal = ({ searchValue, filtersState }: Args) => {
  const [modalOpen, setModalOpen] = useState(false)

  const { mutate, isLoading } = useSaveSearchMutation()

  const savedSearchCreate: SavedSearchCreate = useMemo(() => {
    const initialSearchCreate: SavedSearchCreate = {
      type: SavedSearchType.Search,
      parentId: null,

      search: {
        search: searchValue,
        filters: toFilterStateBase(filtersState)
      }
    }

    return {
      ...initialSearchCreate,
      name: generateSearchName(initialSearchCreate)
    }
  }, [searchValue, filtersState])

  const handleClose = () => {
    setModalOpen(false)
  }

  const handleSave = (value: SavedSearchCreate) => {
    // `value` here is the updated saved search via the modal
    mutate(value, {
      onSuccess: () => {
        setModalOpen(false)
        notifications.showSuccess('Current search was saved successfully.')
      },
      onError: () => {
        notifications.showError('An error occurred while saving the search.', {
          description: 'Please try again.'
        })
      }
    })
  }

  const saveSearch = () => {
    setModalOpen(true)
  }

  const saveSearchModal = (
    <SaveSearchModal
      savedSearchCreate={savedSearchCreate}
      opened={modalOpen}
      saving={isLoading}
      onSave={handleSave}
      onClose={handleClose}
    />
  )

  return {
    saveSearch,
    saveSearchModal
  }
}

export default useSaveSearchModal
