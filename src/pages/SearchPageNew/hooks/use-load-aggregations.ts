import type { Dispatch } from 'react'
import { useRef, useState, useEffect, useMemo } from 'react'
import { useQueryClient } from '@tanstack/react-query'

import { useAggregations, queryKey as aggregationsQueryKey } from '~/api/search/aggregations'
import { STORAGE_KEY } from '~/utils/storage-keys'
import { storage } from '~/utils/storage-utils'

import { useDocumentsQuery } from '../contexts/documents-query-context'
import type { FiltersAction } from '../types/filters-reducer'
import { getQueryAggregations } from '../utils/filters-utils'

type Args = {
  dispatch: Dispatch<FiltersAction>
}

type Result = {
  isLoading: boolean
}

const searchStorage = storage<string>(STORAGE_KEY.SEARCH)
const uploadsStorage = storage<unknown[]>(STORAGE_KEY.UPLOADS)

const useLoadAggregations = ({ dispatch }: Args): Result => {
  // Decide when to perform the initial load of the aggregations
  const [allowLoadAggs, setAllowLoadAggs] = useState(false)

  const { query, searchOptions } = useDocumentsQuery()

  const [queryValue, embeddings] = [query?.value, query?.embeddings]

  const queryClient = useQueryClient()

  const isFirstRenderRef = useRef(true)

  // Fetch aggregations for the current query and/or embeddings
  const { data: aggregations, isFetching } = useAggregations(queryValue, embeddings, {
    enabled: allowLoadAggs,
    searchOptions: {
      ...searchOptions,
      // We don't care about the sort order for aggregations
      sort: undefined
    },
    // Don't set the data empty when the query starts loading
    keepPreviousData: true
  })

  useEffect(() => {
    isFirstRenderRef.current = false

    const savedSearch = searchStorage.read()
    const savedUploads = uploadsStorage.read()

    // Skip the initial load when there is a saved search or uploads
    // to avoid an extra request before `queryValue` or `embeddings` reflect the saved search/uploads
    if (savedSearch || savedUploads?.length) {
      return
    }

    setAllowLoadAggs(true)

    // Invalidate the initial aggregations query to force a new request when no saved search is present
    queryClient.invalidateQueries([aggregationsQueryKey.root, {}])

    // We only want to run this effect once
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    // Allow loading the aggregations when the query or embeddings are set
    if (!!queryValue || !!embeddings?.length) {
      setAllowLoadAggs(true)
    }
  }, [queryValue, embeddings])

  const aggregationResult = useMemo(
    () => getQueryAggregations(aggregations, searchOptions),
    [aggregations, searchOptions]
  )

  useEffect(() => {
    // If `date_year` is undefined it means the aggregations didn't yet load
    if (aggregationResult && aggregationResult.date_year !== undefined) {
      // When the aggregations change, update the state with the new aggregations
      dispatch({ type: 'set_aggregations', aggregations: aggregationResult })
    }
  }, [aggregationResult, dispatch])

  return { isLoading: isFetching }
}

export default useLoadAggregations
