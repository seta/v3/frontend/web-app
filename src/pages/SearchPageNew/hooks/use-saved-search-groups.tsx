import { IconFolderSearch, IconStar } from '@tabler/icons-react'
import type { UseQueryResult } from '@tanstack/react-query'

import type { SavedSearchResponse } from '~/api/search/saved-searches'
import { useSavedSearches } from '~/api/search/saved-searches'
import type {
  SavedSearch,
  SearchGroup,
  SearchGroupWithIcon
} from '~/types/saved-search/saved-search'
import { SavedSearchType } from '~/types/saved-search/saved-search'

type Result = {
  data?: { groups: SearchGroupWithIcon[] } | null
  isLoading?: boolean
  error?: unknown
  refetch: UseQueryResult<SavedSearchResponse>['refetch']
}

export const FAV_GROUP_ID = 'FAV'

const DEFAULT_GROUP: SearchGroup = {
  id: FAV_GROUP_ID,
  name: 'Favourite Searches',
  type: SavedSearchType.Folder,
  parentId: null,
  children: []
}

const getIcon = (group: SavedSearch) =>
  group.id === FAV_GROUP_ID ? <IconStar size={20} /> : <IconFolderSearch size={20} />

/**
 * Custom hook to fetch and manage saved search groups.
 *
 * This hook utilizes the `useSavedSearches` hook to retrieve saved searches data,\
 * and processes it to extract and format search groups, including default and folder-type groups.
 *
 * As only search groups should be displayed as top-level items in My Searches,\
 * using this hook simplifies the process of formatting the data.
 *
 * @returns An object containing:
 * - `data`: An object with a `groups` property, which is an array of search groups.
 * - `error`: An error object if the fetch operation failed.
 * - `isLoading`: A boolean indicating if the data is still loading.
 * - `refetch`: A function to manually refetch the data.
 */
const useSavedSearchGroups = (): Result => {
  const { data, error, isLoading, refetch } = useSavedSearches()

  if (error) {
    return { error, refetch }
  }

  if (isLoading || !data) {
    return { isLoading, refetch }
  }

  const savedFolders = data.items.filter<SearchGroup>(
    (item): item is SearchGroup => item.type === SavedSearchType.Folder
  )

  const rootSearches = data.items.filter(
    item => item.type === SavedSearchType.Search && !item.parentId
  )

  const groups: SearchGroupWithIcon[] = [DEFAULT_GROUP, ...savedFolders].map(group => ({
    ...group,
    children: group.id === FAV_GROUP_ID ? rootSearches : group.children || [],
    icon: getIcon(group)
  }))

  return { data: { groups }, refetch }
}

export default useSavedSearchGroups
