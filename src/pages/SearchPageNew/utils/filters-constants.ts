import type { FiltersState } from '../types/filters-reducer'

export const DEFAULT_YEARS_BOUNDARIES = { min: undefined, max: undefined }

export const DEFAULT_FILTERS_STATE: FiltersState = {
  inForce: false,
  multipleChunks: false,
  yearsRange: undefined,
  yearsRangeBoundaries: DEFAULT_YEARS_BOUNDARIES,
  selectedYearsBoundaries: undefined,
  selectedLabels: [],
  selectedSources: [],
  values: {}
}

export enum FiltersAccordionItemName {
  DATA_SOURCE = 'data-source',
  TAXONOMY_TREE = 'taxonomy-tree',
  OTHER_FILTER = 'other-filter',
  LABELS = 'labels'
}
