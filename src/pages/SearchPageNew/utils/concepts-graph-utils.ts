import type {
  CustomGraphData,
  GraphLink,
  GraphNode
} from '~/pages/SearchPageNew/types/concepts-graph'
import { NodeType } from '~/pages/SearchPageNew/types/concepts-graph'

/**
 * Builds a graph data structure consisting of nodes and links based on the provided term and groups.
 *
 * @param term - The root term for the graph.
 * @param groups - An array of groups, where each group is an array of strings. The first string in each group is considered a parent node, and the rest are considered leaf nodes.
 * @returns An object containing the nodes and links for the graph.
 *
 * The links are created such that there is a link from the root to the first word in each group, and from the first word to the rest of the words in the group.
 *
 * **Example:**\
 * If the term is `'root'` and the group is `[['animal welfare', 'animal protection', 'farm animal welfare', 'animal welfare standards']]`,
 * the resulting links will be:
 * `'root' -> 'animal welfare' -> 'animal protection', 'farm animal welfare', 'animal welfare standards'`.
 */
export const buildNodesLinks = (term: string, groups: string[][] | undefined): CustomGraphData => {
  const root = term
  const rootNode: GraphNode = { id: root, label: root, type: NodeType.root }

  if (!groups) {
    return { nodes: [rootNode], links: [] }
  }

  // Get parent and leaf nodes separately so that they can be rendered in a specific order
  const [parents, leaves] = groups.reduce<[GraphNode[], GraphNode[]]>(
    ([parentsAcc, leavesAcc], group) => {
      // The first word in the group is the parent node, the rest are leaves
      const [firstWord, ...rest] = group

      // If there is only one word in the group, it's rendered as a leaf node - otherwise, it's a parent
      const firstNodeType: NodeType = rest.length > 0 ? NodeType.parent : NodeType.leaf

      return [
        [...parentsAcc, { id: firstWord, label: firstWord, type: firstNodeType }],
        [...leavesAcc, ...rest.map(word => ({ id: word, label: word, type: NodeType.leaf }))]
      ]
    },
    [[], []]
  )

  // Compose the nodes array in the order: leaves, parents, root - so that the root and parents are rendered on top of the leaves
  const nodes: GraphNode[] = [...leaves, ...parents, rootNode]

  // For each group, create a link from the root to the first word in the group and from the first term to the rest of the words in the group.
  // Ex: 'root' -> 'animal welfare' -> 'animal protection', 'farm animal welfare', 'animal welfare standards'
  // where 'root' is the term and the group contains ['animal welfare', 'animal protection', 'farm animal welfare', 'animal welfare standards']

  const links: GraphLink[] = groups
    .map(group => {
      const [firstWord, ...rest] = group

      return [
        { source: root, target: firstWord },
        ...rest.map(word => ({ source: firstWord, target: word }))
      ]
    })
    .flat()

  return { nodes, links }
}
