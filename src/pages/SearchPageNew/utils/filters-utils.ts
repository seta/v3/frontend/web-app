import type { AggregationsResponse } from '~/api/search/aggregations'
import type { DocumentsOptions } from '~/api/search/documents'
import { SearchType } from '~/types/search/common'
import { deepEqual } from '~/utils/object-utils'

import { DEFAULT_FILTERS_STATE } from './filters-constants'

import type {
  CollectionInfo,
  QueryAggregations,
  ReferenceInfo,
  SourceInfo,
  ValueWithCount,
  YearWithCount,
  YearsRangeValue
} from '../types/filters'
import type { FiltersState, FiltersStateBase } from '../types/filters-reducer'

const yearsRangeToQuery = (value: YearsRangeValue | undefined): string[] | undefined => {
  if (!value) {
    return undefined
  }

  // Keep lower interval
  const gte = value[0]

  // Transform upper interval to 'lower than' selected year + 1
  const lt = value[1] + 1

  return [`gte:${gte}`, `lt:${lt}`]
}

const yearsRangeToFilter = (value: YearWithCount[] | undefined): YearsRangeValue | undefined => {
  if (!value || !value.length) {
    return undefined
  }

  const years = value.map(v => Number(v.year))

  // Return the min and max years
  return [Math.min(...years), Math.max(...years)]
}

// Map the `path` ids in the format of `[sourceIds[], collectionIds[], referenceIds[]]`
// to the corresponding query keys
const pathIdToQueryMap: Record<
  number,
  keyof Pick<DocumentsOptions, 'source' | 'collection' | 'reference'>
> = {
  0: 'source',
  1: 'collection',
  2: 'reference'
}

// Reduce the sources to 3 arrays of ids for the sources, collections, and references
const reduceSourcesIds = (sources: ReferenceInfo[]) =>
  sources.reduce<[string[], string[], string[]]>(
    (acc, source) => {
      const { path } = source

      // The `path` array is in the format of `[sourceId, collectionId, referenceId]`
      path?.forEach((id, index) => {
        acc[index].push(id)
      })

      return acc
    },
    [[], [], []]
  )

/**
 * Converts the filters state to a `DocumentsOptions` object to be used in the search query.
 *
 * @param filters - The filters state object.
 * @returns The DocumentsOptions object.
 */
export const filtersToDocumentOptions = ({
  inForce,
  multipleChunks,
  yearsRange,
  selectedLabels,
  selectedSources,
  selectedYearsBoundaries
}: FiltersState): DocumentsOptions => {
  const result: DocumentsOptions = {
    in_force: inForce ? 'true' : undefined,
    search_type: multipleChunks ? SearchType.ALL_CHUNKS_SEARCH : SearchType.CHUNK_SEARCH,
    date_range: selectedYearsBoundaries ? yearsRangeToQuery(yearsRange) : undefined
  }

  if (selectedLabels) {
    result.annotation = selectedLabels.map(({ category, name }) => `${category}:${name}`)
  }

  if (selectedSources) {
    // The `ids` will contain up to 3 arrays of ids for the sources, collections, and references
    const ids = reduceSourcesIds(selectedSources)

    ids.forEach((value, index) => {
      if (value) {
        const key = pathIdToQueryMap[index]

        result[key] = [...new Set(value)]
      }
    })
  }

  return result
}

/**
 * Transforms the data sources by adding path and fullPath properties to each source, collection, and reference.
 *
 * Also returns a flat array of all reference information.
 *
 * @param dataSources - The array of SourceInfo objects representing the data sources.
 * @returns A tuple containing the transformed data sources and a flat array of all reference information.
 */
const transformDataSources = (
  dataSources: SourceInfo[] | undefined
): [SourceInfo[] | undefined, ReferenceInfo[]] => {
  const flatDataSources: ReferenceInfo[] = []

  const transformed = dataSources?.map(source => {
    source.path = [source.key]
    source.fullPath = source.key

    flatDataSources.push(source)

    source.collections?.forEach(collection => {
      collection.path = [source.key, collection.key]
      collection.fullPath = collection.path.join(' / ')

      flatDataSources.push(collection)

      collection.references?.forEach(reference => {
        reference.path = [source.key, collection.key, reference.key]
        reference.fullPath = reference.path.join(' / ')

        flatDataSources.push(reference)
      })
    })

    return source
  })

  return [transformed, flatDataSources]
}

/**
 * Retrieves the values for a given years range.
 *
 * Fills in the missing years between the min and max of `yearsRange` with 0 counts.
 *
 * @param yearsRange - The range of years to retrieve values for.
 * @param yearsWithCount - An optional array of YearWithCount objects.
 * @returns An array of ValueWithCount objects representing the values for the given years range.
 */
const getYearsRangeValues = (
  yearsRange: YearsRangeValue,
  yearsWithCount: YearWithCount[] | undefined
) => {
  const values: ValueWithCount[] = []

  for (let year = yearsRange[0]; year <= yearsRange[1]; year++) {
    const yearWithCount: YearWithCount = yearsWithCount?.find(date => date.year === year) ?? {
      year,
      doc_count: 0
    }

    const yearString = year.toString()

    values.push({
      key: yearString,
      label: yearString,
      count: yearWithCount.doc_count,
      category: 'year'
    })
  }

  return values
}

/**
 * Updates the state with the given aggregations.
 *
 * @param state - The current state of filters.
 * @param aggregations - The aggregations to update the state with.
 * @returns The updated state with the aggregations applied.
 */
export const updateStateWithAggregations = (
  state: FiltersState,
  aggregations: QueryAggregations
): FiltersState => {
  const { date_year } = aggregations

  const values: FiltersState['values'] = { years: [] }

  const yearsRange: FiltersState['yearsRange'] = state.selectedYearsBoundaries
    ? [state.selectedYearsBoundaries.min ?? 0, state.selectedYearsBoundaries.max ?? 0]
    : yearsRangeToFilter(date_year)

  if (state.selectedYearsBoundaries && yearsRange) {
    values.years = getYearsRangeValues(yearsRange, date_year)
  } else if (date_year && date_year.length) {
    values.years = date_year.map(y => {
      return { key: y.year, label: y.year, count: y.doc_count, category: 'year' }
    })
  }

  const yearsRangeBoundaries: FiltersState['yearsRangeBoundaries'] = {
    min: yearsRange?.[0],
    max: yearsRange?.[1]
  }

  const [dataSources, flatDataSources] = transformDataSources(
    aggregations.source_collection_reference?.sources
  )

  const selectedSources = state.selectedSources?.map(selected => {
    const foundSource = flatDataSources?.find(source => source.fullPath === selected.fullPath)

    const value: ReferenceInfo = {
      ...selected,
      // Mark the selected sources as unavailable if they don't exist in the current aggregations
      notAvailable: !foundSource
    }

    return value
  })

  return {
    ...state,
    yearsRange,
    yearsRangeBoundaries,
    dataSources,
    selectedSources,
    values
  }
}

/**
 * Converts aggregations to filters state.
 *
 * @param aggregations - The aggregations to convert.
 * @returns The filters state.
 */
export const aggregationsToFilters = (aggregations: QueryAggregations | null): FiltersState => {
  const defaultFilters: FiltersState = { ...DEFAULT_FILTERS_STATE }

  if (!aggregations) {
    return defaultFilters
  }

  return updateStateWithAggregations(defaultFilters, aggregations)
}

/**
 * Checks if there are any changes in the filters state compared to the default filters state.
 *
 * @param filtersState - The current filters state.
 * @returns A boolean indicating whether there are any changes in the filters state.
 */
export const hasFiltersChanges = (filtersState: FiltersStateBase): boolean => {
  const keys: (keyof FiltersStateBase)[] = [
    'inForce',
    'multipleChunks',
    'selectedYearsBoundaries',
    'selectedLabels',
    'selectedSources'
  ]

  for (const key of keys) {
    const value = filtersState[key]

    if (!deepEqual(value, DEFAULT_FILTERS_STATE[key])) {
      return true
    }
  }

  return false
}

/**
 * Checks if the given document options contain any valid values.
 *
 * This function iterates over the keys of the provided `DocumentsOptions` object
 * and checks if any of the values meet the following criteria:
 * - If the value is an array and has a length greater than 0.
 * - If the key is 'search_type' and the value is not `SearchType.CHUNK_SEARCH`.
 * - If the value is truthy.
 *
 * @param options - The document options to check.
 * @returns `true` if any of the options contain valid values, otherwise `false`.
 */
export const hasDocumentOptions = (options: DocumentsOptions): boolean => {
  for (const k of Object.keys(options)) {
    const key = k as keyof DocumentsOptions
    const value = options[key]

    if (Array.isArray(value)) {
      if (value.length) {
        return true
      }

      continue
    }

    if (key === 'search_type') {
      if (value !== SearchType.CHUNK_SEARCH) {
        return true
      }

      continue
    }

    if (value) {
      return true
    }
  }

  return false
}

/**
 * Retrieves the query aggregations from the response and search options.
 * @param response - The response object containing the aggregations.
 * @param searchOptions - The search options object.
 * @returns The query aggregations object.
 */
export const getQueryAggregations = (
  response: AggregationsResponse | undefined,
  searchOptions: DocumentsOptions | undefined
): QueryAggregations => {
  const { search_type } = searchOptions ?? {}
  const { date_year, source_collection_reference, taxonomies } = response?.aggregations ?? {}

  return {
    date_year,
    search_type,
    source_collection_reference,
    taxonomies
  }
}

/**
 * Checks if the provided value is of type `SourceInfo`.
 * @param value - The value to check.
 * @returns `true` if the value is of type `SourceInfo`, `false` otherwise.
 */
export const isSourceInfo = (
  value: SourceInfo | CollectionInfo | ReferenceInfo
): value is SourceInfo => {
  return 'collections' in value && !!value.collections?.length
}

/**
 * Checks if the provided value is of type `CollectionInfo`.
 * @param value - The value to check.
 * @returns `true` if the value is of type `CollectionInfo`, `false` otherwise.
 */
export const isCollectionInfo = (
  value: SourceInfo | CollectionInfo | ReferenceInfo
): value is CollectionInfo => {
  return 'references' in value && !!value.references?.length
}

/**
 * Resets the state of the filters to the default values.
 *
 * @param state - The current state of the filters.
 * @returns The updated state with the default values.
 */
export const resetState = (state: FiltersState): FiltersState => {
  return {
    ...state,
    ...DEFAULT_FILTERS_STATE,
    // Preserve the values and boundaries to avoid the UI jumping until the new aggregations are fetched
    values: state.values,
    yearsRangeBoundaries: state.yearsRangeBoundaries
  }
}

export const extractStateForStorage = (state: FiltersState): FiltersStateBase => ({
  inForce: state.inForce,
  multipleChunks: state.multipleChunks,
  selectedYearsBoundaries: state.selectedYearsBoundaries,
  selectedLabels: state.selectedLabels,
  selectedSources: state.selectedSources
})

export const hasSavedFilters = (savedState: FiltersStateBase | null) => {
  if (!savedState) {
    return false
  }

  return hasFiltersChanges(savedState)
}
