import type { ReactNode } from 'react'
import { Stack, Text } from '@mantine/core'
import { IconLayoutList } from '@tabler/icons-react'

type Props = {
  name?: string
  icon?: ReactNode
}

const NoItemsSelected = ({ name = 'items', icon = <IconLayoutList /> }: Props) => (
  <Stack align="center" spacing="sm" p="md">
    <Text lh={1} color="gray.5">
      {icon}
    </Text>

    <Text size="sm" color="gray.6">
      No {name} selected
    </Text>
  </Stack>
)

export default NoItemsSelected
