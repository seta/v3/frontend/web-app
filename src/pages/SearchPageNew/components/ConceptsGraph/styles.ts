import { css } from '@emotion/react'

export const container = css`
  min-height: 80vh;
  margin-bottom: -3rem;
  padding: 0.1rem;
  position: relative;

  canvas {
    /* We're setting the canvas size based on the container's dimensions */
    position: absolute;
    left: 0;
    top: 0;
  }
`
