import { Space, Tooltip } from '@mantine/core'
import { IconArrowAutofitHeight, IconFocus2, IconZoomIn, IconZoomOut } from '@tabler/icons-react'
import type { ForceGraphMethods } from 'react-force-graph-2d'

import type { ActionIconExtendedProps } from '~/components/ActionIconExtended'
import ActionIconExtended from '~/components/ActionIconExtended'

import * as S from './styles'

type Props = {
  graph: ForceGraphMethods | undefined
  zoom: number
}

const buttonProps: ActionIconExtendedProps = {
  hoverVariant: 'light',
  size: 'lg'
}

const ZOOM_STEP = 0.3

const GraphActions = ({ graph, zoom }: Props) => {
  const handleZoom = (ratio: number) => {
    graph?.zoom(zoom * ratio, 200)
  }

  return (
    <div css={S.root}>
      <Tooltip.Group openDelay={300}>
        <Tooltip label="Zoom out" withinPortal>
          <ActionIconExtended {...buttonProps} onClick={() => handleZoom(1 - ZOOM_STEP)}>
            <IconZoomOut />
          </ActionIconExtended>
        </Tooltip>

        <Tooltip label="Zoom in" withinPortal>
          <ActionIconExtended {...buttonProps} onClick={() => handleZoom(1 + ZOOM_STEP)}>
            <IconZoomIn />
          </ActionIconExtended>
        </Tooltip>

        <Space w="sm" />

        <Tooltip label="Scale to fit" withinPortal>
          <ActionIconExtended {...buttonProps} onClick={() => graph?.zoomToFit(200)}>
            <IconArrowAutofitHeight />
          </ActionIconExtended>
        </Tooltip>

        <Tooltip label="Center graph" withinPortal>
          <ActionIconExtended {...buttonProps} onClick={() => graph?.centerAt(0, 0, 200)}>
            <IconFocus2 />
          </ActionIconExtended>
        </Tooltip>
      </Tooltip.Group>
    </div>
  )
}

export default GraphActions
