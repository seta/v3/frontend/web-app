import { css } from '@emotion/react'

export const root: ThemedCSS = theme => css`
  display: flex;
  position: absolute;
  top: ${theme.spacing.xs};
  right: ${theme.spacing.xs};
  z-index: 1;
`
