import { Tooltip } from '@mantine/core'
import { IconArrowBarToUp } from '@tabler/icons-react'

import type { ActionIconExtendedProps } from '~/components/ActionIconExtended'
import ActionIconExtended from '~/components/ActionIconExtended'

import * as S from './styles'

const buttonProps: ActionIconExtendedProps = {
  hoverVariant: 'light',
  size: 'lg'
}

const GraphActionsRight = () => {
  const handleScrollToTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }

  return (
    <div css={S.root}>
      <Tooltip.Group openDelay={300}>
        <Tooltip label="Scroll to the top of the page" withinPortal>
          <ActionIconExtended {...buttonProps} onClick={handleScrollToTop}>
            <IconArrowBarToUp />
          </ActionIconExtended>
        </Tooltip>
      </Tooltip.Group>
    </div>
  )
}

export default GraphActionsRight
