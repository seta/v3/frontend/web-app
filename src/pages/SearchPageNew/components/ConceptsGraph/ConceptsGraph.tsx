import { useEffect, useMemo, useRef, useState } from 'react'
import { DEFAULT_THEME } from '@mantine/core'
import { useElementSize } from '@mantine/hooks'
import type { ForceGraphMethods } from 'react-force-graph-2d'
import ForceGraph from 'react-force-graph-2d'

import type { CustomGraphData, GraphNode } from '~/pages/SearchPageNew/types/concepts-graph'
import { NodeType } from '~/pages/SearchPageNew/types/concepts-graph'

import GraphActions from './components/GraphActions'
import GraphActionsRight from './components/GraphActionsRight'
import * as S from './styles'

type Props = {
  data: CustomGraphData
  onNodeClick?: (node: GraphNode) => void
}

const NODE_COLOR: Record<NodeType, string> = {
  [NodeType.root]: DEFAULT_THEME.colors.green[8],
  [NodeType.parent]: DEFAULT_THEME.colors.red[7],
  [NodeType.leaf]: DEFAULT_THEME.colors.dark[3]
}

const SIZE_MULTIPLIER: Record<NodeType, number> = {
  [NodeType.root]: 1.25,
  [NodeType.parent]: 1.15,
  [NodeType.leaf]: 1
}

const FONT_SCALE_RATIO = 16
const NODE_BG = 'rgba(255, 255, 255, 0.8)'

const ConceptsGraph = ({ data, onNodeClick }: Props) => {
  const graphRef = useRef<ForceGraphMethods>()

  const [zoomLevel, setZoomLevel] = useState(1)

  // Keep track of the container's dimensions to resize the graph accordingly
  const { ref: containerRef, width, height } = useElementSize()

  // Create a stable reference to the data to prevent unnecessary re-renders
  const graphData = useMemo(() => data, [data])

  const updateZoomLevel = (value: number) => {
    // Update the state after the graph's render cycle
    setTimeout(() => {
      setZoomLevel(value)
    }, 0)
  }

  useEffect(() => {
    const graph = graphRef.current

    if (!graph) {
      return
    }

    // Set the charge strength to allow for better node separation
    graph.d3Force('charge')?.strength(-200)
  }, [])

  useEffect(() => {
    setTimeout(() => {
      const graph = graphRef.current

      if (!graph) {
        return
      }

      // Reset zoom level and center the graph when the data changes
      graph.zoom(1, 200)
      graph.centerAt(0, 0, 200)
    }, 0)
  }, [graphData])

  return (
    <div ref={containerRef} css={S.container}>
      <GraphActions graph={graphRef.current} zoom={zoomLevel} />
      <GraphActionsRight />

      <ForceGraph
        ref={graphRef}
        width={width}
        height={height}
        graphData={graphData}
        linkWidth={1}
        linkColor={() => DEFAULT_THEME.colors.gray[5]}
        onZoomEnd={({ k }) => updateZoomLevel(k)}
        nodeCanvasObject={(n, ctx, globalScale) => {
          // As long as the data is CustomGraphData, we can safely cast the node as GraphNode
          const node = n as GraphNode

          const label = node.label
          const fontSize = (FONT_SCALE_RATIO / globalScale) * SIZE_MULTIPLIER[node.type]

          // Set the font size and family here to allow measuring the rendered text width
          ctx.font = `${fontSize}px Sans-Serif`

          const textWidth = ctx.measureText(label).width

          // Add a bit of padding to the background dimensions
          const bgDimensions = [textWidth, fontSize].map(val => val + fontSize * 0.1) as [
            number,
            number
          ]

          const x = node.x ?? 0
          const y = node.y ?? 0

          ctx.fillStyle = NODE_BG
          ctx.fillRect(x - bgDimensions[0] / 2, y - bgDimensions[1] / 2, ...bgDimensions)

          ctx.textAlign = 'center'
          ctx.textBaseline = 'middle'
          ctx.fillStyle = NODE_COLOR[node.type]

          ctx.fillText(label, x, y)

          node.__bgDimensions = bgDimensions
        }}
        nodePointerAreaPaint={(node, color, ctx) => {
          ctx.fillStyle = color
          const bgDimensions: [number, number] = node.__bgDimensions

          bgDimensions &&
            ctx.fillRect(
              (node.x ?? 0) - bgDimensions[0] / 2,
              (node.y ?? 0) - bgDimensions[1] / 2,
              ...bgDimensions
            )
        }}
        onNodeClick={node => {
          // As long as the data is CustomGraphData, we can safely cast the node as GraphNode
          onNodeClick?.(node as GraphNode)
        }}
      />
    </div>
  )
}

export default ConceptsGraph
