import type { ReactNode } from 'react'
import { AiOutlineFileSearch } from 'react-icons/ai'
import { BiNetworkChart } from 'react-icons/bi'

import Tabs, { Tab } from '~/components/Tabs'
import { useDocumentsQuery } from '~/pages/SearchPageNew/contexts/documents-query-context'
import { ResultsTab } from '~/pages/SearchPageNew/types/tabs'

import usePinned from '~/hooks/use-pinned'

import StagedDocsButton from './components/StagedDocsButton'
import * as S from './styles'

type Props = {
  children: ReactNode
}

const SearchResultsTabs = ({ children }: Props) => {
  const { pinnedRef, isPinned } = usePinned()

  const { query } = useDocumentsQuery()

  return (
    <Tabs
      css={S.root}
      defaultValue={ResultsTab.DOCUMENTS}
      panels={children}
      tabsRef={pinnedRef}
      // Don't scroll to the top when the tab changes if there are no search terms
      noScroll={!query?.terms.length}
      storageKey="seta-search-tab"
      data-pinned={isPinned}
    >
      <div css={S.tabsContainer}>
        <Tab
          value={ResultsTab.DOCUMENTS}
          label="Documents"
          icon={<AiOutlineFileSearch css={S.documentsIcon} />}
          color="orange"
          rightElement={<StagedDocsButton />}
        />

        <Tab value={ResultsTab.CONCEPTS} label="Concepts" icon={<BiNetworkChart />} color="grape" />

        {/* TODO: Re-enable this 👇🏻 for the Validation tab */}

        {/* <Tab
          value={ResultsTab.VALIDATION}
          label="Validation"
          icon={<RiCheckDoubleLine />}
          color="teal"
        /> */}

        {/* <StagedDocsButton /> */}
      </div>
    </Tabs>
  )
}

export default SearchResultsTabs
