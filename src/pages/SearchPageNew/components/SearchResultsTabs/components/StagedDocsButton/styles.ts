import { css } from '@emotion/react'

export const root = css`
  [data-left] {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }

  [data-right] {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
  }
`

export const stagedDocs: ThemedCSS = theme => css`
  button {
    transition: border-radius 0.2s ease-out;

    &[data-readonly] {
      cursor: not-allowed;

      background-color: ${theme.colors.blue[0]};

      &:active {
        transform: none;
      }
    }

    .seta-Button-icon {
      margin-right: 0.5rem;
    }
  }

  &[data-no-results] {
    button {
      border-radius: ${theme.radius.sm};
    }
  }
`

export const addStagedWrapper = css`
  position: absolute;
  right: 100%;

  button {
    .seta-Button-icon {
      margin-right: 0.1rem;
    }
  }
`
