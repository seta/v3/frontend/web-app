import { useMemo } from 'react'
import { Button, Tooltip, Transition } from '@mantine/core'
import { usePrevious } from '@mantine/hooks'
import { IconMinus, IconPlus } from '@tabler/icons-react'

import { useSearchResults } from '~/pages/SearchPageNew/contexts/search-results-context'
import { useStagedDocuments } from '~/pages/SearchPageNew/contexts/staged-documents-context'

import type { Document } from '~/types/search/documents'
import type { StagedDocument } from '~/types/search/staged-documents'

import * as S from './styles'

import StagedDocsInfo from '../StagedDocsInfo'
import StagedDocsPopup from '../StagedDocsPopup'

/**
 * Filters out documents that are already staged.
 *
 * @param results - An array of documents to filter, or undefined
 * @param staged - An array of staged documents
 * @returns An array of documents that are not staged
 */
const getUnstagedDocs = (results: Document[] | undefined, staged: StagedDocument[]): Document[] => {
  return results?.filter(doc => !staged.some(stagedDoc => stagedDoc.id === doc._id)) ?? []
}

const StagedDocsButton = () => {
  const { stagedDocuments, stageMany, unstageMany } = useStagedDocuments()
  const { hasResults, results } = useSearchResults()

  const unstagedDocs = useMemo(
    () => getUnstagedDocs(results, stagedDocuments),
    [results, stagedDocuments]
  )

  const allStaged = results && unstagedDocs.length === 0

  const countToStage = allStaged ? results.length : unstagedDocs.length

  const icon = allStaged ? (
    <IconMinus size={16} strokeWidth={2} />
  ) : (
    <IconPlus size={16} strokeWidth={2} />
  )

  const prevCountToStage = usePrevious(countToStage)
  const prevIcon = usePrevious(icon)

  // Show the previous values while unmounting the button to prevent flickering
  const [countValue, iconValue] = hasResults ? [countToStage, icon] : [prevCountToStage, prevIcon]

  const stageTooltip = allStaged
    ? 'Unstage all documents on the page'
    : 'Stage remaining documents on the page'

  const handleStageUnstageRemaining = () => {
    if (allStaged) {
      if (results) {
        unstageMany(results)
      }
    } else {
      stageMany(unstagedDocs)
    }
  }

  return (
    <div css={S.root} data-no-results={!hasResults || undefined}>
      <Tooltip.Group openDelay={300}>
        <Transition transition="slide-left" mounted={hasResults}>
          {styles => (
            <div style={styles} css={S.addStagedWrapper}>
              <Tooltip label={stageTooltip}>
                <Button
                  variant="outline"
                  color="blue"
                  size="sm"
                  fz="md"
                  px="xs"
                  mr={-1}
                  leftIcon={iconValue}
                  data-left
                  onClick={handleStageUnstageRemaining}
                >
                  {countValue}
                </Button>
              </Tooltip>
            </div>
          )}
        </Transition>

        <StagedDocsPopup
          target={
            <div css={S.stagedDocs} data-no-results={!hasResults || undefined}>
              <StagedDocsInfo docs={stagedDocuments} />
            </div>
          }
        />
      </Tooltip.Group>
    </div>
  )
}

export default StagedDocsButton
