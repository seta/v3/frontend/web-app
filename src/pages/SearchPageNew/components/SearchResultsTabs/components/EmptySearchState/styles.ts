import { css } from '@emotion/react'

export const root: ThemedCSS = theme => css`
  margin-top: ${theme.spacing.xl};
  padding: 3rem;

  &[data-pad='true'] {
    padding: 4.25rem 3rem;
  }

  .icon {
    color: ${theme.colors.gray[4]};
    font-size: 3rem;
  }
`
