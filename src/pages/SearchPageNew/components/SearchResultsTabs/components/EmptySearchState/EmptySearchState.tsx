import type { ReactNode } from 'react'
import { Stack, Text } from '@mantine/core'
import { BiSearchAlt } from 'react-icons/bi'

import * as S from './styles'

type Props = {
  extraPadding?: boolean
  message?: ReactNode
}

const EmptySearchState = ({ extraPadding, message }: Props) => {
  const messageValue = message ?? (
    <span>
      Enter a few keywords and press <strong>Search</strong> to find documents.
    </span>
  )

  return (
    <Stack align="center" css={S.root} data-pad={extraPadding || undefined}>
      <BiSearchAlt className="icon" />

      <Text mt="md" align="center" fz="lg" color="dimmed" lh={1.6}>
        {messageValue}
      </Text>
    </Stack>
  )
}

export default EmptySearchState
