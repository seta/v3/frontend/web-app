import { Select } from '@mantine/core'
import { IconEye } from '@tabler/icons-react'

import * as S from './styles'

import GraphHistory from '../GraphHistory'

type Props = {
  searchTerms: string[]
  currentTerm: string
  history: string[]
  onTermChange?: (term: string) => void
  onHistoryNavigate?: (term: string) => void
}

const GraphNav = ({
  searchTerms,
  currentTerm,
  history,
  onTermChange,
  onHistoryNavigate
}: Props) => {
  return (
    <div css={S.container}>
      <Select
        data={searchTerms}
        value={currentTerm}
        searchable={searchTerms.length > 5}
        placeholder="Select a search keyword"
        icon={<IconEye />}
        onChange={onTermChange}
      />

      <GraphHistory history={history} onHistoryNavigate={onHistoryNavigate} />
    </div>
  )
}

export default GraphNav
