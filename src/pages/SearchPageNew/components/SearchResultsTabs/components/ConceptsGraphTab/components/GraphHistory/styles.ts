import { css } from '@emotion/react'

export const root = css`
  overflow: hidden;
  min-height: 1.5rem;
`

export const truncatedItem = css`
  overflow: hidden;
  text-overflow: ellipsis;
  transition: all 0.2s ease;
`
