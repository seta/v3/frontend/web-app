import { css } from '@emotion/react'

export const container: ThemedCSS = theme => css`
  --select-width: 250px;

  display: grid;
  grid-template-columns: var(--select-width) 1fr;
  align-items: center;
  gap: ${theme.spacing.sm};
  padding: ${theme.spacing.xs};
  border-bottom: 1px solid ${theme.colors.gray[3]};
`
