import { Loader, Text } from '@mantine/core'

type Props = {
  term?: string
}

const GraphLoader = ({ term }: Props) => {
  return (
    <div className="flex flex-col items-center gap-4">
      {term && (
        <Text size="xl" color="dark.3">
          {term} ...
        </Text>
      )}

      <Loader variant="bars" size="lg" />
    </div>
  )
}

export default GraphLoader
