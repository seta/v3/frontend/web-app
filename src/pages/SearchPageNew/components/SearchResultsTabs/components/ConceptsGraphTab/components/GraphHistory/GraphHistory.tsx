import { Anchor, Breadcrumbs, Text, Tooltip } from '@mantine/core'

import * as S from './styles'

type Props = {
  history: string[]
  onHistoryNavigate?: (term: string) => void
}

const GraphHistory = ({ history, onHistoryNavigate }: Props) => {
  if (history.length === 1) {
    return null
  }

  return (
    <Breadcrumbs css={S.root}>
      {history.map((term, index) => {
        if (index === history.length - 1) {
          return (
            <Text key={term} fw={500} c="dark.3">
              {term}
            </Text>
          )
        }

        const isTruncated = index < history.length - 3

        return (
          <Tooltip key={term} label={term} openDelay={500}>
            <Anchor
              component="button"
              css={isTruncated && S.truncatedItem}
              onClick={() => onHistoryNavigate?.(term)}
            >
              {term}
            </Anchor>
          </Tooltip>
        )
      })}
    </Breadcrumbs>
  )
}

export default GraphHistory
