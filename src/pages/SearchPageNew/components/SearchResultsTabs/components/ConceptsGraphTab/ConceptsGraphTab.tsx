import { useEffect, useMemo, useState } from 'react'
import { LoadingOverlay } from '@mantine/core'

import { TabPanel } from '~/components/Tabs'
import ConceptsGraph from '~/pages/SearchPageNew/components/ConceptsGraph'
import EmptySearchState from '~/pages/SearchPageNew/components/SearchResultsTabs/components/EmptySearchState'
import { useDocumentsQuery } from '~/pages/SearchPageNew/contexts/documents-query-context'
import type { GraphNode } from '~/pages/SearchPageNew/types/concepts-graph'
import { ResultsTab } from '~/pages/SearchPageNew/types/tabs'
import { buildNodesLinks } from '~/pages/SearchPageNew/utils/concepts-graph-utils'

import { useRelatedClusters } from '~/api/search/related-clusters'
import { notifications } from '~/utils/notifications'
import { STORAGE_KEY } from '~/utils/storage-keys'
import { storage } from '~/utils/storage-utils'

import GraphLoader from './components/GraphLoader'
import GraphNav from './components/GraphNav'
import useGraphHistory from './hooks/use-graph-history'
import * as S from './styles'

const searchStorage = storage<string>(STORAGE_KEY.SEARCH)

const ConceptsGraphTab = () => {
  const { query } = useDocumentsQuery()

  const searchTerms = useMemo(() => query?.terms ?? [], [query?.terms])

  const [currentTerm, setCurrentTerm] = useState(searchTerms[0])

  const { data, isLoading, error } = useRelatedClusters({ term: currentTerm })

  const { history, addToHistory, resetHistory, trimHistory } = useGraphHistory()

  // If there is a saved search, set the initial loading state
  const isInitialLoading = useMemo(
    () => !!searchStorage.read() && (searchTerms.length === 0 || currentTerm === undefined),
    [searchTerms, currentTerm]
  )

  // Memoize the graph data
  const graphData = useMemo(
    () => buildNodesLinks(currentTerm, data?.nodes),
    [currentTerm, data?.nodes]
  )

  useEffect(() => {
    // If the current term is undefined, set it to the first search term or last history item
    if (!currentTerm && searchTerms.length > 0) {
      if (searchTerms.includes(history[0])) {
        // If the first history item is in the search terms, the graph history is valid for the current search
        // -> set the current term to the last history item to maintain the whole history chain
        setCurrentTerm(history[history.length - 1])
      } else {
        // ... otherwise, set it to the first search term and reset the history
        setCurrentTerm(searchTerms[0])
        resetHistory(searchTerms[0])
      }
    }
  }, [currentTerm, resetHistory, searchTerms, history])

  useEffect(() => {
    if (error) {
      notifications.showError('There was an error loading the concepts graph.', {
        description: 'Please reload the page or try again later.'
      })
    }
  }, [error])

  const handleNodeClick = (node: GraphNode) => {
    const term = node.label

    setCurrentTerm(term)
    addToHistory(term)
  }

  const handleHistoryNavigate = (term: string) => {
    setCurrentTerm(term)
    trimHistory(term)
  }

  const handleSelectedTermChange = (term: string) => {
    setCurrentTerm(term)
    resetHistory(term)
  }

  const hasContent = isInitialLoading || (!!currentTerm && searchTerms.length > 0 && !error)

  const content = hasContent ? (
    <>
      <LoadingOverlay
        visible={isLoading || isInitialLoading}
        overlayOpacity={1}
        loader={<GraphLoader term={currentTerm} />}
      />

      <GraphNav
        searchTerms={searchTerms}
        currentTerm={currentTerm}
        history={history}
        onTermChange={handleSelectedTermChange}
        onHistoryNavigate={handleHistoryNavigate}
      />

      <ConceptsGraph data={graphData} onNodeClick={handleNodeClick} />
    </>
  ) : (
    <EmptySearchState message="Enter a search keyword to load the concepts graph." extraPadding />
  )

  return (
    <TabPanel value={ResultsTab.CONCEPTS} fullSize>
      <div css={S.container}>{content}</div>
    </TabPanel>
  )
}

export default ConceptsGraphTab
