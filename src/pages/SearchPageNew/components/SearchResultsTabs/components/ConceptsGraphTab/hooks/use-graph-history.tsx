import { useCallback } from 'react'
import { useLocalStorage } from '@mantine/hooks'

import { STORAGE_KEY } from '~/utils/storage-keys'

const useGraphHistory = () => {
  const [history, setHistory] = useLocalStorage<string[]>({
    key: STORAGE_KEY.GRAPH_HISTORY,
    defaultValue: [],
    getInitialValueInEffect: true
  })

  const addToHistory = useCallback(
    (term: string) => {
      setHistory(prev => [...prev, term])
    },
    [setHistory]
  )

  const trimHistory = useCallback(
    (term: string) => {
      const index = history.indexOf(term)
      const newHistory = history.slice(0, index + 1)

      setHistory(newHistory)
    },
    [history, setHistory]
  )

  const resetHistory = useCallback(
    (term: string) => {
      setHistory([term])
    },
    [setHistory]
  )

  return { history, addToHistory, resetHistory, trimHistory }
}

export default useGraphHistory
