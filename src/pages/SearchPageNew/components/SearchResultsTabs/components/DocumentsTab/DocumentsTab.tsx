import { useEffect, useMemo } from 'react'

import { TabPanel } from '~/components/Tabs'
import { SuggestionsLoading } from '~/pages/SearchPageNew/components/common'
import DocumentsList from '~/pages/SearchPageNew/components/documents/DocumentsList'
import EmptySearchState from '~/pages/SearchPageNew/components/SearchResultsTabs/components/EmptySearchState'
import { useDocumentsQuery } from '~/pages/SearchPageNew/contexts/documents-query-context'
import { useSearchResults } from '~/pages/SearchPageNew/contexts/search-results-context'
import { useUploadDocuments } from '~/pages/SearchPageNew/contexts/upload-documents-context'
import { ResultsTab } from '~/pages/SearchPageNew/types/tabs'

import { libraryEvents } from '~/utils/events/library-events'
import { STORAGE_KEY } from '~/utils/storage-keys'
import { storage } from '~/utils/storage-utils'

const searchStorage = storage<string>(STORAGE_KEY.SEARCH)
const uploadsStorage = storage<unknown[]>(STORAGE_KEY.UPLOADS)

type Props = {
  page: number
  searchPressed?: boolean
  onPageChange: (page: number) => void
}

const DocumentsTab = ({ page, searchPressed, onPageChange }: Props) => {
  const { query, searchOptions } = useDocumentsQuery()
  const { setResults } = useSearchResults()

  const { selectDocumentForSearchContext, documents } = useUploadDocuments()

  // If there is a saved search and the query is null, prepare the loading state
  const initialLoading = useMemo(
    () => query === null && (!!searchStorage.read() || !!uploadsStorage.read()?.length),
    [query]
  )

  // Show the documents results when the search is pressed, there is a query, or there are semantic documents/text provided
  const showDocuments = searchPressed || !!query?.value || documents.length > 0

  useEffect(() => {
    libraryEvents.on('findSimilarDocs', selectDocumentForSearchContext)

    return () => {
      libraryEvents.off('findSimilarDocs', selectDocumentForSearchContext)
    }
  }, [selectDocumentForSearchContext])

  // Hide the staged documents button when the search is cleared
  useEffect(() => {
    if (!showDocuments) {
      setResults(undefined)
    }
  }, [showDocuments, setResults])

  const documentsList = showDocuments && (
    <DocumentsList
      query={query?.value}
      terms={query?.terms ?? []}
      embeddings={query?.embeddings}
      searchOptions={searchOptions}
      page={page}
      onPageChange={onPageChange}
    />
  )

  const noDocuments =
    !showDocuments &&
    (initialLoading ? (
      // Replicate the loading state without rendering the DocumentsList
      // if there are documents to load based on the saved search
      <SuggestionsLoading size="lg" mt="5rem" color="blue" variant="bars" />
    ) : (
      <EmptySearchState />
    ))

  return (
    <TabPanel value={ResultsTab.DOCUMENTS}>
      {documentsList}
      {noDocuments}
    </TabPanel>
  )
}

export default DocumentsTab
