import type { MouseEventHandler } from 'react'
import { ActionIcon, Checkbox, Group, Text, Tooltip } from '@mantine/core'
import { CgFileDocument } from 'react-icons/cg'
import { MdClose } from 'react-icons/md'

import ColoredActionIcon from '~/components/ColoredActionIcon'
import { useUploadDocuments } from '~/pages/SearchPageNew/contexts/upload-documents-context'

import { usePopup } from '~/contexts/popup-context'
import QuickSearch from '~/icons/QuickSearch'
import type { StagedDocument } from '~/types/search/staged-documents'

import * as S from './styles'

type Props = {
  document: StagedDocument
  selected?: boolean
  onSelectChange?: (selected: boolean) => void
  onRemove?: () => void
}

const StagedDocInfo = ({ document, selected, onSelectChange, onRemove }: Props) => {
  const { closePopup } = usePopup()
  const { selectDocumentForSearchContext } = useUploadDocuments()

  const { title } = document

  const handleSelect = () => {
    onSelectChange?.(!selected)
  }

  const handleRemove: MouseEventHandler<HTMLButtonElement> = event => {
    event.stopPropagation()
    onRemove?.()
  }

  const handleQuickSearch = () => {
    if (document.originalDocument) {
      selectDocumentForSearchContext(document.originalDocument)

      closePopup()
    }
  }

  return (
    <div css={S.root} onClick={handleSelect}>
      <Checkbox css={S.checkbox} color="teal" checked={selected} onChange={handleSelect} />

      <Group spacing="xs" css={S.title}>
        <CgFileDocument css={S.icon} />

        <Text size="lg" style={{ flex: 1 }} truncate>
          {title}
        </Text>
      </Group>

      <Group spacing={4}>
        <ColoredActionIcon
          variant="subtle"
          color="teal"
          tooltip="Search for similar documents"
          onClick={handleQuickSearch}
        >
          <QuickSearch size={21} />
        </ColoredActionIcon>

        <Tooltip label="Remove from staged documents">
          <ActionIcon variant="subtle" color="gray" data-action="remove" onClick={handleRemove}>
            <MdClose />
          </ActionIcon>
        </Tooltip>
      </Group>
    </div>
  )
}

export default StagedDocInfo
