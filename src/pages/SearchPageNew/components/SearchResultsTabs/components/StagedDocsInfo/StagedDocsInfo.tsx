import { Button, Tooltip, clsx } from '@mantine/core'
import { FiCheck } from 'react-icons/fi'

import type { ClassAndStyleProps } from '~/types/children-props'
import type { StagedDocument } from '~/types/search/staged-documents'
import { pluralize } from '~/utils/string-utils'

type Props = {
  docs: StagedDocument[]
  onClick?: () => void
} & ClassAndStyleProps

const StagedDocsInfo = ({ className, style, docs, onClick, ...rest }: Props) => {
  const count = docs.length

  const label = `${count === 0 ? 'No' : count} ${pluralize('document', count)} staged`

  const classes = clsx(className, 'seta-StagedDocsInfo-root')

  return (
    <div className={classes} style={style} {...rest}>
      <Tooltip label={label}>
        <Button
          variant="outline"
          color="blue"
          size="sm"
          fz="sm"
          px="xs"
          bg="blue.0"
          leftIcon={<FiCheck size={22} />}
          data-right
          data-readonly={count === 0 || undefined}
          onClick={onClick}
        >
          {count}
        </Button>
      </Tooltip>
    </div>
  )
}

export default StagedDocsInfo
