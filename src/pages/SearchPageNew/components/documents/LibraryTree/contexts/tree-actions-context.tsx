import { createContext, useContext } from 'react'

import useExportModal from '~/pages/SearchPageNew/hooks/use-export-modal'

import { getChunk } from '~/api/search/chunk'
import type { ChildrenProp } from '~/types/children-props'
import { LibraryItemType, type LibraryItem } from '~/types/library/library-item'
import { libraryEvents } from '~/utils/events/library-events'
import { notifications } from '~/utils/notifications'

import { useItemLoading } from './item-loading-context'

import useDeleteModal from '../hooks/use-delete-modal'
import useMoveModal from '../hooks/use-move-modal'
import useRenameModal from '../hooks/use-rename-modal'

type TreeActionsContextProps = {
  renameFolder: (folder: LibraryItem) => void
  confirmDelete: (item: LibraryItem) => void
  moveItem: (item: LibraryItem) => void
  exportItem: (item: LibraryItem) => void
  findSimilarDocs: (item: LibraryItem) => void
  openOriginal: (item: LibraryItem) => void
}

const TreeActionsContext = createContext<TreeActionsContextProps | undefined>(undefined)

const TreeActionsProvider = ({ children }: ChildrenProp) => {
  const { renameFolder, renameModal } = useRenameModal()
  const { confirmDelete, confirmDeleteModal } = useDeleteModal()
  const { moveItem, moveModal } = useMoveModal()
  const { exportLibraryItem: exportItem, exportModal } = useExportModal()

  const { setIsLoading } = useItemLoading()

  const findSimilarDocs = async ({ type, source, documentId }: LibraryItem) => {
    if (type !== LibraryItemType.Document || !documentId) {
      return
    }

    if (!source) {
      notifications.showError(
        `Cannot find similar documents because the 'source' property of this document is missing.`,
        {
          description: 'Please search for the original document and save it again to your Library.'
        }
      )

      return
    }

    try {
      setIsLoading(true, documentId)

      const document = await getChunk({ source, id: documentId })

      if (document._id) {
        libraryEvents.emit('findSimilarDocs', document)
      } else {
        notifications.showError('This document is no longer available.')
      }
    } catch (error) {
      notifications.showError('There was an error finding similar documents. Please try again.')
    } finally {
      setIsLoading(false, documentId)
    }
  }

  const openOriginal = ({ link }: LibraryItem) => {
    if (!link) {
      return
    }

    // Prevent the new tab/window from having access to the originating window and referrer info
    window.open(link, '_blank', 'noreferrer=true')
  }

  const value: TreeActionsContextProps = {
    renameFolder,
    confirmDelete,
    moveItem,
    exportItem,
    findSimilarDocs,
    openOriginal
  }

  return (
    <>
      <TreeActionsContext.Provider value={value}>{children}</TreeActionsContext.Provider>

      {renameModal}
      {confirmDeleteModal}
      {moveModal}
      {exportModal}
    </>
  )
}

export const useTreeActions = () => {
  const context = useContext(TreeActionsContext)

  if (context === undefined) {
    throw new Error('useTreeActions must be used within a TreeActionsProvider')
  }

  return context
}

// Must be exported default to work with React.lazy
export default TreeActionsProvider
