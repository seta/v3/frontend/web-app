import { createContext, useContext, useState } from 'react'

import type { ChildrenProp } from '~/types/children-props'

type ItemLoadingContextProps = {
  setIsLoading: (value: boolean, id: string | undefined) => void
  isLoading: (id: string | undefined) => boolean
}

const ItemLoadingContext = createContext<ItemLoadingContextProps | undefined>(undefined)

export const ItemLoadingProvider = ({ children }: ChildrenProp) => {
  const [loading, setLoading] = useState<{ [key: string]: boolean }>({})

  const setIsLoading = (value: boolean, id: string | undefined) => {
    if (!id) {
      return
    }

    setLoading(prev => ({ ...prev, [id]: value }))
  }

  const isLoading = (id: string | undefined) => (id ? !!loading[id] : false)

  const value = {
    setIsLoading,
    isLoading
  }

  return <ItemLoadingContext.Provider value={value}>{children}</ItemLoadingContext.Provider>
}

export const useItemLoading = () => {
  const context = useContext(ItemLoadingContext)

  if (context === undefined) {
    throw new Error('useItemLoading must be used within an ItemLoadingProvider')
  }

  return context
}
