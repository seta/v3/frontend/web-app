import { useState } from 'react'
import { Menu } from '@mantine/core'
import { AiFillDelete, AiFillEdit } from 'react-icons/ai'
import { BsBoxArrowInUpRight, BsThreeDotsVertical } from 'react-icons/bs'
import { FiCornerUpRight, FiExternalLink } from 'react-icons/fi'

import ActionIconMenu from '~/components/ActionIconMenu/ActionIconMenu'
import { TOOLTIP_THRESHOLD } from '~/pages/SearchPageNew/components/documents/LibraryTree/constants'
import { useTreeActions } from '~/pages/SearchPageNew/components/documents/LibraryTree/contexts/tree-actions-context'

import QuickSearch from '~/icons/QuickSearch'
import type { LibraryItem } from '~/types/library/library-item'
import { LibraryItemType } from '~/types/library/library-item'

const ICON_SIZE = 20

type Props = {
  item: LibraryItem
  isLoading?: boolean
  onMenuChange?: (open: boolean) => void
}

const OptionsMenuAction = ({ item, isLoading, onMenuChange }: Props) => {
  const [isOpen, setIsOpen] = useState(false)

  const { renameFolder, confirmDelete, moveItem, exportItem, findSimilarDocs, openOriginal } =
    useTreeActions()

  const isFolder = item.type === LibraryItemType.Folder
  const subject = isFolder ? 'folder' : 'document'

  return (
    <ActionIconMenu
      action={{
        icon: <BsThreeDotsVertical size={18} strokeWidth={0.5} />,
        color: isLoading ? 'blue' : 'gray.7',
        // Hide the actions tooltip if the item itself has a tooltip
        tooltip: item.title.length < TOOLTIP_THRESHOLD ? 'Actions' : undefined,
        loading: isLoading,
        active: isLoading,
        onClick: () => setIsOpen(true)
      }}
      opened={isOpen}
      onChange={onMenuChange}
      onOpen={() => onMenuChange?.(true)}
      onClose={() => onMenuChange?.(false)}
    >
      <Menu.Item icon={<FiCornerUpRight size={ICON_SIZE} />} onClick={() => exportItem(item)}>
        Export metadata
      </Menu.Item>

      {!isFolder && (
        <>
          <Menu.Divider />

          <Menu.Item icon={<QuickSearch size={ICON_SIZE} />} onClick={() => findSimilarDocs(item)}>
            Find similar documents
          </Menu.Item>

          <Menu.Item
            icon={<FiExternalLink size={ICON_SIZE} strokeWidth={2} />}
            onClick={() => openOriginal(item)}
            disabled={!item.link}
          >
            Open original document
          </Menu.Item>
        </>
      )}

      <Menu.Divider />

      {isFolder && (
        <Menu.Item icon={<AiFillEdit size={ICON_SIZE} />} onClick={() => renameFolder(item)}>
          Rename folder
        </Menu.Item>
      )}

      <Menu.Item
        icon={<BsBoxArrowInUpRight strokeWidth={0.5} size={ICON_SIZE} />}
        onClick={() => moveItem(item)}
      >
        Move {subject}
      </Menu.Item>

      <Menu.Item
        icon={<AiFillDelete size={ICON_SIZE} />}
        color="red"
        onClick={() => confirmDelete(item)}
      >
        {isFolder ? 'Delete folder' : 'Remove document'}
      </Menu.Item>
    </ActionIconMenu>
  )
}

export default OptionsMenuAction
