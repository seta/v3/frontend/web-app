import type { ReactElement } from 'react'
import { css } from '@emotion/react'
import { Collapse, Loader } from '@mantine/core'

import ChevronToggleIcon from '~/components/ChevronToggleIcon'

import type { LibraryItem } from '~/types/library/library-item'
import { LibraryItemType } from '~/types/library/library-item'

import EmptyState from '../components/EmptyState'
import { FILE_ICON, FOLDER_ICON, FOLDER_ICON_OPEN, ROOT_ICON } from '../constants'
import { useItemLoading } from '../contexts/item-loading-context'

type Args = {
  item: LibraryItem
  isRoot: boolean | undefined
  isExpanded: boolean
  foldersOnly?: boolean
  folderContent: ReactElement[]
}

const toggleIconStyle = css`
  grid-column: 1;
`

const useNodeContent = ({ item, isRoot, isExpanded, foldersOnly, folderContent }: Args) => {
  const { isLoading } = useItemLoading()

  const isItemLoading = isLoading(item.documentId)

  const isFolder = item.type === LibraryItemType.Folder
  const isEmpty = folderContent.length === 0

  const content = isFolder && (
    <Collapse in={isExpanded}>
      {isRoot && isEmpty ? (
        <EmptyState foldersOnly={foldersOnly} noArrow={foldersOnly} centerMessage={foldersOnly} />
      ) : (
        folderContent
      )}
    </Collapse>
  )

  const toggleIcon = isFolder && (
    <ChevronToggleIcon
      start="right"
      end="down"
      size="sm"
      toggled={isExpanded}
      css={toggleIconStyle}
    />
  )

  const icon = (() => {
    if (isItemLoading) {
      return <Loader size="sm" />
    }

    if (isRoot) {
      return ROOT_ICON
    }

    if (isFolder) {
      return isExpanded ? FOLDER_ICON_OPEN : FOLDER_ICON
    }

    return FILE_ICON
  })()

  return {
    content,
    toggleIcon,
    icon,
    isItemLoading
  }
}

export default useNodeContent
