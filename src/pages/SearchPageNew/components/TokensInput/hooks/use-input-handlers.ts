import type {
  ChangeEvent,
  Dispatch,
  FocusEventHandler,
  KeyboardEvent,
  KeyboardEventHandler,
  MouseEventHandler,
  SetStateAction,
  UIEvent,
  RefObject
} from 'react'
import { useRef } from 'react'
import type { TextInputProps } from '@mantine/core'

type Args = {
  inputRef: RefObject<HTMLInputElement>
  rendererRef: RefObject<HTMLDivElement>
  updateCurrentToken: () => void
  setIsTyping: Dispatch<SetStateAction<boolean>>
  setFocused: Dispatch<SetStateAction<boolean>>
  onBlurContext?: () => void
  onChange?: (value: string) => void
  onKeyDown?: TextInputProps['onKeyDown']
  onKeyUp?: TextInputProps['onKeyUp']
  onMouseUp?: TextInputProps['onMouseUp']
  onFocus?: TextInputProps['onFocus']
  onBlur?: TextInputProps['onBlur']
  onScroll?: TextInputProps['onScroll']
}

const useInputHandlers = ({
  inputRef,
  rendererRef,
  updateCurrentToken,
  setIsTyping,
  setFocused,
  onBlurContext,
  onChange,
  onKeyDown,
  onKeyUp,
  onMouseUp,
  onFocus,
  onBlur,
  onScroll
}: Args) => {
  const timeoutRef = useRef<number | null>(null)

  const syncScroll = (scroll: number) => {
    if (!rendererRef.current) {
      return
    }

    rendererRef.current.scrollLeft = scroll
  }

  const syncScrollDelayed = () => {
    setTimeout(() => {
      syncScroll(inputRef.current?.scrollLeft ?? 0)
    }, 100)
  }

  const markTyping = (e: KeyboardEvent<HTMLInputElement>) => {
    if (
      (e.key.length !== 1 && e.key !== 'Backspace' && e.key !== 'Delete') ||
      e.ctrlKey ||
      e.metaKey
    ) {
      return
    }

    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current)
    }

    setIsTyping(true)

    timeoutRef.current = setTimeout(() => {
      setIsTyping(false)
      syncScroll(inputRef.current?.scrollLeft ?? 0)
    }, 200)
  }

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value
      // Prevent pasting of parentheses
      .replace(/\(|\)/g, '')
      // Prevent pasting of new line characters
      .replace(/\n/g, '')

    onChange?.(value)

    // Sync renderer scroll position in case of pasting long text
    syncScrollDelayed()
  }

  const handleKeyDown: KeyboardEventHandler<HTMLInputElement> = e => {
    const ignoreKeys = ['ArrowUp', 'ArrowDown', '(', ')']

    if (ignoreKeys.includes(e.key)) {
      e.preventDefault()

      return
    }

    const position = e.currentTarget.selectionStart ?? 0
    const val = e.currentTarget.value

    if (
      e.key === ' ' &&
      (val.slice(position - 2, position) === '  ' ||
        val.slice(position, position + 2) === '  ' ||
        val.slice(position - 1, position + 1) === '  ')
    ) {
      e.preventDefault()

      return
    }

    markTyping(e)

    onKeyDown?.(e)
  }

  const handleKeyUp: KeyboardEventHandler<HTMLInputElement> = e => {
    const keys = ['ArrowLeft', 'ArrowRight', 'Meta', 'Home', 'End']

    if (keys.includes(e.key)) {
      updateCurrentToken()
    }

    onKeyUp?.(e)
  }

  const handleMouseUp: MouseEventHandler<HTMLInputElement> = e => {
    updateCurrentToken()
    onMouseUp?.(e)
  }

  const handleFocus: FocusEventHandler<HTMLInputElement> = e => {
    setFocused(true)
    onFocus?.(e)
  }

  const handleBlur: FocusEventHandler<HTMLInputElement> = e => {
    setFocused(false)
    onBlur?.(e)
    onBlurContext?.()
  }

  const handleScroll = (e: UIEvent<HTMLInputElement>) => {
    syncScroll(e.currentTarget.scrollLeft)
    onScroll?.(e)
  }

  return {
    inputRef,
    rendererRef,
    handleChange,
    handleKeyDown,
    handleKeyUp,
    handleMouseUp,
    handleFocus,
    handleBlur,
    handleScroll
  }
}

export default useInputHandlers
