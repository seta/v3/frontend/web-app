import { Menu } from '@mantine/core'
import {
  IconDeviceFloppy,
  IconListSearch,
  IconPaperclip,
  IconSearch,
  IconTrashX
} from '@tabler/icons-react'
import { BsThreeDotsVertical } from 'react-icons/bs'

import ActionIconMenu from '~/components/ActionIconMenu'
import { useFilters } from '~/pages/SearchPageNew/contexts/filters-context'
import { useSearchValue } from '~/pages/SearchPageNew/contexts/search-value-context'
import { useUploadDocuments } from '~/pages/SearchPageNew/contexts/upload-documents-context'
import useSaveSearchModal from '~/pages/SearchPageNew/hooks/use-save-search-modal'
import { hasFiltersChanges } from '~/pages/SearchPageNew/utils/filters-utils'

import useThemeColor from '~/hooks/use-theme-color'
import { searchEvents } from '~/utils/events/search-events'

const ICON_SIZE = 20

type Args = {
  onClearSearch?: (focus?: boolean) => void
  onMenuOpen?: () => void
}

const useInputActionsMenu = ({ onClearSearch, onMenuOpen }: Args) => {
  const { getThemeColor } = useThemeColor()
  const red = getThemeColor('red.5')

  const { searchValue } = useSearchValue()
  const { state: filtersState, dispatchAndApply } = useFilters()
  const { removeAll } = useUploadDocuments()

  const { saveSearch, saveSearchModal } = useSaveSearchModal({
    searchValue,
    filtersState
  })

  // Only enable the save option if there are tokens, embeddings, or filters
  const canSaveSearch =
    !!searchValue.value || searchValue.embeddings?.length || hasFiltersChanges(filtersState)

  const handleClearDocuments = () => {
    removeAll()
  }

  const handleClearFilters = () => {
    dispatchAndApply({ type: 'clear_all' })
  }

  const handleClearAll = () => {
    // Execute with a timeout to allow the menu to update
    setTimeout(async () => {
      onClearSearch?.()
      handleClearDocuments()

      searchEvents.emit('clearQuery', undefined)

      dispatchAndApply({ type: 'clear_all' })
    }, 10)
  }

  const handleMenuChange = (opened: boolean) => {
    if (opened) {
      onMenuOpen?.()
    }
  }

  const actionsMenu = (
    <ActionIconMenu
      offset={4}
      zIndex={400}
      action={{
        icon: <BsThreeDotsVertical size={18} strokeWidth={0.5} />,
        color: 'gray.7'
      }}
      onChange={handleMenuChange}
    >
      <Menu.Item
        icon={<IconSearch size={ICON_SIZE} color={red} />}
        onClick={() => onClearSearch?.(true)}
      >
        Clear keywords
      </Menu.Item>

      <Menu.Item
        icon={<IconPaperclip size={ICON_SIZE} color={red} />}
        onClick={handleClearDocuments}
      >
        Clear attachments
      </Menu.Item>

      <Menu.Item
        icon={<IconListSearch size={ICON_SIZE} color={red} />}
        onClick={handleClearFilters}
      >
        Clear filters
      </Menu.Item>

      <Menu.Item icon={<IconTrashX size={ICON_SIZE} color={red} />} onClick={handleClearAll}>
        Clear all
      </Menu.Item>

      <Menu.Divider />

      <Menu.Item
        icon={<IconDeviceFloppy size={ICON_SIZE + 2} />}
        disabled={!canSaveSearch}
        onClick={saveSearch}
      >
        {canSaveSearch ? 'Save search' : 'No search to save'}
      </Menu.Item>
    </ActionIconMenu>
  )

  return { actionsMenu, saveSearchModal }
}

export default useInputActionsMenu
