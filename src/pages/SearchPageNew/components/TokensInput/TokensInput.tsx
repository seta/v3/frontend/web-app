import { useEffect, useImperativeHandle, forwardRef, useRef, useState } from 'react'
import type { TextInputProps } from '@mantine/core'
import { Box, TextInput, Text, clsx } from '@mantine/core'

import useInputActionsMenu from '~/pages/SearchPageNew/components/TokensInput/hooks/use-input-actions-menu'
import useInputHandlers from '~/pages/SearchPageNew/components/TokensInput/hooks/use-input-handlers'
import { useSearchInput } from '~/pages/SearchPageNew/contexts/search-input-context'

import TokensInfo from './components/TokensInfo'
import useTokens from './hooks/use-tokens'
import * as S from './styles'

type Props = Omit<TextInputProps, 'onChange'> & {
  enrichQuery?: boolean
  onChange?: (value: string) => void
  onActionsMenuOpen?: () => void
}

const TokensInput = forwardRef<HTMLInputElement, Props>(
  (
    {
      className,
      enrichQuery,
      value,
      onActionsMenuOpen,
      onChange,
      onKeyDown,
      onKeyUp,
      onMouseUp,
      onFocus,
      onBlur,
      onScroll,
      ...props
    },
    ref
  ) => {
    const [focused, setFocused] = useState(false)
    const [isTyping, setIsTyping] = useState(false)

    const inputRef = useRef<HTMLInputElement>(null)
    const rendererRef = useRef<HTMLDivElement>(null)

    useImperativeHandle(ref, () => inputRef.current as HTMLInputElement)

    const { updateCurrentToken, renderTokens, tokens } = useTokens(String(value), inputRef, focused)
    const { setInputRef, onBlur: onBlurContext } = useSearchInput()

    useEffect(() => {
      if (inputRef.current) {
        setInputRef(inputRef.current)
      }
    }, [setInputRef])

    const inputEvents = {
      onChange,
      onKeyDown,
      onKeyUp,
      onMouseUp,
      onFocus,
      onBlur,
      onScroll
    }

    const {
      handleChange,
      handleKeyDown,
      handleKeyUp,
      handleMouseUp,
      handleFocus,
      handleBlur,
      handleScroll
    } = useInputHandlers({
      inputRef,
      rendererRef,
      updateCurrentToken,
      setIsTyping,
      setFocused,
      onBlurContext,
      ...inputEvents
    })

    const handleClear = (focus = false) => {
      onChange?.('')
      updateCurrentToken()

      if (focus) {
        inputRef.current?.focus()
      }
    }

    const { actionsMenu, saveSearchModal } = useInputActionsMenu({
      onClearSearch: handleClear,
      onMenuOpen: onActionsMenuOpen
    })

    return (
      <Box className={className} css={S.container}>
        <TextInput
          ref={inputRef}
          css={S.input}
          size="md"
          rightSection={actionsMenu}
          spellCheck={false}
          value={value}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
          onKeyUp={handleKeyUp}
          onMouseUp={handleMouseUp}
          onFocus={handleFocus}
          onBlur={handleBlur}
          onScroll={handleScroll}
          {...props}
        />
        <Text
          ref={rendererRef}
          css={S.renderer}
          className={clsx('renderer', { focused, typing: isTyping })}
        >
          {renderTokens()}
        </Text>

        <div css={S.bg} />

        <TokensInfo tokens={tokens} enrichQuery={enrichQuery} />

        {saveSearchModal}
      </Box>
    )
  }
)

export default TokensInput
