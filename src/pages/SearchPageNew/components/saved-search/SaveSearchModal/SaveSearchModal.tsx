import { useEffect, useState } from 'react'
import type { ModalProps } from '@mantine/core'
import { Stack, Text, Button, Group } from '@mantine/core'
import { IconSearch } from '@tabler/icons-react'

import CancelButton from '~/components/CancelButton'
import ScrollModal from '~/components/ScrollModal'
import { FAV_GROUP_ID } from '~/pages/SearchPageNew/hooks/use-saved-search-groups'

import type { SavedSearchCreate } from '~/types/saved-search/saved-search'

import SaveSearchModalContent from './components/SaveSearchModalContent'

type Props = ModalProps & {
  savedSearchCreate: SavedSearchCreate
  saving?: boolean
  saveError?: string
  onSave?: (value: SavedSearchCreate) => void
}

const SaveSearchModal = ({
  savedSearchCreate,
  saving,
  saveError,
  opened,
  onSave,
  onClose,
  ...modalProps
}: Props) => {
  const [innerValue, setInnerValue] = useState(savedSearchCreate)

  useEffect(() => {
    setInnerValue(savedSearchCreate)
  }, [savedSearchCreate])

  const handleNameChange = (value: string) => {
    setInnerValue({
      ...innerValue,
      name: value
    })
  }

  const handleSelectedGroupChange = (groupId: string) => {
    setInnerValue({
      ...innerValue,
      // Set the parent ID to null if the group is the favorites group
      parentId: groupId === FAV_GROUP_ID ? null : groupId
    })
  }

  const handleSave = () => {
    onSave?.(innerValue)
  }

  const actions = (
    <Group spacing="sm">
      <CancelButton onClick={onClose} />

      <Button color="blue" onClick={handleSave} loading={saving}>
        Save Search
      </Button>
    </Group>
  )

  const titleElement = (
    <Stack spacing={2}>
      <Text>Save Search</Text>

      <Text size="sm" color="gray">
        Edit the name and select the group for this search.
      </Text>
    </Stack>
  )

  const icon = <IconSearch size={26} />

  return (
    <ScrollModal
      {...modalProps}
      opened={opened}
      icon={icon}
      title={titleElement}
      actions={actions}
      onClose={onClose}
    >
      <SaveSearchModalContent
        savedSearchCreate={savedSearchCreate}
        saveError={saveError}
        onNameChange={handleNameChange}
        onSelectedGroupChange={handleSelectedGroupChange}
      />
    </ScrollModal>
  )
}

export default SaveSearchModal
