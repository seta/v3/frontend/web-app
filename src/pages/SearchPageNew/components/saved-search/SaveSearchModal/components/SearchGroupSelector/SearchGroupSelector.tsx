import { useEffect, useRef, useState } from 'react'
import { Button, Stack, UnstyledButton } from '@mantine/core'
import { IconPlus } from '@tabler/icons-react'

import { SuggestionsError, SuggestionsLoading } from '~/pages/SearchPageNew/components/common'
import useSavedSearchGroups, {
  FAV_GROUP_ID
} from '~/pages/SearchPageNew/hooks/use-saved-search-groups'

import * as S from './styles'

import NewGroupPopover from '../NewGroupPopover'

type Props = {
  onChange?: (groupId: string) => void
}

const SearchGroupSelector = ({ onChange }: Props) => {
  const [selectedGroupId, setSelectedGroupId] = useState<string>(FAV_GROUP_ID)

  const { data, isLoading, error, refetch } = useSavedSearchGroups()

  const onChangeRef = useRef(onChange)

  useEffect(() => {
    onChangeRef.current?.(selectedGroupId)
  }, [selectedGroupId])

  if (isLoading || !data) {
    return <SuggestionsLoading size="md" color="blue" />
  }

  if (error) {
    return <SuggestionsError size="md" subject="the search groups" withIcon onTryAgain={refetch} />
  }

  const groups = data.groups

  const isSelected = (groupId: string) => groupId === selectedGroupId || undefined

  return (
    <Stack>
      <div css={S.groups}>
        {groups.map(({ id, name, icon }) => (
          <UnstyledButton
            key={id}
            css={S.item}
            data-selected={isSelected(id)}
            onClick={() => setSelectedGroupId(id)}
          >
            {icon}

            <div>{name}</div>
          </UnstyledButton>
        ))}
      </div>

      <NewGroupPopover
        target={({ toggle, isLoading: newGroupLoading }) => (
          <Button
            size="xs"
            variant="default"
            leftIcon={<IconPlus size={18} strokeWidth={3} />}
            css={S.addButton}
            onClick={toggle}
            loading={newGroupLoading}
          >
            New group
          </Button>
        )}
        onGroupCreated={value => setSelectedGroupId(value.id)}
      />
    </Stack>
  )
}

export default SearchGroupSelector
