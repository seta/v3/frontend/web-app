import { css } from '@emotion/react'

export const groups: ThemedCSS = theme => css`
  display: flex;
  flex-direction: column;
  gap: ${theme.spacing.sm};
  padding: ${theme.spacing.md};
  background-color: ${theme.white};
  border: 1px solid ${theme.colors.gray[3]};
  border-radius: ${theme.radius.sm};
`

export const item: ThemedCSS = theme => css`
  display: grid;
  grid-template-columns: 20px 1fr;
  align-items: center;
  gap: ${theme.spacing.xs};
  padding: ${theme.spacing.xs} ${theme.spacing.sm};
  border-radius: ${theme.radius.sm};
  border: 1px solid transparent;
  cursor: pointer;

  svg {
    color: ${theme.colors.gray[7]};
  }

  &:hover {
    background-color: ${theme.colors.gray[1]};
  }

  &:active:not([data-selected]) {
    transform: translateY(1px);
  }

  &[data-selected] {
    background-color: ${theme.colors.blue[0]};
    border: 1px solid ${theme.colors.blue[2]};
  }
`

export const addButton: ThemedCSS = theme => css`
  align-self: flex-end;

  .seta-Button-leftIcon {
    margin-right: 0.4rem;
    color: ${theme.colors.gray[7]};
  }
`
