import type { MouseEventHandler, ReactNode } from 'react'
import { useState } from 'react'
import { ActionIcon, Group, Popover, TextInput } from '@mantine/core'
import { FaCheck } from 'react-icons/fa'

import { useCreateGroupMutation } from '~/api/search/saved-searches'
import type { SavedSearch } from '~/types/saved-search/saved-search'
import { notifications } from '~/utils/notifications'

import * as S from './styles'

type TargetArgs = {
  isLoading: boolean
  toggle: () => void
}

type Props = {
  target: (args: TargetArgs) => ReactNode
  onGroupCreated?: (data: SavedSearch) => void
}

// TODO: Extract as a reusable component and use for both SearchGroupSelector & ActionIconPopover
const NewGroupPopover = ({ target, onGroupCreated }: Props) => {
  const [isOpen, setIsOpen] = useState(false)
  const [inputValue, setInputValue] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const { mutateAsync: createNewGroupAsync } = useCreateGroupMutation()

  const isValid = inputValue.trim().length > 0

  const submitNewGroup = async () => {
    setIsOpen(false)
    setIsLoading(true)

    await createNewGroupAsync(inputValue.trim(), {
      onSuccess: data => {
        onGroupCreated?.(data)
      },

      onError: () => {
        setIsOpen(true)
        setInputValue(inputValue)

        notifications.showError('Failed to create a new group.')
      },

      onSettled: () => {
        setIsLoading(false)
      }
    })
  }

  const handleSaveClick: MouseEventHandler<HTMLButtonElement> = e => {
    e.stopPropagation()
    submitNewGroup()
  }

  const handleInputChange: React.ChangeEventHandler<HTMLInputElement> = e => {
    setInputValue(e.currentTarget.value)
  }

  const handleInputKeyDown: React.KeyboardEventHandler<HTMLInputElement> = e => {
    if (e.key === 'Enter') {
      if (!isValid) {
        e.preventDefault()

        return
      }

      submitNewGroup()
    }
  }

  const handleClose = () => {
    setTimeout(() => {
      setInputValue('')
    }, 200)
  }

  const toggle = () => setIsOpen(prev => !prev)

  return (
    <Popover
      shadow="sm"
      position="bottom-end"
      trapFocus
      opened={isOpen}
      onChange={setIsOpen}
      onClose={handleClose}
    >
      <Popover.Target>
        <div css={S.targetWrapper}>{target({ isLoading, toggle })}</div>
      </Popover.Target>

      <Popover.Dropdown css={S.popover}>
        <Group>
          <TextInput
            placeholder="New search group"
            size="md"
            color="blue"
            css={S.input}
            autoFocus
            value={inputValue}
            onChange={handleInputChange}
            onKeyDown={handleInputKeyDown}
          />

          <ActionIcon
            css={S.saveButton}
            color="blue"
            variant="light"
            disabled={!isValid}
            onClick={handleSaveClick}
          >
            <FaCheck size={18} />
          </ActionIcon>
        </Group>
      </Popover.Dropdown>
    </Popover>
  )
}

export default NewGroupPopover
