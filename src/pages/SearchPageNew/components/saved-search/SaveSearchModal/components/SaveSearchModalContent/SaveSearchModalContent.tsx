import type { ChangeEvent } from 'react'
import { useEffect, useRef, useState } from 'react'
import { Stack, TextInput } from '@mantine/core'
import { ImArrowDown2 } from 'react-icons/im'

import ClosableAlert from '~/components/ClosableAlert'

import type { SavedSearchCreate } from '~/types/saved-search/saved-search'

import * as S from './styles'

import SearchGroupSelector from '../SearchGroupSelector'

type Props = {
  savedSearchCreate: SavedSearchCreate
  saveError?: string
  modalOpen?: boolean
  onNameChange: (value: string) => void
  onSelectedGroupChange: (groupId: string) => void
}

const SaveSearchModalContent = ({
  savedSearchCreate,
  saveError,
  modalOpen,
  onNameChange,
  onSelectedGroupChange
}: Props) => {
  const [name, setName] = useState(savedSearchCreate.name)

  const inputRef = useRef<HTMLInputElement>(null)

  useEffect(() => {
    // Focus the input and select the text when the modal opens
    const timeout = setTimeout(() => {
      inputRef.current?.focus()
      inputRef.current?.select()
    }, 200)

    return () => clearTimeout(timeout)
  }, [modalOpen])

  const handleNameChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value

    setName(value)
    onNameChange(value)
  }

  const errorAlert = !!saveError && (
    <ClosableAlert title="Error saving the search" color="red" variant="outline" mb="sm">
      {saveError}
      <br />
      Please try again.
    </ClosableAlert>
  )

  return (
    <div css={S.root}>
      {errorAlert}

      <Stack align="center" spacing="sm" mb="sm">
        <TextInput
          ref={inputRef}
          css={S.input}
          w="100%"
          value={name}
          onChange={handleNameChange}
          placeholder="Enter a name for this search"
          required
        />

        <ImArrowDown2 size={18} css={S.arrowDown} />
      </Stack>

      <SearchGroupSelector onChange={onSelectedGroupChange} />
    </div>
  )
}

export default SaveSearchModalContent
