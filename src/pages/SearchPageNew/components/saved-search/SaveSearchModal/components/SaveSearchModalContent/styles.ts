import { css } from '@emotion/react'

import { slideDown } from '~/styles/keyframe-animations'

export const root = css`
  min-height: 40vh;

  .seta-SuggestionsError-root,
  .seta-SuggestionsLoading-root {
    padding-top: 3rem;
  }
`

export const input = css`
  .seta-Input-input {
    height: 2.6rem;
  }
`

export const arrowDown: ThemedCSS = theme => css`
  color: ${theme.colors.gray[7]};
  opacity: 0;
  transform: translateY(-100%);
  animation: ${slideDown} 500ms ease forwards;
`
