import { useState } from 'react'
import { Collapse, Loader, Text } from '@mantine/core'

import ChevronToggleIcon from '~/components/ChevronToggleIcon'
import ClearButton from '~/components/ClearButton'
import { FAV_GROUP_ID } from '~/pages/SearchPageNew/hooks/use-saved-search-groups'

import { SavedSearchType } from '~/types/saved-search/saved-search'
import type {
  SearchGroupWithIcon,
  SearchItem,
  SerializedSearch
} from '~/types/saved-search/saved-search'

import EmptySearchItems from './components/EmptySearchItems'
import { FOLDER_OPEN_ICON, ITEM_ICON } from './constants'
import * as S from './styles'

type Props = {
  item: SearchItem | SearchGroupWithIcon
  loadingId?: string
  onApply?: (search: SerializedSearch | undefined) => void
  onDelete?: (id: string) => void
}

const SavedSearchItem = ({ item, loadingId, onApply, onDelete }: Props) => {
  const [isExpanded, setIsExpanded] = useState(false)

  const isFav = item.id === FAV_GROUP_ID
  const isGroup = item.type === SavedSearchType.Folder
  const isTopLevel = isGroup && !item.parentId

  const isLoading = loadingId === item.id

  const handleClick = () => {
    if (item.type === SavedSearchType.Folder) {
      setIsExpanded(prev => !prev)

      return
    }

    if (isLoading) {
      return
    }

    onApply?.(item.search)
  }

  const handleDelete = () => {
    if (isLoading) {
      return
    }

    onDelete?.(item.id)
  }

  const action = isLoading ? (
    <Loader size="sm" />
  ) : (
    !isFav && <ClearButton hoverColor="red" hoverVariant="filled" onClick={handleDelete} />
  )

  const icon = (() => {
    if (isGroup) {
      if (isFav) {
        return item.icon
      }

      return isExpanded ? FOLDER_OPEN_ICON : item.icon
    }

    return ITEM_ICON
  })()

  const toggleIcon = isGroup && (
    <ChevronToggleIcon start="right" end="down" size="sm" toggled={isExpanded} />
  )

  const content = isGroup && (
    <Collapse in={isExpanded}>
      {item.children.length > 0 ? (
        item.children.map(child => (
          <SavedSearchItem
            key={child.id}
            item={child as SearchItem}
            loadingId={loadingId}
            onApply={onApply}
            onDelete={onDelete}
          />
        ))
      ) : (
        <EmptySearchItems />
      )}
    </Collapse>
  )

  return (
    <div css={S.node} data-top={isTopLevel || undefined}>
      <div
        css={S.item}
        onClick={handleClick}
        data-disabled={isLoading || undefined}
        data-group={isGroup || undefined}
      >
        {toggleIcon}
        <div css={S.icon}>{icon}</div>

        <Text size={isGroup ? 'md' : 'sm'}>{item.name}</Text>

        {action}
      </div>

      {content}
    </div>
  )
}

export default SavedSearchItem
