import { css } from '@emotion/react'

export const node = css`
  margin-left: 26px;

  &[data-top] {
    margin-left: 0;
  }
`

export const item: ThemedCSS = theme => css`
  position: relative;
  display: grid;
  grid-template-columns: 20px 1fr auto;
  align-items: center;
  gap: 6px;
  padding: ${theme.spacing.xs};
  border-radius: ${theme.radius.sm};
  border: 1px solid transparent;
  cursor: pointer;

  &:hover {
    background-color: ${theme.colors.gray[1]};
  }

  &:active {
    transform: translateY(1px);
  }

  &[data-group] {
    grid-template-columns: 14px 20px 1fr auto;
  }

  &[data-disabled] {
    background-color: ${theme.colors.gray[1]};
    opacity: 0.6;
    pointer-events: none;
  }
`

export const icon: ThemedCSS = theme => css`
  color: ${theme.colors.gray[6]};
  font-size: 1.15rem;
  line-height: 0;
  justify-self: center;
  margin-right: 2px;
`
