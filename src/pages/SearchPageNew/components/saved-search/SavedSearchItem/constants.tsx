import { IconFolderOpen, IconSearch } from '@tabler/icons-react'

export const FOLDER_OPEN_ICON = <IconFolderOpen size={20} />

export const ITEM_ICON = <IconSearch size={20} />
