import { Stack, Text } from '@mantine/core'

import * as S from './styles'

const EmptySearchItems = () => {
  return (
    <Stack css={S.root} p="xs" my="xs" spacing="xs" align="center">
      <div>No searches available yet.</div>

      <Text size="sm" color="gray.6">
        Save your searches to this group to see them here.
      </Text>
    </Stack>
  )
}

export default EmptySearchItems
