import { css } from '@emotion/react'

export const root: ThemedCSS = theme => css`
  background-color: ${theme.colors.gray[1]};
  border-radius: ${theme.radius.sm};
  margin-left: 30px;
`
