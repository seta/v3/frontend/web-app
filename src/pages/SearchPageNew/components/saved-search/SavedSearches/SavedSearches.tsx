import { useState } from 'react'
import { Center, Loader, Stack } from '@mantine/core'

import SavedSearchItem from '~/pages/SearchPageNew/components/saved-search/SavedSearchItem/SavedSearchItem'
import { useFilters } from '~/pages/SearchPageNew/contexts/filters-context'
import useSavedSearchGroups from '~/pages/SearchPageNew/hooks/use-saved-search-groups'

import { useDeleteSearchMutation } from '~/api/search/saved-searches'
import type { SerializedSearch } from '~/types/saved-search/saved-search'
import { searchEvents } from '~/utils/events/search-events'

const PAGE_TOP_OFFSET = 190

const SavedSearches = () => {
  const { data, isLoading } = useSavedSearchGroups()

  const { mutate: deleteSearch } = useDeleteSearchMutation()

  const [itemLoading, setItemLoading] = useState<string>()

  const { dispatchAndApply } = useFilters()

  const handleDelete = (id: string) => {
    setItemLoading(id)

    deleteSearch(id, {
      onSettled: () => {
        setItemLoading(undefined)
      }
    })
  }

  const handleApply = (serializedSearch: SerializedSearch | undefined) => {
    if (!serializedSearch) {
      // If this is a folder for whatever reason, do nothing
      return
    }

    const { search, filters } = serializedSearch

    searchEvents.emit('setSearch', search)
    searchEvents.emit('setDocuments', search.embeddings ?? [])

    dispatchAndApply({ type: 'set_all', state: filters })

    window.scrollTo({ top: PAGE_TOP_OFFSET, behavior: 'smooth' })
  }

  if (isLoading || !data) {
    return (
      <Center py="md">
        <Loader size="sm" />
      </Center>
    )
  }

  return (
    <Stack spacing={0}>
      {data.groups.map(group => (
        <SavedSearchItem
          key={group.id}
          item={group}
          loadingId={itemLoading}
          onApply={handleApply}
          onDelete={handleDelete}
        />
      ))}
    </Stack>
  )
}

export default SavedSearches
