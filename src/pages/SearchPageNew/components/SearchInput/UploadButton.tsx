import { useEffect, useRef, useState } from 'react'
import type { DefaultMantineColor } from '@mantine/core'
import { Indicator, Loader, Tooltip } from '@mantine/core'
import { useScrollIntoView } from '@mantine/hooks'
import { IconPaperclip } from '@tabler/icons-react'

import ActionIconExtended from '~/components/ActionIconExtended'
import { useSearch } from '~/pages/SearchPageNew/contexts/search-context'
import { useUploadDocuments } from '~/pages/SearchPageNew/contexts/upload-documents-context'

import { EmbeddingType } from '~/types/embeddings'
import type { Variant } from '~/types/lib-props'

import * as S from './styles'

type Props = {
  active?: boolean
  inputFocused?: boolean
  onClick?: () => void
}

const SCROLL_OFFSET = 50
const INDICATOR_TIMEOUT = 1000

const UploadButton = ({ active, inputFocused, onClick }: Props) => {
  const [countChanged, setCountChanged] = useState(false)

  const { loading, documents } = useUploadDocuments()
  const { onSearch } = useSearch()

  const prevDocuments = useRef(documents)
  const changeTimeoutRef = useRef<number>()

  const { scrollIntoView, targetRef: scrollRef } = useScrollIntoView<HTMLButtonElement>({
    duration: 0,
    offset: SCROLL_OFFSET
  })

  const markCountChanged = () => {
    if (changeTimeoutRef.current) {
      clearTimeout(changeTimeoutRef.current)
    }

    setCountChanged(true)

    changeTimeoutRef.current = window.setTimeout(() => {
      setCountChanged(false)
    }, INDICATOR_TIMEOUT)
  }

  useEffect(() => {
    if (
      // If the documents have changed...
      prevDocuments.current !== documents &&
      // ...and there is only one document...
      documents.length === 1 &&
      // ...and it's a document from the search results...
      documents[0].type === EmbeddingType.Document
    ) {
      scrollIntoView()
      markCountChanged()

      // ...then automatically trigger the search
      onSearch(documents)

      prevDocuments.current = documents
    }
  }, [documents, onSearch, scrollIntoView])

  useEffect(() => {
    // Trigger the animation whenever the count changes
    markCountChanged()
  }, [documents.length])

  const count = documents.length

  const color: DefaultMantineColor = active ? 'blue' : 'gray.5'
  const variant: Variant = active ? 'light' : 'outline'

  return (
    <Indicator
      label={count}
      disabled={!count}
      color="teal"
      size={22}
      withBorder
      position="top-start"
      css={countChanged ? S.uploadIndicatorFlash : undefined}
      style={{ zIndex: '10' }}
    >
      <Tooltip label="Attach files or enter large amounts of text" disabled={!!count}>
        <ActionIconExtended
          ref={scrollRef}
          css={S.uploadButton}
          className="upload-button"
          color={color}
          variant={variant}
          hoverColor="blue"
          hoverVariant="light"
          size="xl"
          onClick={onClick}
          id="search-upload"
          data-input-focused={inputFocused}
        >
          {loading ? <Loader color="blue.4" size="sm" /> : <IconPaperclip />}
        </ActionIconExtended>
      </Tooltip>
    </Indicator>
  )
}

export default UploadButton
