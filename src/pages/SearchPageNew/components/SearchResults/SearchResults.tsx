import type { ReactNode } from 'react'
import { Box } from '@mantine/core'

import * as S from './styles'

import SearchResultsTabs from '../SearchResultsTabs'

type Props = {
  children: ReactNode
}

const SearchResults = ({ children }: Props) => {
  return (
    <Box css={S.root} mt="2rem">
      <SearchResultsTabs>{children}</SearchResultsTabs>
    </Box>
  )
}

export default SearchResults
