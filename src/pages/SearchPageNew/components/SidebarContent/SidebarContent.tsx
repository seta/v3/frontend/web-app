import { Box } from '@mantine/core'
import { IconListSearch, IconSearch, IconWallet, IconX } from '@tabler/icons-react'

import type { ToggleSectionProps } from '~/components/ToggleSection'
import ToggleSection from '~/components/ToggleSection'
import LibraryTree from '~/pages/SearchPageNew/components/documents/LibraryTree'
import SavedSearches from '~/pages/SearchPageNew/components/saved-search/SavedSearches'
import { useFilters } from '~/pages/SearchPageNew/contexts/filters-context'

import { useLibrary } from '~/api/search/library'
import { useSidebarState } from '~/contexts/sidebar-state-context'
import type { ClassNameProp } from '~/types/children-props'

import FiltersPanel from './components/FiltersPanel'

const SidebarContent = ({ className }: ClassNameProp) => {
  const { hasFilter, dispatchAndApply } = useFilters()

  const { data: libraryData, isLoading, error, refetch } = useLibrary()
  const { sidebarCollapsed } = useSidebarState()

  const filtersSectionAction: ToggleSectionProps['action'] = {
    label: 'Clear all',
    color: 'red',
    icon: <IconX size={20} />,
    hidden: !hasFilter,
    onClick: () => dispatchAndApply({ type: 'clear_all' })
  }

  return (
    <Box className={className} id="tab-filters">
      <ToggleSection
        icon={<IconListSearch size={20} />}
        color="teal"
        title="Filters"
        open={!sidebarCollapsed}
        action={filtersSectionAction}
        marker={hasFilter ? 'teal' : null}
      >
        <FiltersPanel />
      </ToggleSection>

      <ToggleSection icon={<IconSearch size={20} />} color="grape" title="My Searches">
        <SavedSearches />
      </ToggleSection>

      <ToggleSection icon={<IconWallet size={20} />} color="orange" title="My Documents">
        <LibraryTree
          data={libraryData?.items}
          isLoading={isLoading}
          error={error}
          onTryAgain={refetch}
        />
      </ToggleSection>
    </Box>
  )
}

export default SidebarContent
