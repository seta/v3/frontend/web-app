import { css } from '@emotion/react'

export const noWrap = css`
  white-space: nowrap;
  overflow: hidden;
`
