import { css } from '@emotion/react'

export const toggleContainer: ThemedCSS = theme => css`
  display: flex;
  flex-direction: column;
  gap: ${theme.spacing.sm};

  .seta-Switch-body {
    display: grid;
    grid-template-columns: 1fr auto;

    .seta-Switch-track,
    .seta-Switch-label {
      cursor: pointer;
    }
  }
`

export const dateFilter = css`
  border-top: 1px solid #e9ecef;
  padding-top: 1rem;
`

export const sourceChart: ThemedCSS = theme => css`
  display: flex;
  justify-content: center;
  padding: 0.5rem 0;
  border-top: 1px solid ${theme.colors.gray[3]};
`
