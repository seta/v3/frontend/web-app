import { useEffect, useMemo } from 'react'
import { Loader, ScrollArea } from '@mantine/core'

import LabelsGroup from '~/components/LabelsGroup'
import { SuggestionsError } from '~/pages/SearchPageNew/components/common'
import useGroupedLabels from '~/pages/SearchPageNew/hooks/use-grouped-labels'
import type { FilterPickerModalContentProps } from '~/pages/SearchPageNew/types/filters'

import { useAnnotations } from '~/api/catalogues/annotations'
import useScrolled from '~/hooks/use-scrolled'
import type { Label } from '~/types/search/annotations'
import { findLabels } from '~/utils/labels-utils'

import NoAnnotations from './components/NoAnnotations'
import * as S from './styles'

type Props = FilterPickerModalContentProps<Label>

const LabelsModalContent = ({
  modalOpen,
  selectedItems,
  searchValue,
  setSelectedItems,
  setSubmitDisabled
}: Props) => {
  const { data, isLoading, isFetching, error, refetch } = useAnnotations({ enabled: modalOpen })

  const foundLabels = useMemo(() => findLabels(data, searchValue), [data, searchValue])
  const { groupedLabels } = useGroupedLabels(foundLabels)

  const { scrolled, handleScrollChange } = useScrolled()

  useEffect(() => {
    setSubmitDisabled(!data?.length)
  }, [data, setSubmitDisabled])

  const handleLabelClick = (label: Label) => {
    // Toggle the label
    const newSelected = selectedItems.some(l => l.id === label.id)
      ? selectedItems.filter(l => l.id !== label.id)
      : [...selectedItems, label]

    setSelectedItems(newSelected)
  }

  const handleGroupClick = (category: string) => {
    const clickedLabels = groupedLabels[category]

    // If at least one label in the group is selected, deselect all labels in the group
    const newSelected = selectedItems.some(l => l.id === clickedLabels[0].id)
      ? selectedItems.filter(l => !clickedLabels.some(cl => cl.id === l.id))
      : [...selectedItems, ...clickedLabels]

    setSelectedItems(newSelected)
  }

  if (isLoading || isFetching || !data) {
    // Negative margin to compensate the grid gap in the parent component
    return <Loader mx="auto" mt="-1rem" />
  }

  if (error) {
    return (
      <SuggestionsError size="md" subject="annotations" withIcon mt="-1rem" onTryAgain={refetch} />
    )
  }

  if (!data.length) {
    return <NoAnnotations message="There are no annotations available in the system." />
  }

  if (!foundLabels?.length) {
    return <NoAnnotations message="No annotations found for this search." />
  }

  return (
    <div css={S.container} data-scrolled={scrolled}>
      <ScrollArea.Autosize mx="auto" h="100%" onScrollPositionChange={handleScrollChange}>
        {Object.entries(groupedLabels).map(([category, groupLabels]) => (
          <LabelsGroup
            key={category}
            css={S.group}
            category={category}
            labels={groupLabels}
            searchValue={searchValue}
            selected={selectedItems}
            onLabelClick={handleLabelClick}
            onGroupClick={() => handleGroupClick(category)}
          />
        ))}
      </ScrollArea.Autosize>
    </div>
  )
}

export default LabelsModalContent
