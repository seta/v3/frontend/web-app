import { css } from '@emotion/react'

export const root: ThemedCSS = theme => css`
  display: grid;
  grid-template-columns: minmax(250px, auto) 1fr;
  grid-template-rows: 1fr auto;
  align-self: stretch;
  overflow: hidden;
  background-color: ${theme.white};
  border-radius: ${theme.radius.md};
  border: 1px solid ${theme.colors.gray[4]};
`

export const selectedSources: ThemedCSS = theme => css`
  grid-column: 1 / -1;
  grid-row: 2;
  border-top: 1px solid ${theme.colors.gray[4]};
  padding: ${theme.spacing.xs};
`
