import { useRef } from 'react'
import { ScrollArea, Text } from '@mantine/core'
import { IconChevronLeft } from '@tabler/icons-react'

import ActionIconExtended from '~/components/ActionIconExtended'
import SelectableSourceItem from '~/pages/SearchPageNew/components/SidebarContent/components/SelectableSourceItem'
import type { CollectionInfo, ReferenceInfo } from '~/pages/SearchPageNew/types/filters'

import useScrolled from '~/hooks/use-scrolled'

import * as S from './styles'

type Props = {
  data: CollectionInfo[] | undefined
  currentCollection?: CollectionInfo
  selectedItems?: ReferenceInfo[]
  onSelectedChange?: (items: ReferenceInfo[]) => void
  onCollectionClick?: (collection: CollectionInfo) => void
  onGoBack?: () => void
}

const CollectionsResourcesPane = ({
  data,
  currentCollection,
  selectedItems,
  onSelectedChange,
  onCollectionClick,
  onGoBack
}: Props) => {
  const { scrolled, handleScrollChange } = useScrolled()

  const viewportRef = useRef<HTMLDivElement>(null)

  const scrollToTop = () => viewportRef.current?.scrollTo({ top: 0 })

  const items: CollectionInfo[] | ReferenceInfo[] | undefined = currentCollection
    ? currentCollection.references
    : data

  const handleCollectionClick = (item: CollectionInfo) => {
    onCollectionClick?.(item)
    scrollToTop()
  }

  const handleGoBack = () => {
    onGoBack?.()
    scrollToTop()
  }

  const handleSelectedChange = (item: ReferenceInfo, checked: boolean) => {
    const newSelectedItems = checked
      ? [...(selectedItems ?? []), item]
      : (selectedItems ?? []).filter(i => i.fullPath !== item.fullPath)

    onSelectedChange?.(newSelectedItems)
  }

  const isSelected = (item: ReferenceInfo) =>
    selectedItems?.some(i => i.fullPath === item.fullPath) ?? false

  const collectionHeader = currentCollection && (
    <div css={S.navHeader} data-scrolled={scrolled}>
      <ActionIconExtended hoverVariant="filled" hoverColor="gray.5" onClick={handleGoBack}>
        <IconChevronLeft />
      </ActionIconExtended>

      <Text size="md" weight={500}>
        {currentCollection.key}
      </Text>
    </div>
  )

  return (
    <div css={S.root} data-scrolled={!currentCollection && scrolled}>
      {collectionHeader}

      <ScrollArea.Autosize
        className="overflow-hidden"
        viewportRef={viewportRef}
        onScrollPositionChange={handleScrollChange}
      >
        <div css={[S.items, currentCollection && S.collectionItems]}>
          {items?.map((item: CollectionInfo | ReferenceInfo) => (
            <SelectableSourceItem
              key={item.fullPath}
              item={item}
              checked={isSelected(item)}
              onItemClick={() => handleCollectionClick(item)}
              onChange={checked => handleSelectedChange(item, checked)}
            />
          ))}
        </div>
      </ScrollArea.Autosize>
    </div>
  )
}

export default CollectionsResourcesPane
