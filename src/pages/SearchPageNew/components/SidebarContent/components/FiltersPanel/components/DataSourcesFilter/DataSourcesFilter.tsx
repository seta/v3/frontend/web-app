import { IconDatabase } from '@tabler/icons-react'

import type { ReferenceInfo } from '~/pages/SearchPageNew/types/filters'

import DataSourcesModalContent from './components/DataSourcesModalContent'
import SelectedDataSources from './components/SelectedDataSources'

import FilterModalPicker from '../FilterModalPicker'

type Props = {
  selectedItems: ReferenceInfo[]
  onSelectedChange: (items: ReferenceInfo[]) => void
}

const DataSourcesFilter = ({ selectedItems, onSelectedChange }: Props) => {
  const handleRemoveItem = (item: ReferenceInfo) => {
    const newItems = selectedItems.filter(i => i.fullPath !== item.fullPath)

    onSelectedChange(newItems)
  }

  return (
    <FilterModalPicker
      selectedItems={selectedItems}
      emptyActionLabel="Add data sources filter"
      modalTitle="Select data sources"
      modalIcon={<IconDatabase />}
      noSearch
      renderModalContent={DataSourcesModalContent}
      onApplySelection={onSelectedChange}
    >
      <SelectedDataSources items={selectedItems} onRemoveItem={handleRemoveItem} />
    </FilterModalPicker>
  )
}

export default DataSourcesFilter
