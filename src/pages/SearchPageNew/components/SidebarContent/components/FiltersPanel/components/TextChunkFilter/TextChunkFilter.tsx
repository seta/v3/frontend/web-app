import type { ChangeEvent } from 'react'
import { Switch } from '@mantine/core'

type Props = {
  multipleChunks: boolean
  disabled?: boolean
  onChange?(multipleChunks: boolean): void
}

const TextChunkFilter = ({ multipleChunks, disabled, onChange }: Props) => {
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const checked = event.currentTarget.checked

    onChange?.(checked)
  }

  return (
    <Switch
      checked={multipleChunks}
      disabled={disabled}
      onChange={handleChange}
      label="Show multiple chunks"
      labelPosition="left"
      onLabel="YES"
      offLabel="NO"
      size="md"
      color="teal"
    />
  )
}

export default TextChunkFilter
