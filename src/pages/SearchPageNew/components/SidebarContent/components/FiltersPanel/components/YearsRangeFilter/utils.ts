import type { YearsRangeBoundaries, YearsRangeValue } from '~/pages/SearchPageNew/types/filters'

const getRangeValue = (
  value: YearsRangeValue | undefined | null,
  rangeBoundaries: YearsRangeBoundaries | undefined
): YearsRangeValue | undefined => {
  if (value) {
    return value
  }

  if (rangeBoundaries) {
    return [rangeBoundaries.min ?? 0, rangeBoundaries.max ?? 0]
  }

  return undefined
}

export const getRangeProps = (
  value: YearsRangeValue | undefined | null,
  rangeBoundaries: YearsRangeBoundaries | undefined
) => {
  const { min, max } = rangeBoundaries ?? {}

  const rangeValue = getRangeValue(value, rangeBoundaries)

  const marks: { value: number; label?: string }[] | undefined = []

  if (min) {
    marks.push({ value: min, label: `${min}` })
  }

  if (max && min !== max) {
    marks.push({ value: max, label: `${max}` })
  }

  const step = min === max ? 0 : 1

  return { rangeValue, min, max, marks, step }
}
