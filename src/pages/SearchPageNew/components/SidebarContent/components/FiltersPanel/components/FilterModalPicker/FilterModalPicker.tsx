import type { ReactNode } from 'react'
import { useState } from 'react'
import { Button, ScrollArea, rem } from '@mantine/core'
import { IconX } from '@tabler/icons-react'

import type { FilterPickerModalContentProps } from '~/pages/SearchPageNew/types/filters'

import useScrolled from '~/hooks/use-scrolled'
import type { ChildrenProp } from '~/types/children-props'

import * as S from './styles'

import FilterModal from '../FilterModal'

type Props<T> = {
  selectedItems: T[]
  emptyActionLabel?: string
  editActionLabel?: string
  modalTitle?: string
  inputPlaceholder?: string
  modalIcon?: ReactNode
  noSearch?: boolean
  onApplySelection: (items: T[]) => void
  // Render prop for the content of the modal with state awareness
  renderModalContent: (props: FilterPickerModalContentProps<T>) => ReactNode
} & ChildrenProp

// Must extend `unknown` to avoid TypeScript getting confused with the `T` type
// eslint-disable-next-line @typescript-eslint/no-unnecessary-type-constraint
const FilterModalPicker = <T extends unknown>({
  modalTitle = 'Select items',
  emptyActionLabel = 'Add new filter',
  editActionLabel = 'Edit filter',
  selectedItems,
  modalIcon,
  children,
  renderModalContent,
  onApplySelection,
  ...modalProps
}: Props<T>) => {
  const [modalOpen, setModalOpen] = useState(false)

  const { scrolled, handleScrollChange: handleSourceScrollChange } = useScrolled()

  const hasSelectedItems = selectedItems.length > 0

  const handleAddItems = () => {
    setModalOpen(true)
  }

  const handleModalClose = () => {
    setModalOpen(false)
  }

  const handleClearAll = () => {
    onApplySelection([])
  }

  const content = hasSelectedItems ? (
    children
  ) : (
    <div className="flex items-center justify-center py-3">
      <Button variant="outline" onClick={handleAddItems}>
        {emptyActionLabel}
      </Button>
    </div>
  )

  const toolbar = hasSelectedItems && (
    <div css={S.actionsWrapper} data-scrolled={scrolled}>
      <Button variant="subtle" onClick={handleAddItems}>
        {editActionLabel}
      </Button>

      <Button variant="subtle" color="red" leftIcon={<IconX size={20} />} onClick={handleClearAll}>
        Clear all
      </Button>
    </div>
  )

  return (
    <div>
      {toolbar}

      <ScrollArea.Autosize
        mx="auto"
        mah={rem(250)}
        onScrollPositionChange={handleSourceScrollChange}
      >
        {content}
      </ScrollArea.Autosize>

      <FilterModal
        opened={modalOpen}
        selectedItems={selectedItems}
        title={modalTitle}
        icon={modalIcon}
        onApplySelection={onApplySelection}
        onClose={handleModalClose}
        // Pass the state to the render prop of the modal
        renderContent={({
          searchValue,
          modalSelectedItems,
          setModalSelectedItems,
          setSubmitDisabled
        }) =>
          // Pass the internal selected items and the setters to the modal content
          renderModalContent({
            modalOpen,
            searchValue,
            selectedItems: modalSelectedItems,
            setSelectedItems: setModalSelectedItems,
            setSubmitDisabled,
            onApplySelection
          })
        }
        {...modalProps}
      />
    </div>
  )
}

export default FilterModalPicker
