import { ScrollArea } from '@mantine/core'

import SelectableSourceItem from '~/pages/SearchPageNew/components/SidebarContent/components/SelectableSourceItem'
import type { SourceInfo } from '~/pages/SearchPageNew/types/filters'

import useScrolled from '~/hooks/use-scrolled'

import * as S from './styles'

type Props = {
  sources: SourceInfo[] | undefined
  currentSource?: SourceInfo
  selectedItems?: SourceInfo[]
  onSelectedChange?: (items: SourceInfo[]) => void
  onSourceClick?: (item: SourceInfo) => void
}

const SourcesPane = ({
  sources,
  currentSource,
  selectedItems,
  onSelectedChange,
  onSourceClick
}: Props) => {
  const { scrolled, handleScrollChange } = useScrolled()

  const isSelected = (item: SourceInfo) =>
    selectedItems?.some(i => i.fullPath === item.fullPath) ?? false

  const handleSelectedChange = (item: SourceInfo, checked: boolean) => {
    const newSelectedItems = checked
      ? [...(selectedItems ?? []), item]
      : (selectedItems ?? []).filter(i => i.fullPath !== item.fullPath)

    onSelectedChange?.(newSelectedItems)
  }

  return (
    <div css={S.root} data-scrolled={scrolled}>
      <ScrollArea.Autosize mx="auto" h="100%" onScrollPositionChange={handleScrollChange}>
        <div css={S.items}>
          {sources?.map(item => (
            <SelectableSourceItem
              key={item.fullPath}
              item={item}
              checked={isSelected(item)}
              selected={item.fullPath === currentSource?.fullPath}
              onItemClick={() => onSourceClick?.(item)}
              onChange={checked => handleSelectedChange(item, checked)}
            />
          ))}
        </div>
      </ScrollArea.Autosize>
    </div>
  )
}

export default SourcesPane
