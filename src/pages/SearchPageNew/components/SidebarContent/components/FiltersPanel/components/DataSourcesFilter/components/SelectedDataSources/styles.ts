import { css } from '@emotion/react'

export const root: ThemedCSS = theme => css`
  display: flex;
  flex-direction: column;
  gap: ${theme.spacing.xs};

  &[data-horizontal='true'] {
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
  }
`

export const item: ThemedCSS = theme => css`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: ${theme.spacing.xs};
  padding: ${theme.spacing.xs} ${theme.spacing.sm};
  border-radius: ${theme.radius.sm};
  background-color: ${theme.colors.gray[0]};

  &.thin {
    padding: 0.425rem 0.625rem;
    padding-right: 0.325rem;
  }
`

export const right = css`
  display: flex;
  align-items: center;
  gap: 4px;
`
