import { IconTag } from '@tabler/icons-react'

import type { Label } from '~/types/search/annotations'

import LabelsModalContent from './components/LabelsModalContent'
import SelectedLabels from './components/SelectedLabels'

import FilterModalPicker from '../FilterModalPicker'

type Props = {
  selectedLabels: Label[]
  onSelectedChange: (labels: Label[]) => void
}

const LabelsFilter = ({ selectedLabels, onSelectedChange }: Props) => {
  const handleRemoveLabels = (labels: Label[]) => {
    const newLabels = selectedLabels.filter(label => !labels.some(l => l.id === label.id))

    onSelectedChange(newLabels)
  }

  return (
    <FilterModalPicker
      selectedItems={selectedLabels}
      emptyActionLabel="Add annotations filter"
      modalTitle="Select annotations"
      inputPlaceholder="Type to find annotations"
      modalIcon={<IconTag />}
      renderModalContent={LabelsModalContent}
      onApplySelection={onSelectedChange}
    >
      <SelectedLabels labels={selectedLabels} onRemoveLabels={handleRemoveLabels} />
    </FilterModalPicker>
  )
}

export default LabelsFilter
