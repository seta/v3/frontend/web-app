import { useEffect, useState } from 'react'

import { useFilters } from '~/pages/SearchPageNew/contexts/filters-context'
import type {
  CollectionInfo,
  FilterPickerModalContentProps,
  ReferenceInfo,
  SourceInfo
} from '~/pages/SearchPageNew/types/filters'

import CollectionsResourcesPane from './components/CollectionsResourcesPane'
import NoDataSources from './components/NoDataSources'
import SourcesPane from './components/SourcesPane'
import * as S from './styles'

import SelectedDataSources from '../SelectedDataSources'

type Props = FilterPickerModalContentProps<ReferenceInfo>

const DataSourcesModalContent = ({
  modalOpen,
  selectedItems,
  setSelectedItems,
  setSubmitDisabled
}: Props) => {
  const {
    state: { dataSources = [] }
  } = useFilters()

  const [selectedSource, setSelectedSource] = useState<SourceInfo | undefined>(dataSources[0])

  const [selectedCollection, setSelectedCollection] = useState<CollectionInfo | undefined>(
    undefined
  )

  useEffect(() => {
    if (!modalOpen) {
      // Reset the selected items after the modal animation is finished
      setTimeout(() => {
        setSelectedCollection(undefined)
        setSelectedSource(dataSources[0])
      }, 200)
    }

    setSubmitDisabled(!dataSources.length)
  }, [modalOpen, dataSources, setSubmitDisabled])

  // Empty state
  if (!dataSources.length) {
    return <NoDataSources />
  }

  const collectionsResources = selectedCollection
    ? selectedCollection.references
    : selectedSource?.collections

  const handleSourceSelect = (source: SourceInfo) => {
    setSelectedSource(source)
    setSelectedCollection(undefined)
  }

  const handleCollectionClick = (collection: CollectionInfo) => {
    setSelectedCollection(collection)
  }

  const handleCollectionGoBack = () => {
    setSelectedCollection(undefined)
  }

  const handleRemoveItem = (item: ReferenceInfo) => {
    const newItems = selectedItems.filter(i => i.fullPath !== item.fullPath)

    setSelectedItems(newItems)
  }

  return (
    <div css={S.root}>
      <SourcesPane
        sources={dataSources}
        currentSource={selectedSource}
        selectedItems={selectedItems}
        onSelectedChange={setSelectedItems}
        onSourceClick={handleSourceSelect}
      />

      <CollectionsResourcesPane
        data={collectionsResources}
        currentCollection={selectedCollection}
        selectedItems={selectedItems}
        onSelectedChange={setSelectedItems}
        onCollectionClick={handleCollectionClick}
        onGoBack={handleCollectionGoBack}
      />

      {selectedItems.length > 0 && (
        <div css={S.selectedSources}>
          <SelectedDataSources
            items={selectedItems}
            layout="horizontal"
            onRemoveItem={handleRemoveItem}
          />
        </div>
      )}
    </div>
  )
}

export default DataSourcesModalContent
