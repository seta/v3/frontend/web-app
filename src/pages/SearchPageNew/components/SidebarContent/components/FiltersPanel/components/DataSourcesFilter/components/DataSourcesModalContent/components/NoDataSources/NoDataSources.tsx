import { Stack, Text } from '@mantine/core'

const NoDataSources = () => {
  return (
    <Stack align="center">
      <Text align="center" size="lg" color="gray.7">
        There are no data sources available.
      </Text>

      <Text align="center" size="md" color="gray.6">
        If you have a filter selected, please remove it and try again.
      </Text>
    </Stack>
  )
}

export default NoDataSources
