import { Text, Tooltip, clsx } from '@mantine/core'
import { IconAlertTriangle } from '@tabler/icons-react'

import ClearButton from '~/components/ClearButton'
import Color from '~/components/Color'
import type { ReferenceInfo } from '~/pages/SearchPageNew/types/filters'

import * as S from './styles'

type Props = {
  items: ReferenceInfo[]
  layout?: 'vertical' | 'horizontal'
  onRemoveItem?: (item: ReferenceInfo) => void
}

const SelectedDataSources = ({ items, layout = 'vertical', onRemoveItem }: Props) => {
  const notAvailableIcon = (
    <Tooltip label="Not available in the current results" withinPortal>
      <div className="cursor-help">
        <Color color="gray.5">
          <IconAlertTriangle size={18} />
        </Color>
      </div>
    </Tooltip>
  )

  return (
    <div css={S.root} data-horizontal={layout === 'horizontal' || undefined}>
      {items.map(item => (
        <div key={item.fullPath} css={S.item} className={clsx({ thin: layout === 'horizontal' })}>
          <Text size="sm" color={item.notAvailable ? 'dimmed' : 'dark'}>
            {item.fullPath}
          </Text>

          <div css={S.right}>
            {item.notAvailable && notAvailableIcon}

            <ClearButton
              hoverColor="red"
              hoverVariant="light"
              onClick={() => onRemoveItem?.(item)}
            />
          </div>
        </div>
      ))}
    </div>
  )
}

export default SelectedDataSources
