import { Box } from '@mantine/core'
import { IconTagOff } from '@tabler/icons-react'

import LabelsGroup from '~/components/LabelsGroup'
import NoItemsSelected from '~/pages/SearchPageNew/components/NoItemsSelected'
import useGroupedLabels from '~/pages/SearchPageNew/hooks/use-grouped-labels'

import type { Label } from '~/types/search/annotations'

type Props = {
  labels: Label[]
  onRemoveLabels?: (labels: Label[]) => void
}

const SelectedLabels = ({ labels, onRemoveLabels }: Props) => {
  const { groupedLabels } = useGroupedLabels(labels)

  const content = labels.length ? (
    Object.entries(groupedLabels).map(([category, groupLabels]) => (
      <LabelsGroup
        key={category}
        category={category}
        labels={groupLabels}
        allSelected
        onRemoveLabels={onRemoveLabels}
      />
    ))
  ) : (
    <NoItemsSelected name="annotations" icon={<IconTagOff />} />
  )

  return (
    <Box my="-0.25rem" px="0.25rem">
      {content}
    </Box>
  )
}

export default SelectedLabels
