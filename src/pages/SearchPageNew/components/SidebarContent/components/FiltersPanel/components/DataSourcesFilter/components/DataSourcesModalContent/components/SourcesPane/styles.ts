import { css } from '@emotion/react'

const SPACING = '8px'

export const root: ThemedCSS = theme => css`
  position: relative;
  border-right: 1px solid ${theme.colors.gray[4]};
  overflow: hidden;
  padding-left: ${theme.spacing.md};

  /* Simulate scroll shadow */
  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: 6px;
    background: linear-gradient(180deg, ${theme.colors.gray[4]} 0%, transparent 100%);
    opacity: 0;
    pointer-events: none;
    z-index: 1;
    transition: opacity 200ms ${theme.transitionTimingFunction};
  }

  &[data-scrolled='true']::before {
    opacity: 0.5;
  }
`

export const items: ThemedCSS = theme => css`
  display: flex;
  flex-direction: column;
  padding: ${SPACING} 0;
  margin: ${theme.spacing.sm} ${theme.spacing.md} ${theme.spacing.sm} 0;
`
