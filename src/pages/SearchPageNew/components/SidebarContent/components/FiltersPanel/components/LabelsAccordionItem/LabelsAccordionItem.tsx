import { IconTag } from '@tabler/icons-react'

import { FiltersAccordionItemName } from '~/pages/SearchPageNew/utils/filters-constants'

import type { Label } from '~/types/search/annotations'

import FilterAccordionItem from '../FilterAccordionItem'
import LabelsFilter from '../LabelsFilter'

type Props = {
  selectedLabels: Label[]
  onSelectedLabelsChange: (labels: Label[]) => void
}

const LabelsAccordionItem = ({ selectedLabels, onSelectedLabelsChange }: Props) => (
  <FilterAccordionItem
    value={FiltersAccordionItemName.LABELS}
    title="Annotations"
    icon={<IconTag />}
    color="pink"
    hasMarker={selectedLabels.length > 0}
  >
    <LabelsFilter selectedLabels={selectedLabels} onSelectedChange={onSelectedLabelsChange} />
  </FilterAccordionItem>
)

export default LabelsAccordionItem
