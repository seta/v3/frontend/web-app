import { css } from '@emotion/react'

export const actionsWrapper: ThemedCSS = theme => css`
  z-index: 1;
  position: relative;
  display: flex;
  justify-content: space-between;
  padding: 1px;
  border-bottom: 1px solid ${theme.colors.gray[4]};

  transition: box-shadow 200ms ${theme.transitionTimingFunction},
    border-color 200ms ${theme.transitionTimingFunction};

  &[data-scrolled='true'] {
    box-shadow: ${theme.shadows.sm};
    border-bottom-color: ${theme.colors.gray[3]};
  }

  & .seta-Button-root {
    border-radius: 0;

    &:disabled {
      background-color: transparent;
    }
  }
`
