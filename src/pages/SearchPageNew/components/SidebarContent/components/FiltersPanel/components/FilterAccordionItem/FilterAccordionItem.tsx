import type { ReactNode } from 'react'
import type { AccordionItemProps, DefaultMantineColor } from '@mantine/core'
import { Accordion, Text } from '@mantine/core'

import { AccordionItem, AccordionPanel } from '~/components/Accordion'

import * as S from './styles'

type Props = AccordionItemProps & {
  title: string
  icon: ReactNode
  color?: DefaultMantineColor
  hasMarker?: boolean
}

const FilterAccordionItem = ({ title, icon, color, hasMarker, children, ...props }: Props) => {
  const iconWrapper = (
    <Text span lh={1} color={color}>
      {icon}
    </Text>
  )

  return (
    <AccordionItem {...props}>
      <Accordion.Control icon={iconWrapper}>
        <Text span css={S.label} data-with-marker={hasMarker || undefined}>
          {title}
        </Text>
      </Accordion.Control>

      <AccordionPanel $withScrollArea css={S.container}>
        {children}
      </AccordionPanel>
    </AccordionItem>
  )
}

export default FilterAccordionItem
