import { css } from '@emotion/react'

export const container = css`
  display: flex;
  flex-direction: column;
`

export const label: ThemedCSS = theme => css`
  --marker-size: 8px;

  position: relative;

  &[data-with-marker]::after {
    content: '';
    position: absolute;
    top: calc(-1 * var(--marker-size) / 2);
    width: var(--marker-size);
    height: var(--marker-size);
    border-radius: 50%;
    background-color: ${theme.colors.teal[6]};
  }
`
