import type { ChangeEvent, ReactNode } from 'react'
import { useEffect, useRef, useState } from 'react'
import { Button, Group, TextInput } from '@mantine/core'
import { IconSearch } from '@tabler/icons-react'

import CancelButton from '~/components/CancelButton'
import ClearButton from '~/components/ClearButton'
import DefaultModal from '~/components/DefaultModal'

import type { ModalStateProps } from '~/types/lib-props'

import * as S from './styles'

type RenderProps<T> = {
  searchValue: string
  modalSelectedItems: T[]
  setModalSelectedItems: (items: T[]) => void
  // Allow disabling the submit button from the modal content
  setSubmitDisabled: (disabled: boolean) => void
}

export type FilterModalProps<T> = {
  selectedItems: T[]
  title?: ReactNode
  inputPlaceholder?: string
  icon?: ReactNode
  noSearch?: boolean
  onApplySelection: (items: T[]) => void
  // Render prop for the content of the modal
  renderContent: (props: RenderProps<T>) => ReactNode
} & ModalStateProps

// TS gets confused with the generic type here, so we need to add a constraint
// eslint-disable-next-line @typescript-eslint/no-unnecessary-type-constraint
const FilterModal = <T extends unknown>({
  selectedItems,
  inputPlaceholder = 'Type to find items',
  noSearch,
  onApplySelection,
  opened,
  onClose,
  renderContent,
  ...modalProps
}: FilterModalProps<T>) => {
  const [inputValue, setInputValue] = useState('')
  const [internalSelected, setInternalSelected] = useState<T[]>([])

  const [submitDisabled, setSubmitDisabled] = useState(false)

  const inputRef = useRef<HTMLInputElement>(null)

  useEffect(() => {
    if (opened) {
      // Sync the selected items
      setInternalSelected(selectedItems)
    }
  }, [opened, selectedItems])

  const handleInputChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
    setInputValue(target.value)
  }

  const handleClearInput = () => {
    setInputValue('')
    inputRef.current?.focus()
  }

  const handleApply = () => {
    onApplySelection(internalSelected)
    onClose()
  }

  const actions = (
    <Group spacing="sm">
      <CancelButton onClick={onClose} />

      <Button color="teal" disabled={submitDisabled} onClick={handleApply}>
        Apply Selection ({internalSelected.length})
      </Button>
    </Group>
  )

  const filterInput = noSearch ? null : (
    <TextInput
      ref={inputRef}
      value={inputValue}
      icon={<IconSearch size={20} />}
      iconWidth={32}
      placeholder={inputPlaceholder}
      aria-label={inputPlaceholder}
      rightSection={<ClearButton onClick={handleClearInput} />}
      data-autofocus
      onChange={handleInputChange}
    />
  )

  return (
    <DefaultModal
      opened={opened}
      actions={actions}
      css={S.root}
      transitionProps={{
        onExited: () => setInputValue('')
      }}
      onClose={onClose}
      {...modalProps}
    >
      <div css={noSearch ? S.contentNoSearch : S.content}>
        {filterInput}

        {/* Pass the local state down to the rendered content */}
        {renderContent({
          searchValue: inputValue,
          modalSelectedItems: internalSelected,
          setModalSelectedItems: setInternalSelected,
          // Allow the content to disable the submit button
          setSubmitDisabled
        })}
      </div>
    </DefaultModal>
  )
}

export default FilterModal
