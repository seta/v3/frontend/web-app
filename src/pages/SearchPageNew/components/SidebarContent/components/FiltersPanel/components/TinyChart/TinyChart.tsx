import { Paper, Text, useMantineTheme } from '@mantine/core'
import type { TooltipProps } from 'recharts'
import { BarChart, Bar, XAxis, Tooltip, Cell } from 'recharts'

import type { ValueWithCount } from '~/pages/SearchPageNew/types/filters'

type Props = {
  values?: ValueWithCount[]
  width?: number
  height?: number
  margin?: { top?: number; right?: number; bottom?: number; left?: number }
}

const ChartToolTip = ({ active, payload, label }: TooltipProps<string, string>) => {
  if (active && payload && payload.length) {
    return (
      <Paper shadow="xs" p="xs" withBorder>
        <Text fz="sm">{`${label} : ${payload[0].value}`}</Text>
      </Paper>
    )
  }

  return null
}

const TinyChart = ({ values, margin, height, width }: Props) => {
  const { colors } = useMantineTheme()

  if (!values?.length) {
    return null
  }

  return (
    <BarChart
      width={width ?? 320}
      height={height ?? 50}
      data={values}
      maxBarSize={10}
      margin={margin}
      style={{ zIndex: 5 }}
    >
      <XAxis dataKey="label" hide={true} />

      <Tooltip
        allowEscapeViewBox={{ y: true }}
        offset={5}
        content={ChartToolTip}
        cursor={{ fill: colors.gray[2] }}
      />

      <Bar dataKey="count">
        {values.map(entry => (
          <Cell fill={colors.gray[4]} key={`cell-${entry.key}`} />
        ))}
      </Bar>
    </BarChart>
  )
}

export default TinyChart
