import { useMemo } from 'react'
import type { DefaultMantineColor } from '@mantine/core'
import { Box, RangeSlider, Divider } from '@mantine/core'

import ClearButton from '~/components/ClearButton'
import type {
  ValueWithCount,
  YearsRangeValue,
  YearsRangeBoundaries
} from '~/pages/SearchPageNew/types/filters'

import * as S from './styles'
import { getRangeProps } from './utils'

import TinyChart from '../TinyChart'

type Props = {
  value?: YearsRangeValue | null
  rangeBoundaries?: YearsRangeBoundaries
  chartValues?: ValueWithCount[]
  selectedRangeBoundaries?: YearsRangeBoundaries
  onValueChange?(value: YearsRangeValue): void
  onValueChangeEnd?(value: YearsRangeValue): void
  onReset?: () => void
}

const CHART_WIDTH = 280
const CHART_MARGIN_BASE = 4

const YearsRangeFilter = ({
  value,
  rangeBoundaries,
  chartValues,
  selectedRangeBoundaries,
  onValueChange,
  onValueChangeEnd,
  onReset
}: Props) => {
  const { rangeValue, min, max, marks, step } = useMemo(
    () => getRangeProps(value, rangeBoundaries),
    [value, rangeBoundaries]
  )

  const color: DefaultMantineColor = selectedRangeBoundaries ? 'teal' : 'blue'

  const hasRange = rangeValue && !!min && !!max

  const slider = !hasRange ? (
    <Divider size="xl" color="gray.3" />
  ) : min === max ? (
    // One value -> show only one mark in the middle
    <RangeSlider
      color={color}
      step={0}
      value={rangeValue}
      min={min - 1}
      max={max + 1}
      minRange={0}
      marks={marks}
    />
  ) : (
    <RangeSlider
      color={color}
      step={step}
      value={rangeValue}
      min={min}
      max={max}
      minRange={step}
      marks={marks}
      label={null}
      onChange={onValueChange}
      onChangeEnd={onValueChangeEnd}
    />
  )

  const length = chartValues?.length ?? 0

  // Normalize the length to add some margin to the chart
  const chartMargin = useMemo(
    () =>
      Math.floor(
        (Math.max(0, Math.min(CHART_WIDTH, (length - 1) * 10)) / CHART_WIDTH) * CHART_MARGIN_BASE
      ),
    [length]
  )

  return (
    <Box css={S.layout} mt={!chartValues?.length ? 'xs' : undefined}>
      <div>
        <TinyChart
          width={CHART_WIDTH}
          margin={{ top: 0, left: chartMargin, right: chartMargin, bottom: 8 }}
          values={chartValues}
        />

        {slider}
      </div>

      <div css={S.actionsWrapper}>
        <ClearButton
          size="sm"
          // Keep it aligned with the slider
          mt={hasRange ? 32 : undefined}
          variant="light"
          color={selectedRangeBoundaries ? 'red' : 'gray.5'}
          hoverColor="red"
          hoverVariant="filled"
          onClick={onReset}
          disabled={!selectedRangeBoundaries}
        />
      </div>
    </Box>
  )
}

export default YearsRangeFilter
