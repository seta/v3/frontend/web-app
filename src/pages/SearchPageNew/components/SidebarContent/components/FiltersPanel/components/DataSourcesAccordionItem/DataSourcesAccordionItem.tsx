import { IconDatabase } from '@tabler/icons-react'

import type { ReferenceInfo } from '~/pages/SearchPageNew/types/filters'
import { FiltersAccordionItemName } from '~/pages/SearchPageNew/utils/filters-constants'

import DataSourcesFilter from '../DataSourcesFilter'
import FilterAccordionItem from '../FilterAccordionItem'

type Props = {
  selectedItems: ReferenceInfo[]
  onSelectedChange: (items: ReferenceInfo[]) => void
}

const DataSourcesAccordionItem = ({ selectedItems, onSelectedChange }: Props) => (
  <FilterAccordionItem
    value={FiltersAccordionItemName.DATA_SOURCE}
    title="Data Sources"
    icon={<IconDatabase />}
    color="teal"
    hasMarker={selectedItems.length > 0}
  >
    <DataSourcesFilter selectedItems={selectedItems} onSelectedChange={onSelectedChange} />
  </FilterAccordionItem>
)

export default DataSourcesAccordionItem
