import { css } from '@emotion/react'

export const root: ThemedCSS = theme => css`
  position: relative;
  overflow: hidden;

  display: grid;
  grid-template-rows: auto 1fr;

  /* TODO: Make this reusable 👇 */

  /* Simulate scroll shadow */
  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: 6px;
    background: linear-gradient(180deg, ${theme.colors.gray[4]} 0%, transparent 100%);
    opacity: 0;
    pointer-events: none;
    z-index: 1;
    transition: opacity 200ms ${theme.transitionTimingFunction};
  }

  &[data-scrolled='true']::before {
    opacity: 0.5;
  }
`

export const navHeader: ThemedCSS = theme => css`
  position: relative;
  display: flex;
  align-items: center;
  gap: 6px;
  padding: ${theme.spacing.xs} ${theme.spacing.md};
  background-color: ${theme.colors.gray[1]};
  border-bottom: 1px solid ${theme.colors.gray[4]};

  /* Simulate scroll shadow */
  &::after {
    content: '';
    position: absolute;
    bottom: -6px;
    left: 0;
    right: 0;
    height: 6px;
    background: linear-gradient(180deg, ${theme.colors.gray[4]} 0%, transparent 100%);
    opacity: 0;
    pointer-events: none;
    z-index: 1;
    transition: opacity 200ms ${theme.transitionTimingFunction};
  }

  &[data-scrolled='true']::after {
    opacity: 0.5;
  }
`

export const items: ThemedCSS = theme => css`
  display: flex;
  flex-direction: column;
  padding: 8px 0;
  margin: ${theme.spacing.sm} ${theme.spacing.md};
`

export const collectionItems: ThemedCSS = theme => css`
  margin: 0 ${theme.spacing.md};
`
