import { css } from '@emotion/react'

export const layout: ThemedCSS = theme => css`
  display: grid;
  grid-template-columns: 280px 1fr;
  align-items: center;
  gap: ${theme.spacing.xl};

  .seta-Slider-root {
    margin-bottom: 1rem;
  }
`

export const actionsWrapper: ThemedCSS = theme => css`
  display: flex;
  flex-direction: column;
  gap: ${theme.spacing.sm};
`
