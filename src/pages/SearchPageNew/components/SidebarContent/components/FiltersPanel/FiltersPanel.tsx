import { useState } from 'react'
import { Accordion, Container, Flex, LoadingOverlay, rem } from '@mantine/core'

import { useFilters } from '~/pages/SearchPageNew/contexts/filters-context'
import type { FiltersStateBase } from '~/pages/SearchPageNew/types/filters-reducer'
import { FiltersAccordionItemName } from '~/pages/SearchPageNew/utils/filters-constants'

import { STORAGE_KEY } from '~/utils/storage-keys'
import { storage } from '~/utils/storage-utils'

import DataSourcesAccordionItem from './components/DataSourcesAccordionItem'
import InForceFilter from './components/InForceFilter'
import LabelsAccordionItem from './components/LabelsAccordionItem'
import TextChunkFilter from './components/TextChunkFilter'
import YearsRangeFilter from './components/YearsRangeFilter'
import * as S from './styles'

const filtersStorage = storage<FiltersStateBase>(STORAGE_KEY.FILTERS)

const FilterPanel = () => {
  const {
    isLoading,
    state: {
      inForce,
      multipleChunks,
      yearsRange,
      yearsRangeBoundaries,
      selectedYearsBoundaries,
      values,
      selectedLabels = [],
      selectedSources = []
    },
    dispatch,
    dispatchAndApply
  } = useFilters()

  // Set the filters that should be open by default
  const [defaultFiltersOpen] = useState(() => {
    const savedFilters = filtersStorage.read()

    const filtersOpen = [FiltersAccordionItemName.DATA_SOURCE]

    // Open the annotations filter if there is a selection
    if (savedFilters?.selectedLabels?.length) {
      filtersOpen.push(FiltersAccordionItemName.LABELS)
    }

    return filtersOpen
  })

  return (
    <Flex direction="column" align="center" gap="xl" className="relative">
      <LoadingOverlay visible={isLoading} overlayBlur={5} mx={-10} my={-6} zIndex={10} />

      <Container w={350} css={S.toggleContainer}>
        <InForceFilter
          value={inForce}
          onChange={() => dispatchAndApply({ type: 'toggle_inForce' })}
        />

        <TextChunkFilter
          multipleChunks={multipleChunks}
          onChange={() => dispatchAndApply({ type: 'toggle_multipleChunks' })}
        />
      </Container>

      <Container w={350} css={S.dateFilter}>
        <YearsRangeFilter
          value={yearsRange}
          rangeBoundaries={yearsRangeBoundaries}
          chartValues={values.years}
          selectedRangeBoundaries={selectedYearsBoundaries}
          onValueChange={value => dispatch({ type: 'update_yearsRange', value })}
          onValueChangeEnd={value => dispatchAndApply({ type: 'update_yearsRange', value })}
          onReset={() => dispatchAndApply({ type: 'reset_yearsRange' })}
        />
      </Container>

      <Accordion
        w={rem(350)}
        mt="sm"
        multiple
        defaultValue={defaultFiltersOpen}
        variant="separated"
      >
        <DataSourcesAccordionItem
          selectedItems={selectedSources}
          onSelectedChange={items => dispatchAndApply({ type: 'update_sources', items })}
        />

        <LabelsAccordionItem
          selectedLabels={selectedLabels}
          onSelectedLabelsChange={labels => dispatchAndApply({ type: 'update_labels', labels })}
        />
      </Accordion>
    </Flex>
  )
}

export default FilterPanel
