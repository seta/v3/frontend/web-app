import type { MouseEvent } from 'react'
import { Checkbox } from '@mantine/core'
import { IconArrowRight } from '@tabler/icons-react'

import type { CollectionInfo, ReferenceInfo, SourceInfo } from '~/pages/SearchPageNew/types/filters'
import { isCollectionInfo, isSourceInfo } from '~/pages/SearchPageNew/utils/filters-utils'

import * as S from './styles'

type Props = {
  item: SourceInfo | CollectionInfo | ReferenceInfo
  checked?: boolean
  selected?: boolean
  onChange?: (checked: boolean) => void
  onItemClick?: () => void
}

const SelectableSourceItem = ({ item, checked, selected, onItemClick, onChange }: Props) => {
  const isCollection = isSourceInfo(item) || isCollectionInfo(item)

  const handleLabelClick = (e: MouseEvent<HTMLDivElement>) => {
    if (isCollection) {
      onItemClick?.()

      e.preventDefault()
      e.stopPropagation()
    }
  }

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onChange?.(event.target.checked)
  }

  const label = (
    <div
      css={S.item}
      className="seta-SelectableSourceItem-label"
      data-collection={isCollection || undefined}
      onClick={handleLabelClick}
    >
      <div>{item.key}</div>

      {isCollection && <IconArrowRight size={20} />}
    </div>
  )

  return (
    <Checkbox
      checked={checked}
      label={label}
      css={[S.root, selected && S.rootSelected]}
      data-selected={selected || undefined}
      onChange={handleChange}
    />
  )
}

export default SelectableSourceItem
