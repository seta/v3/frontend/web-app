import { css } from '@emotion/react'

const SPACING = '8px'

export const root = css`
  .seta-Checkbox-body {
    align-items: center;

    .seta-Checkbox-inner {
      cursor: pointer;

      .seta-Checkbox-input {
        cursor: pointer;
      }
    }

    .seta-Checkbox-labelWrapper {
      flex: 1;

      .seta-Checkbox-label {
        padding-left: 0.5rem;
      }
    }
  }
`

export const rootSelected: ThemedCSS = theme => css`
  &:not(:first-of-type) {
    margin-top: ${SPACING};
  }

  &:not(:last-of-type) {
    margin-bottom: ${SPACING};
  }

  .seta-SelectableSourceItem-label {
    background-color: ${theme.colors.gray[1]} !important;
    border-top-right-radius: ${theme.radius.xs};
    border-bottom-right-radius: ${theme.radius.xs};

    &::after {
      width: 5px !important;
    }

    & > svg {
      opacity: 0 !important;
      margin-right: -34px !important;
    }
  }
`

export const item: ThemedCSS = theme => css`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  gap: ${theme.spacing.xs};
  padding: 0.5rem ${theme.spacing.xs};
  border-radius: ${theme.radius.sm};
  overflow: hidden;
  cursor: default;

  &[data-collection='true'] {
    transition: all 0.2s ${theme.transitionTimingFunction};
    cursor: pointer;

    &::after {
      position: absolute;
      content: '';
      width: 0px;
      right: 0;
      top: 0;
      height: 100%;
      background-color: ${theme.colors.blue[5]};
      transition: width 0.3s ${theme.transitionTimingFunction};
    }

    & > svg {
      color: ${theme.colors.gray[4]};
      opacity: 0;
      margin-right: 6px;
      transition: all 0.3s ${theme.transitionTimingFunction};
    }

    &:hover {
      background-color: ${theme.colors.blue[0]};

      & > svg {
        color: ${theme.colors.gray[5]};
        opacity: 1;
        margin-right: 0;
      }
    }
  }
`
