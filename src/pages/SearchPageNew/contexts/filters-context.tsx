import type { Dispatch } from 'react'
import {
  useCallback,
  useRef,
  useMemo,
  useEffect,
  createContext,
  useContext,
  useReducer
} from 'react'

import type { YearsRangeValue } from '~/pages/SearchPageNew/types/filters'

import type { DocumentsOptions } from '~/api/search/documents'
import type { ChildrenProp } from '~/types/children-props'
import { STORAGE_KEY } from '~/utils/storage-keys'
import { storage } from '~/utils/storage-utils'

import useLoadAggregations from '../hooks/use-load-aggregations'
import filtersReducer from '../reducers/filters-reducer'
import type { FiltersAction, FiltersState, FiltersStateBase } from '../types/filters-reducer'
import { DEFAULT_FILTERS_STATE } from '../utils/filters-constants'
import {
  extractStateForStorage,
  filtersToDocumentOptions,
  hasFiltersChanges,
  hasSavedFilters
} from '../utils/filters-utils'

type FiltersProviderProps = {
  onApplyFilters: (options: DocumentsOptions) => void
}

type FiltersContextProps = {
  state: FiltersState
  hasFilter: boolean
  isLoading: boolean
  dispatch: Dispatch<FiltersAction>
  dispatchAndApply: Dispatch<FiltersAction>
  applyFilters: () => void
}

const FiltersContext = createContext<FiltersContextProps | undefined>(undefined)

const filtersStorage = storage<FiltersStateBase>(STORAGE_KEY.FILTERS)

export const FiltersProvider = ({
  onApplyFilters,
  children
}: FiltersProviderProps & ChildrenProp) => {
  const [state, dispatch] = useReducer(filtersReducer, undefined, () => {
    const savedFilters = filtersStorage.read()

    const yearsRange: YearsRangeValue = [
      savedFilters?.selectedYearsBoundaries?.min ?? 0,
      savedFilters?.selectedYearsBoundaries?.max ?? 0
    ]

    return savedFilters
      ? { ...DEFAULT_FILTERS_STATE, ...savedFilters, yearsRange }
      : DEFAULT_FILTERS_STATE
  })

  const { isLoading } = useLoadAggregations({ dispatch })

  const hasFilter = useMemo(() => hasFiltersChanges(state), [state])

  const willApplyFiltersRef = useRef(false)

  const applyFilters = useCallback(
    (skipSaveFilter = false) => {
      // Transform filters state to document query options
      const documentOptions = filtersToDocumentOptions(state)

      if (!skipSaveFilter) {
        filtersStorage.write(extractStateForStorage(state))
      }

      onApplyFilters(documentOptions)
    },
    [state, onApplyFilters]
  )

  // Update the filters state and apply them in one go
  const dispatchAndApply = (action: FiltersAction) => {
    dispatch(action)

    // Apply the filters during the next render, after the state has been updated
    willApplyFiltersRef.current = true
  }

  useEffect(() => {
    const savedFilters = filtersStorage.read()

    if (hasSavedFilters(savedFilters)) {
      applyFilters(true)
    }
    // We only want to run this effect once
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (willApplyFiltersRef.current) {
      applyFilters()

      willApplyFiltersRef.current = false
    }
  }, [state, applyFilters])

  const value: FiltersContextProps = {
    state,
    hasFilter,
    isLoading,
    dispatch,
    dispatchAndApply,
    applyFilters
  }

  return <FiltersContext.Provider value={value}>{children}</FiltersContext.Provider>
}

export const useFilters = () => {
  const context = useContext(FiltersContext)

  if (context === undefined) {
    throw new Error('useFilters must be used within a FiltersProvider')
  }

  return context
}
