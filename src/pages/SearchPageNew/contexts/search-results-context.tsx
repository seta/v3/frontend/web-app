import { createContext, useCallback, useContext, useState } from 'react'

import type { ChildrenProp } from '~/types/children-props'
import type { Document } from '~/types/search/documents'

type SearchResultsContextProps = {
  setResults: (results: Document[] | undefined) => void
  results: Document[] | undefined
  hasResults: boolean
  getIds: () => string[]
}

const SearchResultsContext = createContext<SearchResultsContextProps | undefined>(undefined)

export const SearchResultsProvider = ({ children }: ChildrenProp) => {
  const [results, setResults] = useState<Document[]>()

  const getIds = useCallback(() => results?.map(({ id }) => id) || [], [results])

  const hasResults = !!results?.length

  return (
    <SearchResultsContext.Provider value={{ setResults, results, hasResults, getIds }}>
      {children}
    </SearchResultsContext.Provider>
  )
}

export const useSearchResults = () => {
  const context = useContext(SearchResultsContext)

  if (!context) {
    throw new Error('useSearchResults must be used within a SearchResultsProvider')
  }

  return context
}
