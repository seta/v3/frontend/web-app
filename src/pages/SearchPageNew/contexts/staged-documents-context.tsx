import { createContext, useCallback, useContext, useEffect, useState } from 'react'

import type { ChildrenProp } from '~/types/children-props'
import type { Document } from '~/types/search/documents'
import type { StagedDocument } from '~/types/search/staged-documents'
import { toggleArrayValue } from '~/utils/array-utils'
import { toStagedDocument } from '~/utils/document-utils'
import { STORAGE_KEY } from '~/utils/storage-keys'
import { storage } from '~/utils/storage-utils'

type StagedDocumentsContextProps = {
  isStaged: (documentId: string) => boolean
  toggleStaged: (stagedDoc: StagedDocument, staged: boolean) => void
  removeStaged: (documentId: string | string[]) => void
  stageMany: (docs: Document[]) => void
  unstageMany: (docs: Document[]) => void
  clearStaged: () => void
  stagedDocuments: StagedDocument[]
}

const WRITE_STORAGE_DELAY = 50

const stagedDocsStorage = storage<StagedDocument[]>(STORAGE_KEY.STAGED_DOCS)

const StagedDocumentsContext = createContext<StagedDocumentsContextProps | undefined>(undefined)

/**
 * Keep track of staged documents in local storage.
 */
export const StagedDocumentsProvider = ({ children }: ChildrenProp) => {
  const [stagedDocuments, setStagedDocuments] = useState<StagedDocument[]>([])

  useEffect(() => {
    const stagedDocs = stagedDocsStorage.read()

    if (stagedDocs) {
      setStagedDocuments(stagedDocs)
    }
  }, [])

  const writeStagedDocs = (newStagedDocs: StagedDocument[]) => {
    setTimeout(() => {
      stagedDocsStorage.write(newStagedDocs)
    }, WRITE_STORAGE_DELAY)
  }

  const isStaged: StagedDocumentsContextProps['isStaged'] = useCallback(
    documentId => stagedDocuments.some(({ id }) => id === documentId),
    [stagedDocuments]
  )

  const toggleStaged: StagedDocumentsContextProps['toggleStaged'] = useCallback(
    (stagedDoc: StagedDocument, staged) => {
      const newStagedDocs = toggleArrayValue(stagedDocuments, stagedDoc, staged, 'id')

      setStagedDocuments(newStagedDocs)
      writeStagedDocs(newStagedDocs)
    },
    [stagedDocuments]
  )

  const stageMany = (docs: Document[]) => {
    const newStagedDocs = docs.map(doc => toStagedDocument(doc))

    const staged = [...stagedDocuments, ...newStagedDocs]

    setStagedDocuments(staged)
    writeStagedDocs(staged)
  }

  const unstageMany = (docs: Document[]) => {
    const ids = docs.map(({ _id }) => _id)
    const newStagedDocs = stagedDocuments.filter(({ id }) => !ids.includes(id))

    setStagedDocuments(newStagedDocs)
    writeStagedDocs(newStagedDocs)
  }

  const removeStaged: StagedDocumentsContextProps['removeStaged'] = useCallback(
    documentId => {
      const ids = Array.isArray(documentId) ? documentId : [documentId]
      const newStagedDocs = stagedDocuments.filter(({ id }) => !ids.includes(id))

      setStagedDocuments(newStagedDocs)
      writeStagedDocs(newStagedDocs)
    },
    [stagedDocuments]
  )

  const clearStaged: StagedDocumentsContextProps['clearStaged'] = () => {
    setStagedDocuments([])
    stagedDocsStorage.write([])
  }

  const value: StagedDocumentsContextProps = {
    isStaged,
    toggleStaged,
    removeStaged,
    stageMany,
    unstageMany,
    clearStaged,
    stagedDocuments
  }

  return <StagedDocumentsContext.Provider value={value}>{children}</StagedDocumentsContext.Provider>
}

/**
 * Hook to access the staged documents context
 */
export const useStagedDocuments = () => {
  const context = useContext(StagedDocumentsContext)

  if (!context) {
    throw new Error('useStagedDocuments must be used within a StagedDocumentsProvider')
  }

  return context
}
