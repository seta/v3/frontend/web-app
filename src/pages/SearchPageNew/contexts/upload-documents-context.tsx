import { createContext, useCallback, useContext, useEffect, useState } from 'react'
import { useLocalStorage } from '@mantine/hooks'

import {
  getFilesEmbeddingInfo,
  type FileEmbeddingsProgressInfo,
  getTextEmbeddingInfo,
  documentToEmbeddingInfo
} from '~/api/embeddings/embedding-info'
import type { ChildrenProp } from '~/types/children-props'
import type { EmbeddingInfo } from '~/types/embeddings'
import type { DocumentBrief } from '~/types/search/documents'
import { searchEvents } from '~/utils/events/search-events'
import { STORAGE_KEY } from '~/utils/storage-keys'

export enum LoadingType {
  DOCUMENTS = 'documents',
  TEXT = 'text'
}

type UploadDocumentsContextProps = {
  uploadFiles: (files: File[]) => Promise<void>
  text: string
  setText: (value: string) => void
  uploadText: () => Promise<EmbeddingInfo>
  progress: FileEmbeddingsProgressInfo | null
  loading: LoadingType | null
  documents: EmbeddingInfo[]
  removeChunk: (chunkId: string) => void
  removeDocument: (id: string) => void
  removeAll: () => void
  selectDocumentForSearchContext: (document: DocumentBrief) => void
}

const UploadDocumentsContext = createContext<UploadDocumentsContextProps | undefined>(undefined)

export const UploadDocumentsProvider = ({ children }: ChildrenProp) => {
  const [documents, setDocuments] = useLocalStorage<EmbeddingInfo[]>({
    key: STORAGE_KEY.UPLOADS,
    defaultValue: [],
    getInitialValueInEffect: true
  })

  const [text, setText] = useLocalStorage({
    key: STORAGE_KEY.TEXT_UPLOAD,
    defaultValue: ''
  })

  const [loading, setLoading] = useState<LoadingType | null>(null)
  const [progress, setProgress] = useState<FileEmbeddingsProgressInfo | null>(null)

  useEffect(() => {
    searchEvents.on('setDocuments', setDocuments)

    return () => {
      searchEvents.off('setDocuments', setDocuments)
    }
    // Must run only once
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const uploadFiles = async (files: File[]) => {
    setLoading(LoadingType.DOCUMENTS)

    const docs = await getFilesEmbeddingInfo(files, {
      onProgress: setProgress,
      existing: documents
    })

    // Allow the 100% step to be shown for a second
    setTimeout(() => {
      setProgress(null)
      setDocuments(prev => [...prev, ...docs])
      setLoading(null)
    }, 1000)
  }

  const uploadText = async () => {
    setLoading(LoadingType.TEXT)

    const doc = await getTextEmbeddingInfo(text, { existing: documents })

    setDocuments(prev => [...prev, doc])

    setText('')
    setLoading(null)

    return doc
  }

  const removeChunk = (chunkId: string) => {
    const docs = [...documents]

    const itemIndex = docs.findIndex(doc => doc.chunks?.some(chunk => chunk.id === chunkId))

    if (itemIndex === -1) {
      return
    }

    const item = docs[itemIndex]

    if (!item.chunks) {
      return
    }

    const newChunks = item.chunks.filter(c => c.id !== chunkId)

    if (newChunks.length === 0) {
      docs.splice(itemIndex, 1)
    } else {
      item.chunks = newChunks
    }

    setDocuments(docs)
  }

  const removeDocument = (id: string) => setDocuments(prev => prev.filter(doc => doc.id !== id))

  const removeAll = () => setDocuments([])

  const selectDocumentForSearchContext = useCallback(
    (document: DocumentBrief) => {
      const encoded = documentToEmbeddingInfo(document)

      setDocuments([encoded])
    },
    [setDocuments]
  )

  const value: UploadDocumentsContextProps = {
    documents,
    uploadFiles,
    text,
    setText,
    uploadText,
    progress,
    loading,
    removeChunk,
    removeDocument,
    removeAll,
    selectDocumentForSearchContext
  }

  return <UploadDocumentsContext.Provider value={value}>{children}</UploadDocumentsContext.Provider>
}

export const useUploadDocuments = () => {
  const context = useContext(UploadDocumentsContext)

  if (context === undefined) {
    throw new Error('useUploadedDocuments must be used within an UploadedDocumentsProvider')
  }

  return context
}
