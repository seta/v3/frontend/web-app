import { createContext, useContext } from 'react'

import type { DocumentsOptions } from '~/api/search/documents'
import type { ChildrenProp } from '~/types/children-props'

import type { SearchData, SearchState } from '../types/search'

type DocumentsQueryProviderProps = {
  query: SearchState | null
  searchOptions: DocumentsOptions | undefined
  setSearchData: (value: SearchData | null) => void
}

type DocumentsQueryContextProps = DocumentsQueryProviderProps

const DocumentsQueryContext = createContext<DocumentsQueryContextProps | undefined>(undefined)

export const DocumentsQueryProvider = ({
  children,
  ...props
}: DocumentsQueryProviderProps & ChildrenProp) => (
  <DocumentsQueryContext.Provider value={props}>{children}</DocumentsQueryContext.Provider>
)

export const useDocumentsQuery = () => {
  const context = useContext(DocumentsQueryContext)

  if (!context) {
    throw new Error('useDocumentsQuery must be used within a DocumentsQueryProvider')
  }

  return context
}
