import { createContext, useContext } from 'react'

import type { SearchValue } from '~/pages/SearchPageNew/types/search'

import type { ChildrenProp } from '~/types/children-props'

type SearchValueContextProps = {
  searchValue: SearchValue
}

const SearchValueContext = createContext<SearchValueContextProps | undefined>(undefined)

export const SearchValueProvider = ({
  searchValue,
  children
}: SearchValueContextProps & ChildrenProp) => {
  const value: SearchValueContextProps = {
    searchValue
  }

  return <SearchValueContext.Provider value={value}>{children}</SearchValueContext.Provider>
}

export const useSearchValue = () => {
  const context = useContext(SearchValueContext)

  if (!context) {
    throw new Error('useSearchValue must be used within an SearchValueProvider')
  }

  return context
}
