import type { GraphData } from 'react-force-graph-2d'

export const NodeType = {
  root: 'root',
  parent: 'parent',
  leaf: 'leaf'
} as const

// This is not a duplicate declaration
// eslint-disable-next-line @typescript-eslint/no-redeclare
export type NodeType = (typeof NodeType)[keyof typeof NodeType]

type NodeProps = {
  label: string
  type: NodeType
}

// Add custom properties to the graph nodes
export type CustomGraphData = GraphData<NodeProps>

export type GraphNode = CustomGraphData['nodes'][number]
export type GraphLink = CustomGraphData['links'][number]
