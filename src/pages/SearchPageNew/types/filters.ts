export type YearsRangeValue = [number, number]

export type YearsRangeBoundaries = {
  min?: number
  max?: number
}

export type YearWithCount = {
  year: number
  doc_count: number
}

export type ValueWithCount = {
  key: string | number
  label: string | number
  count?: number
  category?: string
}

export type ReferenceInfo = {
  key: string
  doc_count: number
  path?: string[]
  fullPath?: string
  // Controls the display of the "not available" icon
  notAvailable?: boolean
}

export type CollectionInfo = ReferenceInfo & {
  references?: ReferenceInfo[]
}

export type SourceInfo = ReferenceInfo & {
  collections?: CollectionInfo[]
}

export type Taxonomy = {
  doc_count?: number
  classifier?: string
  code?: string
  label?: string
  longLabel?: string
  name_in_path: string
  validated?: string
  version?: string
  subcategories?: Taxonomy[]
}

export type QueryAggregations = {
  search_type?: string
  date_year?: YearWithCount[]
  source_collection_reference?: { sources: SourceInfo[] }
  taxonomies?: Taxonomy[]
}

export type FilterPickerModalContentProps<T> = {
  modalOpen: boolean
  searchValue: string
  selectedItems: T[]
  setSelectedItems: (items: T[]) => void
  onApplySelection: (items: T[]) => void
  setSubmitDisabled: (disabled: boolean) => void
}
