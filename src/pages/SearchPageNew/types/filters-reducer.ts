import type {
  QueryAggregations,
  ReferenceInfo,
  SourceInfo,
  ValueWithCount,
  YearsRangeBoundaries,
  YearsRangeValue
} from '~/pages/SearchPageNew/types/filters'

import type { Label } from '~/types/search/annotations'

export type FiltersStateBase = {
  inForce: boolean
  multipleChunks: boolean
  selectedYearsBoundaries?: YearsRangeBoundaries
  selectedLabels?: Label[]
  selectedSources?: ReferenceInfo[]
}

export type FiltersState = FiltersStateBase & {
  yearsRange: YearsRangeValue | undefined
  yearsRangeBoundaries: { min: number | undefined; max: number | undefined }
  dataSources?: SourceInfo[]
  values: {
    years?: ValueWithCount[]
  }
}

export type FiltersAction =
  | {
      type: 'set_aggregations'
      aggregations: QueryAggregations
    }
  | {
      type: 'toggle_inForce'
    }
  | {
      type: 'toggle_multipleChunks'
    }
  | {
      type: 'update_yearsRange'
      value: YearsRangeValue
    }
  | {
      type: 'reset_yearsRange'
    }
  | {
      type: 'update_labels'
      labels: Label[]
    }
  | {
      type: 'update_sources'
      items: ReferenceInfo[]
    }
  | {
      type: 'set_all'
      state: FiltersStateBase
    }
  | {
      type: 'clear_all'
    }
