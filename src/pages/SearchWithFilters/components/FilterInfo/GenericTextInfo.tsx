import { Grid, Badge, Text } from '@mantine/core'

import { FilterStatusColors } from './utils'

type Props = {
  label: string
  value?: string
  modified?: boolean
}

const GenericTextInfo = ({ label, value, modified }: Props) => {
  if (!modified) {
    return null
  }

  const color = modified ? FilterStatusColors.MODIFIED : FilterStatusColors.APPLIED

  return (
    <Grid gutter="xs">
      <Grid.Col span={5}>
        <Text span>{label}:</Text>
      </Grid.Col>
      <Grid.Col span={7}>
        <Badge
          color={color}
          variant="outline"
          size="lg"
          styles={{ root: { textTransform: 'none' } }}
        >
          {value}
        </Badge>
      </Grid.Col>
    </Grid>
  )
}

export default GenericTextInfo
