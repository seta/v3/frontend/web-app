import GenericTextInfo from './GenericTextInfo'

import { TextChunkValues } from '../../types/filters'

type Props = {
  value?: string
  modified?: boolean
}

const TextChunkInfo = ({ value, modified }: Props) => {
  const chunkValue = TextChunkValues[value ?? 'CHUNK_SEARCH']

  if (!modified && chunkValue === TextChunkValues.CHUNK_SEARCH) {
    return null
  }

  const chunkLabel = chunkValue === TextChunkValues.CHUNK_SEARCH ? 'No' : 'Yes'

  return <GenericTextInfo label="Show multiple chunks" value={chunkLabel} modified={modified} />
}

export default TextChunkInfo
