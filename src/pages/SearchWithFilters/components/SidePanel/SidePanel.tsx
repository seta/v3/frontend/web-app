import { useState } from 'react'
import type { DefaultMantineColor } from '@mantine/core'
import { Box } from '@mantine/core'
import { IconListSearch, IconSearch, IconWallet } from '@tabler/icons-react'

import ToggleSection from '~/components/ToggleSection'
import UnderDevelopment from '~/components/UnderDevelopment/UnderDevelopment'
import LibraryTree from '~/pages/SearchPageNew/components/documents/LibraryTree'
import type { AdvancedFilterProps } from '~/pages/SearchWithFilters/types/contracts'
import { FilterStatus } from '~/pages/SearchWithFilters/types/filter-info'

import { useLibrary } from '~/api/search/library'
import { useSidebarState } from '~/contexts/sidebar-state-context'

import * as S from './styles'

import FiltersPanel from '../FiltersPanel'

const markerColor: Record<FilterStatus, DefaultMantineColor | null> = {
  [FilterStatus.UNKNOWN]: null,
  [FilterStatus.PROCESSING]: null,
  [FilterStatus.MODIFIED]: 'orange',
  [FilterStatus.APPLIED]: 'teal'
}

const SidePanel = ({
  className,
  queryContract,
  onApplyFilter,
  filtersDisabled
}: AdvancedFilterProps) => {
  const [filtersStatus, setFiltersStatus] = useState<FilterStatus | null>(null)

  const { data: libraryData, isLoading, error, refetch } = useLibrary()
  const { sidebarCollapsed } = useSidebarState()

  const marker = markerColor[filtersStatus ?? FilterStatus.UNKNOWN]

  return (
    <Box className={className} id="tab-filters">
      <ToggleSection
        icon={<IconListSearch size={20} />}
        color="teal"
        title="Filters"
        marker={marker}
        open={!filtersDisabled && !sidebarCollapsed}
        disabled={filtersDisabled}
      >
        <FiltersPanel
          queryContract={queryContract}
          onApplyFilter={onApplyFilter}
          onStatusChange={setFiltersStatus}
        />
      </ToggleSection>

      <ToggleSection icon={<IconSearch size={20} />} color="grape" title="My Searches">
        <UnderDevelopment variant="coming-soon" css={S.noWrap} />
      </ToggleSection>

      <ToggleSection icon={<IconWallet size={20} />} color="orange" title="My Documents">
        <LibraryTree
          data={libraryData?.items}
          isLoading={isLoading}
          error={error}
          onTryAgain={refetch}
        />
      </ToggleSection>
    </Box>
  )
}

export default SidePanel
