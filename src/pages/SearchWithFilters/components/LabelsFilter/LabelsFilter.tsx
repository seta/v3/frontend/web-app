import { useState } from 'react'
import { Button, ScrollArea, rem } from '@mantine/core'
import { IconLayoutList, IconX } from '@tabler/icons-react'

import useScrolled from '~/hooks/use-scrolled'
import type { Label } from '~/types/search/annotations'

import LabelsSearchModal from './components/LabelsSearchModal'
import SelectedLabels from './components/SelectedLabels'
import * as S from './styles'

type Props = {
  selectedLabels: Label[]
  onSelectedChange: (labels: Label[]) => void
}

const LabelsFilter = ({ selectedLabels, onSelectedChange }: Props) => {
  const [modalOpen, setModalOpen] = useState(false)

  const { scrolled, handleScrollChange: handleSourceScrollChange } = useScrolled()

  const handleAddLabels = () => {
    setModalOpen(true)
  }

  const handleModalClose = () => {
    setModalOpen(false)
  }

  const handleRemoveLabels = (labels: Label[]) => {
    const newLabels = selectedLabels.filter(label => !labels.some(l => l.id === label.id))

    onSelectedChange(newLabels)
  }

  const handleClearAll = () => {
    onSelectedChange([])
  }

  return (
    <div>
      <div css={S.actionsWrapper} data-scrolled={scrolled}>
        <Button variant="subtle" leftIcon={<IconLayoutList size={20} />} onClick={handleAddLabels}>
          Select annotations
        </Button>

        <Button
          variant="subtle"
          color="red"
          leftIcon={<IconX size={20} />}
          disabled={!selectedLabels.length}
          onClick={handleClearAll}
        >
          Clear all
        </Button>
      </div>

      <ScrollArea.Autosize
        mx="auto"
        mah={rem(250)}
        onScrollPositionChange={handleSourceScrollChange}
      >
        <SelectedLabels labels={selectedLabels} onRemoveLabels={handleRemoveLabels} />
      </ScrollArea.Autosize>

      <LabelsSearchModal
        opened={modalOpen}
        selectedLabels={selectedLabels}
        onClose={handleModalClose}
        onSelectedChange={onSelectedChange}
      />
    </div>
  )
}

export default LabelsFilter
