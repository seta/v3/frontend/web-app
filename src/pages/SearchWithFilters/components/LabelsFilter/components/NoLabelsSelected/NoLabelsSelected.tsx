import { Stack, Text } from '@mantine/core'
import { IconTagOff } from '@tabler/icons-react'

import useThemeColor from '~/hooks/use-theme-color'

const NoLabelsSelected = () => {
  const { getThemeColor } = useThemeColor()

  return (
    <Stack align="center" spacing="sm" p="md">
      <IconTagOff color={getThemeColor('gray.4')} />

      <Text size="sm" color="gray.5">
        No annotations selected
      </Text>
    </Stack>
  )
}

export default NoLabelsSelected
