import type { ChangeEvent } from 'react'
import { Switch } from '@mantine/core'

type Props = {
  value?: boolean
  disabled?: boolean
  onChange?(value: boolean): void
}

const InForceFilter = ({ value, disabled, onChange }: Props) => {
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const checked = event.currentTarget.checked

    onChange?.(checked)
  }

  return (
    <Switch
      checked={value ?? false}
      disabled={disabled}
      onChange={handleChange}
      label="In force"
      labelPosition="left"
      onLabel="YES"
      offLabel="NO"
      size="md"
    />
  )
}

export default InForceFilter
