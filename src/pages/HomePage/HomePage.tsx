import React, { useEffect, useState } from 'react'
import {
  createStyles,
  Container,
  Title,
  Text,
  rem,
  keyframes,
  useMantineTheme
} from '@mantine/core'
import TextTransition, { presets } from 'react-text-transition'

import image from '~/images/background.png'

const steamEffect = keyframes({
  '0%': { transform: 'scale(1.05, 1.2) translateY(5px)' },
  '100%': { transform: 'scale(0.9, 1) translateY(0px)' }
})

const useStyles = createStyles(theme => ({
  wrapper: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    minHeight: 800,
    backgroundImage: 'url(' + image + ')',
    backgroundSize: 'cover',
    backgroundPosition: 'center'
  },

  title: {
    fontWeight: 800,
    fontSize: rem(45),
    letterSpacing: rem(-1),
    paddingLeft: theme.spacing.md,
    paddingRight: theme.spacing.md,
    color: theme.white,
    textAlign: 'center',
    fontFamily: `Greycliff CF, ${theme.fontFamily}`
  },

  text: {
    position: 'relative',
    display: 'inline-block',
    textShadow: '0px 4px 4px rgba(0, 0, 0, 0.1)',

    '&:before': {
      content: 'attr(data-text)',
      textShadow: '0px 0px 12px white',
      position: 'absolute',
      zIndex: -1,
      left: 0,
      top: 0,
      color: 'transparent',
      animation: `${steamEffect} 10s ease-in`,
      transformOrigin: 'center'
    }
  },

  controls: {
    marginTop: 100,
    display: 'flex',
    justifyContent: 'center',
    paddingLeft: theme.spacing.md,
    paddingRight: theme.spacing.md,

    [theme.fn.smallerThan('xs')]: {
      flexDirection: 'column'
    }
  }
}))

const TEXTS = [
  "Connect, Share and Learn: Explore SeTA's Thriving Data Sources",
  'Revolutionize Document Analysis with SeTA: Visualize Concepts and Track Development Over Time',
  "Unlock the Power of Textual Data with SeTA's Advanced Text Analysis Techniques"
]

const HomePage = () => {
  const { classes } = useStyles()
  const [index, setIndex] = useState(0)

  const theme = useMantineTheme()

  useEffect(() => {
    const intervalId = setInterval(
      () => setIndex(value => value + 1),
      5000 // every 5 seconds
    )

    return () => clearTimeout(intervalId)
  }, [])

  const currentText = TEXTS[index % TEXTS.length]

  return (
    <>
      <div className={classes.wrapper}>
        <Container className="relative">
          <Title className={classes.title}>
            <TextTransition springConfig={presets.stiff}>
              <Text
                component="span"
                className={classes.text}
                inherit
                color={theme.other.jrcBlue}
                data-text={currentText}
              >
                {currentText}
              </Text>
            </TextTransition>
          </Title>
        </Container>
      </div>
    </>
  )
}

export default HomePage
