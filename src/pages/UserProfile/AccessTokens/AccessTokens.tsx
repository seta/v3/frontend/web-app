import { useState } from 'react'
import { Anchor, Text, Title } from '@mantine/core'

import logger from '~/utils/logger'

import CreateAccessToken from './components/Create/CreateAccessToken'
import ViewAccessToken from './components/View/ViewAccessToken'

const AccessTokens = () => {
  const [accessToken, setAccessToken] = useState('')

  const handleNewAccessToken = (token: string) => {
    setAccessToken(token)
    logger.log('New access token generated: ' + token)
  }

  return (
    <>
      <Title order={4}>Personal Access Token</Title>
      <Text fz="sm" color="gray.7" pt={10}>
        You can generate a personal access token for an application you use that needs access to{' '}
        <Anchor href="/seta-search/rag/v1/doc" target="_blank">
          SeTA RAG API
        </Anchor>
        . <br />
        No generated access tokens are stored on SeTA's side. Keep them secure.
      </Text>

      <CreateAccessToken onAccessTokenCreate={handleNewAccessToken} />

      {accessToken && <ViewAccessToken token={accessToken} />}
    </>
  )
}

export default AccessTokens
