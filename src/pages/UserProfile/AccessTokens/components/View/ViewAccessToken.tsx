import { CopyButton, Button, Code, Card, Textarea, Tooltip } from '@mantine/core'
import { IconCopy, IconCheck } from '@tabler/icons-react'

type Props = { token: string }

const ViewAccessToken = ({ token }: Props) => {
  const call = `
    POST /seta-search/rag/v1/query
    Accept: application/json
    Authorization: Bearer `

  return (
    <Card shadow="xs" padding="lg" radius="sm" withBorder w={1000} p="lg">
      <Code block mb={10}>
        {call}
        <Textarea value={token} variant="unstyled" autosize readOnly />
      </Code>

      <CopyButton value={token} timeout={2000}>
        {({ copied, copy }) => (
          <Tooltip label={copied ? 'Copied' : 'Copy token to clipboard'} withArrow position="right">
            <Button
              size="xs"
              variant="light"
              color={copied ? 'teal' : 'blue'}
              onClick={copy}
              leftIcon={copied ? <IconCheck size="1rem" /> : <IconCopy size="1rem" />}
            >
              {copied ? 'Copied' : 'Copy token'}
            </Button>
          </Tooltip>
        )}
      </CopyButton>
    </Card>
  )
}

export default ViewAccessToken
