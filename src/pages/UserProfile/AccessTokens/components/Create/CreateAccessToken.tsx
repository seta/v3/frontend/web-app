import { useState } from 'react'
import { Box, Button, Divider, Group, Input, Text, Title, Stack, Space, Paper } from '@mantine/core'
import { DatePicker } from '@mantine/dates'
import { IconKey } from '@tabler/icons-react'
import type { AxiosError } from 'axios'

import { useCreateAccessToken } from '~/api/user/rag-tokens'
import { notifications } from '~/utils/notifications'

type Props = {
  onAccessTokenCreate?(token: string): void
}

const CreateAccessToken = ({ onAccessTokenCreate }: Props) => {
  const minDate = new Date()

  minDate.setHours(0, 0, 0, 0)
  const maxDate = new Date(minDate.getTime() + 180 * 24 * 60 * 60 * 1000) // 180 days

  const [expiresDate, setExpiresDate] = useState<Date | null>(maxDate)
  const [loading, setLoading] = useState<boolean>(false)

  const createAccessTokenMutation = useCreateAccessToken()
  const handleCreateAccessToken = () => {
    if (!expiresDate) {
      notifications.showError('Expires date is required', { autoClose: true })

      return
    }

    setLoading(true)

    createAccessTokenMutation.mutate(
      { expires: expiresDate },
      {
        onSuccess: data => {
          notifications.showWarning(
            "No generated access tokens are stored on SeTA's side. Keep them secure.",
            { autoClose: false }
          )

          if (onAccessTokenCreate) {
            onAccessTokenCreate(data.accessToken)
          }
        },
        onError: (error: AxiosError | any) => {
          let message = 'Failed to create access token'

          if (error && error.response) {
            if (error.response.status === 401) {
              message = 'Please re-login to create access token'
            } else if (error.response.data && error.response.data['message']) {
              message = error.response.data['message']
            }
          }

          notifications.showError(message, { autoClose: false })
        },

        onSettled: () => {
          setLoading(false)
        }
      }
    )
  }

  return (
    <Box>
      <Group position="left" align="top" spacing="xl">
        <Input.Wrapper
          label="Expiration"
          description="The expiration date is limited to 180 days in the future."
          size="md"
          withAsterisk
          p="lg"
        >
          <DatePicker
            value={expiresDate}
            onChange={setExpiresDate}
            placeholder="Select expires date"
            minDate={minDate}
            maxDate={maxDate}
            defaultDate={maxDate}
            mt={5}
          />
        </Input.Wrapper>

        <Stack justify="space-between" p="lg">
          <Box>
            <Title order={5}>Scope</Title>
            <Divider my="xs" />
            <Space h="xs" />
            <Paper p="sm">
              <Title order={6}>query</Title>
              <Space h="xs" />
              <Text color="gray.6" size="sm">
                Access user query on SeTA search api.
              </Text>
            </Paper>
          </Box>
          <Box>
            <Button
              onClick={handleCreateAccessToken}
              loading={loading}
              leftIcon={<IconKey size="1rem" />}
            >
              Generate Token
            </Button>
          </Box>
        </Stack>
      </Group>
    </Box>
  )
}

export default CreateAccessToken
