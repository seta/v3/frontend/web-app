import { useState } from 'react'
import { Collapse, Tabs } from '@mantine/core'

import type { ClassNameProp } from '~/types/children-props'

import ApplicationAccessTokens from './components/ApplicationAccessTokens'
import ApplicationAuthKey from './components/ApplicationsAuthKey'

type Props = ClassNameProp & {
  open: boolean
  appName: string
}

const ApplicationDetails = ({ className, open, appName }: Props) => {
  const [activeTab, setActiveTab] = useState<string | null>('auth-key')

  return (
    <tr style={open ? { backgroundColor: 'white' } : { display: 'none' }}>
      <td colSpan={6}>
        <Collapse className={className} in={open}>
          <Tabs value={activeTab} onTabChange={setActiveTab} orientation="horizontal" p="1rem">
            <Tabs.List
              sx={theme => ({
                marginBottom: theme.spacing.xs
              })}
            >
              <Tabs.Tab value="auth-key">SSH Key</Tabs.Tab>
              <Tabs.Tab value="rag-tokens">Generate RAG Token</Tabs.Tab>
            </Tabs.List>

            <Tabs.Panel value="auth-key">
              {open ? <ApplicationAuthKey appName={appName} /> : null}
            </Tabs.Panel>

            <Tabs.Panel value="rag-tokens">
              {open ? <ApplicationAccessTokens appName={appName} /> : null}
            </Tabs.Panel>
          </Tabs>
        </Collapse>
      </td>
    </tr>
  )
}

export default ApplicationDetails
