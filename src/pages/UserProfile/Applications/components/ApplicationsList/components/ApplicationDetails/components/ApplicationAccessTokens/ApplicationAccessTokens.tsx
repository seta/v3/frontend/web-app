import { useState } from 'react'

import logger from '~/utils/logger'

import CreateAccessToken from './components/Create/CreateAccessToken'
import ViewAccessToken from './components/View/ViewAccessToken'

type Props = { appName: string }

const ApplicationAccessTokens = ({ appName }: Props) => {
  const [accessToken, setAccessToken] = useState('')

  const handleNewAccessToken = (token: string) => {
    setAccessToken(token)
    logger.log('New access token generated: ' + token)
  }

  return (
    <>
      <CreateAccessToken appName={appName} onAccessTokenCreate={handleNewAccessToken} />

      {accessToken && <ViewAccessToken token={accessToken} />}
    </>
  )
}

export default ApplicationAccessTokens
