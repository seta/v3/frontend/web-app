import { CopyButton, Button, Code, Card, Textarea, Tooltip } from '@mantine/core'
import { IconCopy, IconCheck } from '@tabler/icons-react'

type Props = { token: string }

const ViewAccessToken = ({ token }: Props) => {
  return (
    <Card shadow="xs" padding="lg" radius="sm" withBorder w={1000} p="lg" mt={10}>
      <Code block mb={10}>
        <Textarea value={token} variant="unstyled" autosize readOnly />
      </Code>

      <CopyButton value={token} timeout={2000}>
        {({ copied, copy }) => (
          <Tooltip label={copied ? 'Copied' : 'Copy token to clipboard'} withArrow position="right">
            <Button
              size="xs"
              variant="light"
              color={copied ? 'teal' : 'blue'}
              onClick={copy}
              leftIcon={copied ? <IconCheck size="1rem" /> : <IconCopy size="1rem" />}
            >
              {copied ? 'Copied' : 'Copy token'}
            </Button>
          </Tooltip>
        )}
      </CopyButton>
    </Card>
  )
}

export default ViewAccessToken
