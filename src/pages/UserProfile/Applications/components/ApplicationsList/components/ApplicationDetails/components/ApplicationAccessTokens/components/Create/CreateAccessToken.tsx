import { useState } from 'react'
import { Button, Group } from '@mantine/core'
import { DatePickerInput } from '@mantine/dates'
import { IconKey } from '@tabler/icons-react'
import type { AxiosError } from 'axios'

import { useCreateAccessToken } from '~/api/user/application-rag-tokens'
import { notifications } from '~/utils/notifications'

type Props = {
  appName: string
  onAccessTokenCreate?(token: string): void
}

const CreateAccessToken = ({ appName, onAccessTokenCreate }: Props) => {
  const minDate = new Date()

  minDate.setHours(0, 0, 0, 0)
  const maxDate = new Date(minDate.getTime() + 180 * 24 * 60 * 60 * 1000) // 180 days

  const [expiresDate, setExpiresDate] = useState<Date | null>(maxDate)
  const [loading, setLoading] = useState<boolean>(false)

  const createAccessTokenMutation = useCreateAccessToken()
  const handleCreateAccessToken = () => {
    if (!expiresDate) {
      notifications.showError('Expires date is required', { autoClose: true })

      return
    }

    setLoading(true)

    createAccessTokenMutation.mutate(
      { name: appName, expires: expiresDate },
      {
        onSuccess: data => {
          notifications.showWarning(
            "No generated access tokens are stored on SeTA's side. Keep them secure.",
            { autoClose: false }
          )

          if (onAccessTokenCreate) {
            onAccessTokenCreate(data.accessToken)
          }
        },
        onError: (error: AxiosError | any) => {
          let message = 'Failed to create access token'

          if (error && error.response) {
            if (error.response.status === 401) {
              message = 'Please re-login to create access token'
            } else if (error.response.data && error.response.data['message']) {
              message = error.response.data['message']
            }
          }

          notifications.showError(message, { autoClose: false })
        },

        onSettled: () => {
          setLoading(false)
        }
      }
    )
  }

  return (
    <Group position="left" align="end" spacing="xl">
      <DatePickerInput
        label="Expiration"
        description="The expiration date is limited to 180 days in the future."
        placeholder="Pick expiration date"
        value={expiresDate}
        minDate={minDate}
        maxDate={maxDate}
        onChange={setExpiresDate}
        withAsterisk
      />
      <Button
        onClick={handleCreateAccessToken}
        loading={loading}
        leftIcon={<IconKey size="1rem" />}
      >
        Generate Token
      </Button>
    </Group>
  )
}

export default CreateAccessToken
