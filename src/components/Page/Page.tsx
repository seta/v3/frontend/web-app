import type { ReactElement } from 'react'
import { Flex, clsx } from '@mantine/core'

import Breadcrumbs from '~/components/Breadcrumbs'
import Sidebar from '~/components/Sidebar'

import type { Crumb } from '~/types/breadcrumbs'
import type { ChildrenProp, ClassNameProp } from '~/types/children-props'

import * as S from './styles'

type Props = {
  sidebarContent?: ReactElement
  collapsibleSidebar?: boolean
  breadcrumbs?: Crumb[]
} & ChildrenProp &
  ClassNameProp

const Page = ({ className, sidebarContent, collapsibleSidebar, breadcrumbs, children }: Props) => {
  const rootCls = clsx({ 'with-breadcrumbs': breadcrumbs }, className)

  const sidebar = sidebarContent && (
    <Sidebar withBreadcrumbs={!!breadcrumbs} collapsible={collapsibleSidebar}>
      {sidebarContent}
    </Sidebar>
  )

  return (
    <Flex direction="column" css={S.root} className={rootCls}>
      {breadcrumbs && <Breadcrumbs crumbs={breadcrumbs} />}

      <Flex css={S.pageWrapper}>
        {sidebar}

        <Flex direction="column" align="center" css={S.contentWrapper}>
          {children}
        </Flex>
      </Flex>
    </Flex>
  )
}

export default Page
