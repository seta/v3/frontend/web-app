import { useEffect, useState } from 'react'
import type { LoaderProps, MenuProps } from '@mantine/core'
import { Center, Loader, Menu } from '@mantine/core'

import type { ActionWithPopover } from '~/components/ActionIconPopover'
import ColoredActionIcon from '~/components/ColoredActionIcon'

import type { ClassNameProp } from '~/types/children-props'

import * as S from './styles'

type Props = {
  action: ActionWithPopover
  isLoading?: boolean
  loaderProps?: LoaderProps
} & MenuProps &
  ClassNameProp

const ActionIconMenu = ({
  action,
  isLoading,
  loaderProps,
  opened,
  children,
  className,
  onChange,
  ...menuProps
}: Props) => {
  const [isOpen, setIsOpen] = useState(opened)

  const { icon, active, onClick, color, ...actionProps } = action

  const handleIconClick = () => {
    setIsOpen(prev => !prev)
    onClick?.()
  }

  const handleChange = (open: boolean) => {
    setIsOpen(open)
    onChange?.(open)
  }

  useEffect(() => {
    setIsOpen(opened)
  }, [opened])

  // Create a wrapper to prevent the click event from bubbling up to the parent
  const inner = <div onClick={e => e.stopPropagation()}>{children}</div>

  const target = isLoading ? (
    // Must be wrapped because the Loader component isn't forwarding the ref
    <Center>
      <Loader size="sm" {...loaderProps} />
    </Center>
  ) : (
    <ColoredActionIcon
      color={color}
      activeColor={color}
      activeVariant="filled"
      isActive={isOpen || active}
      onClick={handleIconClick}
      {...actionProps}
    >
      {icon}
    </ColoredActionIcon>
  )

  return (
    <Menu
      shadow="md"
      position="bottom-end"
      width={240}
      opened={isOpen}
      onChange={handleChange}
      {...menuProps}
      disabled={isLoading}
    >
      <Menu.Target>{target}</Menu.Target>

      <Menu.Dropdown className={className} css={S.menu}>
        {inner}
      </Menu.Dropdown>
    </Menu>
  )
}

export default ActionIconMenu
