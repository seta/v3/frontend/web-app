import type { ForwardedRef, ReactNode } from 'react'
import { useEffect, useRef, useState } from 'react'
import type { TabsListProps } from '@mantine/core'
import { Tabs as MantineTabs } from '@mantine/core'
import { useScrollIntoView } from '@mantine/hooks'

import type { ClassAndChildrenProps } from '~/types/children-props'
import type { StorageKey } from '~/utils/storage-keys'

import { CurrentTabProvider } from './contexts/tabs-context'
import * as S from './styles'

const SCROLL_DELAY = 0

export type TabsProps = {
  panels: ReactNode
  defaultValue?: string
  position?: TabsListProps['position']
  tabsRef?: ForwardedRef<HTMLDivElement>
  noScroll?: boolean
  storageKey?: StorageKey
  onTabChange?: (value: string) => void
} & ClassAndChildrenProps

const Tabs = ({
  panels,
  defaultValue,
  className,
  tabsRef,
  noScroll,
  storageKey,
  onTabChange,
  children,
  ...rest
}: TabsProps) => {
  const [activeTab, setActiveTab] = useState(() => {
    // Check if there is a stored tab in localStorage
    if (storageKey) {
      const storedTab = localStorage.getItem(storageKey)

      if (storedTab) {
        return storedTab
      }
    }

    // Otherwise, use the default value
    if (defaultValue) {
      return defaultValue
    }

    return null
  })

  const prevTabRef = useRef<string | undefined>(undefined)
  const timeoutRef = useRef<number | undefined>(undefined)

  const { scrollIntoView, targetRef } = useScrollIntoView<HTMLDivElement>({
    duration: 100,
    offset: 16
  })

  // Cleanup timeout on unmount
  useEffect(() => {
    // Store the initial tab in the ref after the first render to make sure the state is updated
    prevTabRef.current = activeTab ?? undefined

    return () => {
      window.clearTimeout(timeoutRef.current)
    }
    // We only want to run this effect once
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  // Store the active tab in localStorage
  useEffect(() => {
    if (storageKey && activeTab) {
      localStorage.setItem(storageKey, activeTab)
    }
  }, [storageKey, activeTab])

  const handleTabChange = (value: string) => {
    if (!noScroll) {
      timeoutRef.current = window.setTimeout(() => {
        scrollIntoView()
      }, SCROLL_DELAY)
    }

    if (prevTabRef.current === value) {
      return
    }

    setActiveTab(value)

    prevTabRef.current = value

    onTabChange?.(value)
  }

  return (
    <MantineTabs
      css={S.root}
      ref={targetRef}
      className={className}
      variant="outline"
      value={activeTab}
      onTabChange={handleTabChange}
      {...rest}
    >
      <CurrentTabProvider value={activeTab}>
        <MantineTabs.List ref={tabsRef} position="center">
          {children}
        </MantineTabs.List>

        {panels}
      </CurrentTabProvider>
    </MantineTabs>
  )
}

export default Tabs
