import { css } from '@emotion/react'

export const iconWrapper: ThemedCSS = theme => css`
  font-size: ${theme.fontSizes.xl};
`

export const rightContainer = css`
  position: absolute;
  z-index: 1;
  top: 0;
  right: 0;
  margin-top: -4px;
  display: flex;
  align-items: center;
  justify-content: center;
`
