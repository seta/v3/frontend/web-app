import { css } from '@emotion/react'

import { CONTENT_MAX_WIDTH } from '~/styles'

export const root: ThemedCSS = theme => css`
  max-width: ${CONTENT_MAX_WIDTH};
  position: relative;
  padding: ${theme.spacing.lg} 0;
  margin-left: auto;
  margin-right: auto;

  &[data-full='true'] {
    max-width: 100%;
    padding: 0;
  }
`

export const content = css`
  margin-left: auto;
  margin-right: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
`
