import { Tabs } from '@mantine/core'

import type { ClassAndChildrenProps } from '~/types/children-props'

import * as S from './styles'

type Props = {
  value: string
  fullSize?: boolean
} & ClassAndChildrenProps

const TabPanel = ({ value, fullSize, className, children }: Props) => {
  return (
    <Tabs.Panel css={S.root} value={value} className={className} data-full={fullSize || undefined}>
      <div css={S.content}>{children}</div>
    </Tabs.Panel>
  )
}

export default TabPanel
