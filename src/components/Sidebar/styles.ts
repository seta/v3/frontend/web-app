import { css } from '@emotion/react'

export const wrapper = css`
  position: sticky;
  z-index: 11;
  top: 0;
  align-self: stretch;
`

export const sidebar: ThemedCSS = theme => css`
  position: sticky;
  top: 0;
  margin: -3rem 0;
  min-width: 24rem;
  max-width: 24rem;
  max-height: 100vh;
  height: auto;
  flex: 1;
  border-color: ${theme.colors.gray[3]};
  background-color: ${theme.fn.rgba(theme.colors.gray[0], 1)};
  box-shadow: ${theme.shadows.lg};

  transition: min-width 200ms ${theme.transitionTimingFunction},
    max-width 200ms ${theme.transitionTimingFunction},
    border 200ms ${theme.transitionTimingFunction},
    box-shadow 200ms ${theme.transitionTimingFunction};

  .seta-ScrollArea-viewport > div {
    display: block !important;
  }

  & > div {
    transition: opacity 200ms ${theme.transitionTimingFunction};
    will-change: opacity;
  }

  .toggle-button {
    position: absolute;
    top: 17px;
    left: calc(100% + 1px);
    z-index: 20;
    border-top-right-radius: ${theme.radius.sm};
    border-bottom-right-radius: ${theme.radius.sm};
    transition: left 200ms ${theme.transitionTimingFunction},
      top 200ms ${theme.transitionTimingFunction}, width 200ms ${theme.transitionTimingFunction};

    svg {
      transition: transform 500ms ${theme.transitionTimingFunction};
    }
  }
`

export const sidebarCollapsed: ThemedCSS = theme => css`
  min-width: 58px;
  max-width: 58px;
  box-shadow: none;

  & > div {
    overflow: hidden;
    white-space: nowrap;
  }

  .toggle-button {
    left: 1px;
    width: calc(100% - 1px);
    top: 0;

    svg {
      transform: rotate(180deg);
    }
  }

  /* Update the indicator to better align with the icon when the sidebar is collapsed */
  && .seta-Indicator-indicator {
    width: calc(0.5rem + 1px);
    height: calc(0.5rem + 1px);
    margin-right: 0;
    margin-top: 0;
    border: solid 1px ${theme.white};
  }
`
