import { useEffect, useState } from 'react'
import { Flex, Navbar, ScrollArea, Box, clsx, ActionIcon } from '@mantine/core'
import { IconChevronsLeft } from '@tabler/icons-react'

import { SidebarStateProvider } from '~/contexts/sidebar-state-context'
import type { ChildrenProp, ClassNameProp } from '~/types/children-props'
import { SidebarState } from '~/types/sidebar'
import { STORAGE_KEY } from '~/utils/storage-keys'
import { storage } from '~/utils/storage-utils'

import * as S from './styles'

type Props = {
  withBreadcrumbs?: boolean
  collapsible?: boolean
} & ChildrenProp &
  ClassNameProp

const sidebarStorage = storage<SidebarState>(STORAGE_KEY.SIDEBAR)

const Sidebar = ({ className, withBreadcrumbs, collapsible, children }: Props) => {
  const [sidebarState, setSidebarState] = useState(() => {
    if (collapsible) {
      return sidebarStorage.read() ?? SidebarState.COLLAPSED
    }

    return SidebarState.EXPANDED
  })

  useEffect(() => {
    if (collapsible) {
      sidebarStorage.write(sidebarState)
    }
  }, [collapsible, sidebarState])

  const toggleCollapsed = () => {
    setSidebarState(current => {
      // Expand it if it's collapsed
      if (current === SidebarState.COLLAPSED) {
        return SidebarState.EXPANDED
      }

      // Collapse it otherwise
      return SidebarState.COLLAPSED
    })
  }

  const navbarCls = clsx({ 'with-breadcrumbs': withBreadcrumbs })

  const sidebarStyle = [S.sidebar, sidebarState === SidebarState.COLLAPSED && S.sidebarCollapsed]

  const toggleButton = collapsible && (
    <ActionIcon
      color="gray"
      variant="light"
      bg="gray.1"
      size="md"
      radius={0}
      className="toggle-button"
      onClick={toggleCollapsed}
    >
      <IconChevronsLeft />
    </ActionIcon>
  )

  return (
    <Flex className={className} css={S.wrapper}>
      <Navbar css={sidebarStyle} className={navbarCls}>
        {toggleButton}

        <SidebarStateProvider
          sidebarState={sidebarState}
          setSidebarState={setSidebarState}
          toggleSidebarCollapsed={toggleCollapsed}
        >
          <Navbar.Section className="section" grow component={ScrollArea}>
            <Box py="2rem">{children}</Box>
          </Navbar.Section>
        </SidebarStateProvider>
      </Navbar>
    </Flex>
  )
}

export default Sidebar
