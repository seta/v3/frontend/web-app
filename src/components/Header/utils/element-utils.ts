/**
 * Loads a web tool by dynamically creating and appending a script element to the document head.
 * @param src - The source URL of the script to be loaded.
 */
export const loadWebTool = function (src: string) {
  const script = document.createElement('script')

  script.defer = true
  script.type = 'text/javascript'
  script.src = src

  document.head.appendChild(script)
}

/**
 * Creates a script element with the specified id and innerHTML, and appends it to the document head.
 * @param id - The id attribute for the script element.
 * @param innerHTML - The innerHTML content for the script element.
 */
export const createScript = function (id: string, innerHTML: string) {
  const script = document.createElement('script')

  script.id = id
  script.type = 'application/json'
  script.innerHTML = innerHTML

  document.head.appendChild(script)
}
