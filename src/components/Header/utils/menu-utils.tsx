import { Menu } from '@mantine/core'
import { FaUser, FaWrench } from 'react-icons/fa'
import { FiLogOut } from 'react-icons/fi'
import { NavLink } from 'react-router-dom'

import * as S from '../styles'
import type { MenuItem, DropdownItem, DropdownProps, GetAboutDropdownArgs } from '../types'

/**
 * Returns an array of menu items based on the authentication status.
 *
 * @param authenticated - A boolean indicating whether the user is authenticated.
 * @returns An array of `MenuItem` objects.
 */
export const getMenuItems = (authenticated: boolean): MenuItem[] => [
  {
    to: '/search',
    label: 'Search',
    hidden: !authenticated,
    tabId: 'search-tab'
  },
  {
    to: '/data-sources',
    label: 'Data Sources',
    hidden: !authenticated,
    tabId: 'datasource-tab'
  }
]

/**
 * Retrieves the dropdown items for the "About" menu.
 *
 * @param options - The options for generating the dropdown items.
 * @param options.onTakeTour - The callback function for taking a tour.
 * @returns An array of dropdown items.
 */
export const getDropdownAbout = ({ onTakeTour }: GetAboutDropdownArgs): DropdownItem[] => [
  {
    label: 'Docs',
    url: '/docs',
    external: true
  },
  {
    label: 'Contact Us',
    url: '/contact'
  },
  {
    divider: true
  },
  {
    label: 'Take a Tour',
    onClick: onTakeTour
  }
]

/**
 * Retrieves the dropdown items for the user menu.
 *
 * @param options - The options for generating the dropdown items.
 * @param options.isAdmin - Indicates whether the user is an admin.
 * @param options.onLogout - The callback function for logging out.
 * @returns The array of dropdown items.
 */
export const getDropdownItems = ({ isAdmin, onLogout }: DropdownProps): DropdownItem[] => {
  const items: DropdownItem[] = [
    {
      label: 'Profile',
      icon: <FaUser size="1.1rem" />,
      url: '/profile'
    }
  ]

  if (isAdmin) {
    items.push({
      label: 'Administration',
      icon: <FaWrench size="1.1rem" />,
      url: '/admin'
    })
  }

  items.push(
    {
      collapse: true
    },
    {
      divider: true
    },
    {
      label: 'Sign Out',
      icon: <FiLogOut size="1.1rem" />,
      onClick: onLogout
    }
  )

  return items
}

/**
 * Generates an array of menu items based on the provided dropdown items, current pathname, and selected link style.
 *
 * @param dropdownItems - The array of dropdown items.
 * @param pathname - The current pathname.
 * @param selectedLinkStyle - The selected link style to apply.
 * @returns An array of dropdown menu items.
 */
export const getDropdownMenuItems = (dropdownItems: DropdownItem[]) =>
  dropdownItems.map((item, index) => {
    if (itemIsDivider(item)) {
      // eslint-disable-next-line react/no-array-index-key
      return <Menu.Divider key={index} />
    }

    if (itemIsCollapse(item)) {
      return null
    }

    const { label, icon, url, external, hidden, onClick } = item

    if (!hidden && url) {
      if (external) {
        return (
          <Menu.Item
            key={label}
            icon={icon}
            component="a"
            href={url}
            target="_blank"
            rel="noreferrer"
          >
            {label}
          </Menu.Item>
        )
      }

      return (
        <Menu.Item key={label} icon={icon} component={NavLink} to={url} css={S.navLink}>
          {label}
        </Menu.Item>
      )
    }

    if (!hidden) {
      return (
        <Menu.Item key={label} icon={icon} onClick={onClick}>
          {label}
        </Menu.Item>
      )
    }

    return null
  })

/**
 * Checks if the given dropdown item is a divider.
 * @param item - The dropdown item to check.
 * @returns `true` if the item is a divider, `false` otherwise.
 */
export const itemIsDivider = (item: DropdownItem): item is { divider: true } => 'divider' in item

/**
 * Checks if the given dropdown item is collapsed.
 * @param item - The dropdown item to check.
 * @returns `true` if the item is collapsed, `false` otherwise.
 */
export const itemIsCollapse = (item: DropdownItem): item is { collapse: true } => 'collapse' in item
