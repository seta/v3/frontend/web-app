export type MenuItem = {
  to: string
  label: string
  hidden?: boolean
  collapse?: boolean
  tabId?: string
}

export type DropdownItem =
  | {
      label: string
      icon?: JSX.Element
      url?: string
      external?: boolean
      hidden?: boolean
      onClick?: () => void
    }
  | { divider: true }
  | { collapse: true }

export type GetAboutDropdownArgs = {
  onTakeTour: () => void
}

export type DropdownProps = {
  isAdmin?: boolean
  onLogout: () => void
}
