import { useState } from 'react'
import { Group, Menu, UnstyledButton, Text } from '@mantine/core'
import { IconChevronDown } from '@tabler/icons-react'

import * as S from '~/components/Header/styles'
import { getDropdownAbout, getDropdownMenuItems } from '~/components/Header/utils/menu-utils'

import '~/components/Header/style.css'
import { useTourContext } from '~/contexts/tour-context'

const AboutDropdown = () => {
  const [isOpen, setIsOpen] = useState(false)

  const { openTour } = useTourContext()

  const handleToggle = () => {
    setIsOpen(current => !current)
  }

  const handleMenuClose = () => {
    setIsOpen(false)
  }

  const handleOpenTour = () => {
    openTour()
  }

  const aboutDropdown = getDropdownAbout({ onTakeTour: handleOpenTour })

  const aboutDropdownItems = getDropdownMenuItems(aboutDropdown)

  return (
    <Menu
      shadow="md"
      width={200}
      position="bottom-start"
      id="about"
      onClose={handleMenuClose}
      opened={isOpen}
    >
      <Menu.Target>
        <UnstyledButton css={S.button} onClick={handleToggle}>
          <Group>
            <Text size="md">About</Text>
            <IconChevronDown css={S.chevron} data-open={isOpen} size="1rem" />
          </Group>
        </UnstyledButton>
      </Menu.Target>

      <Menu.Dropdown css={S.aboutDropdown}>{aboutDropdownItems}</Menu.Dropdown>
    </Menu>
  )
}

export default AboutDropdown
