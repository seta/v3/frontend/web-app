import type { ReactElement, MouseEvent } from 'react'
import { useEffect, useState } from 'react'
import type { DefaultMantineColor } from '@mantine/core'
import {
  Transition,
  Button,
  Indicator,
  Box,
  Collapse,
  Flex,
  Text,
  ThemeIcon,
  UnstyledButton,
  Tooltip
} from '@mantine/core'
import { IconChevronRight } from '@tabler/icons-react'

import { useSidebarState } from '~/contexts/sidebar-state-context'
import type { ClassAndChildrenProps } from '~/types/children-props'

import * as S from './styles'

export type ToggleSectionProps = ClassAndChildrenProps & {
  icon?: ReactElement
  color?: DefaultMantineColor
  marker?: DefaultMantineColor | boolean | null
  title: string
  open?: boolean
  disabled?: boolean
  action?: {
    label: string
    color?: DefaultMantineColor
    icon?: ReactElement
    disabled?: boolean
    hidden?: boolean
    onClick: () => void
  }
}

const ToggleSection = ({
  className,
  children,
  icon,
  color,
  title,
  marker,
  open,
  disabled,
  action
}: ToggleSectionProps) => {
  const [isOpen, setIsOpen] = useState(open ?? false)

  const { sidebarCollapsed, setSidebarExpanded } = useSidebarState()

  const openValue = !disabled && isOpen

  useEffect(() => {
    if (sidebarCollapsed) {
      setIsOpen(false)
    }
  }, [sidebarCollapsed])

  const handleToggle = () => {
    setIsOpen(current => !current)

    // Expand the sidebar if it's collapsed
    if (sidebarCollapsed) {
      setSidebarExpanded()
    }
  }

  const handleAction = (e: MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation()
    action?.onClick()
  }

  const withMarker = (enabled: boolean, content: JSX.Element) => {
    if (!marker || !enabled) {
      return content
    }

    const markerColor: DefaultMantineColor = typeof marker === 'boolean' ? 'blue' : marker

    return (
      <Indicator css={S.marker} color={markerColor} size={8}>
        {content}
      </Indicator>
    )
  }

  const buttonStyle = [S.button, sidebarCollapsed && S.buttonCollapsed]
  const actionButtonStyle = [S.actionButton, sidebarCollapsed && S.actionButtonHidden]

  const actionButton = (
    <Transition
      mounted={!!action && !action.hidden && openValue}
      transition="pop"
      exitDuration={100}
    >
      {style => (
        <Button
          style={style}
          variant="subtle"
          css={actionButtonStyle}
          color={action?.color}
          leftIcon={action?.icon}
          disabled={action?.disabled}
          onClick={handleAction}
        >
          {action?.label}
        </Button>
      )}
    </Transition>
  )

  return (
    <Box className={className} css={S.root} data-open={openValue}>
      <Tooltip label={title} disabled={!sidebarCollapsed} position="right" withinPortal>
        <UnstyledButton
          css={buttonStyle}
          onClick={handleToggle}
          data-open={openValue}
          disabled={disabled}
        >
          <Flex direction="row" justify="space-between" gap={0}>
            <Flex align="center">
              {withMarker(
                sidebarCollapsed,
                <ThemeIcon
                  variant={openValue || sidebarCollapsed ? 'filled' : 'outline'}
                  color={color}
                >
                  {icon}
                </ThemeIcon>
              )}

              {withMarker(
                !sidebarCollapsed,
                <Text fz="md" fw={500} ml="md" data-title>
                  {title}
                </Text>
              )}
            </Flex>

            <IconChevronRight
              css={S.chevron}
              size="1.5rem"
              stroke={1.5}
              data-open={openValue}
              data-chevron
            />
          </Flex>
        </UnstyledButton>
      </Tooltip>

      <Collapse css={S.content} in={openValue}>
        {children}
      </Collapse>

      {actionButton}
    </Box>
  )
}

export default ToggleSection
