export type AuthKey = {
  publicKey: string
}

export type AccessTokenRequest = { expires: Date }

export type AccessTokenResponse = {
  accessToken: string
}

export type ApplicationAccessTokenRequest = AccessTokenRequest & { name: string }
