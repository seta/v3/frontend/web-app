import { useQuery } from '@tanstack/react-query'
import type { AxiosRequestConfig } from 'axios'

import api from '~/api'
import type { RelatedTerm } from '~/types/search/related-term'

const RELATED_CLUSTERS_API_PATH = '/ontology-list'

export type RelatedClustersResponse = {
  nodes: RelatedTerm[][]
}

type Params = {
  term: string | undefined
}

export const queryKey = {
  root: 'related-clusters',
  params: ({ term }: Params) => [queryKey.root, term]
}

const getRelatedClusters = async (
  { term }: Params,
  config?: AxiosRequestConfig
): Promise<RelatedClustersResponse> => {
  if (!term) {
    return { nodes: [] }
  }

  const { data } = await api.get<RelatedClustersResponse>(
    `${RELATED_CLUSTERS_API_PATH}?term=${term}`,
    config
  )

  return data
}

export const useRelatedClusters = (params: Params) =>
  useQuery({
    queryKey: queryKey.params(params),
    // Passing the `signal` makes the request cancelable
    queryFn: ({ signal }) => getRelatedClusters(params, { signal })
  })
