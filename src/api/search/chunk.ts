import type { UseQueryOptions } from '@tanstack/react-query'
import { useQuery } from '@tanstack/react-query'
import type { AxiosRequestConfig } from 'axios'

import api from '~/api'
import type { Document } from '~/types/search/documents'

const DOCUMENT_CHUNKS_API_PATH = '/chunks'

type GetChunkPayload = {
  id: string
  source: string
}

// Mimic the response of the API where an empty object is returned if the document is not found,
// keeping it easy to check on the TS side
type GetChunkResponse = Document | { _id: undefined }

export const getChunkQueryKey = {
  root: 'chunk',
  chunk: (id: string, source: string) => [getChunkQueryKey.root, { source, id }]
}

export const getChunk = async (
  { id, source }: GetChunkPayload,
  config?: AxiosRequestConfig
): Promise<GetChunkResponse> => {
  const { data } = await api.get<GetChunkResponse>(
    `${DOCUMENT_CHUNKS_API_PATH}/${source}/${id}`,
    config
  )

  return data
}

type UseChunkOptions = UseQueryOptions<GetChunkResponse>

export const useChunk = (id: string, source: string, options?: UseChunkOptions) =>
  useQuery({
    queryKey: getChunkQueryKey.chunk(id, source),

    queryFn: ({ signal }) => getChunk({ id, source }, { signal }),
    ...options
  })
