import type { QueryKey } from '@tanstack/react-query'
import { useMutation, useQueryClient, useQuery } from '@tanstack/react-query'
import type { AxiosRequestConfig } from 'axios'

import api from '~/api'
import { environment } from '~/environments/environment'
import { SavedSearchType } from '~/types/saved-search/saved-search'
import type { SavedSearch, SavedSearchCreate } from '~/types/saved-search/saved-search'
import { getSaveSearchPayload } from '~/utils/saved-search-utils'

const defaultConfig: AxiosRequestConfig = {
  baseURL: environment.baseUrl
}

const SAVED_SEARCH_API_PATH = '/me/searches'

export type SavedSearchResponse = {
  items: SavedSearch[]
}

export const savedSearchQueryKey: QueryKey = ['saved-search']

/* GET ALL SAVED SEARCHES */

const getSavedSearches = async (config?: AxiosRequestConfig): Promise<SavedSearchResponse> => {
  const { data } = await api.get<SavedSearchResponse>(SAVED_SEARCH_API_PATH, {
    ...defaultConfig,
    ...config
  })

  return data
}

export const useSavedSearches = () => {
  return useQuery({
    queryKey: savedSearchQueryKey,
    queryFn: getSavedSearches
  })
}

/* SAVE SEARCH */

const saveSearch = async (value: SavedSearchCreate, config?: AxiosRequestConfig): Promise<void> => {
  const payload = getSaveSearchPayload(value)

  await api.post<SavedSearchCreate>(SAVED_SEARCH_API_PATH, payload, {
    ...defaultConfig,
    ...config
  })
}

export const useSaveSearchMutation = () => {
  const queryClient = useQueryClient()

  return useMutation({
    mutationFn: saveSearch,

    onSuccess: async () => {
      await queryClient.invalidateQueries({ queryKey: savedSearchQueryKey })
    }
  })
}

/* DELETE SEARCH */

const deleteSearch = async (id: string, config?: AxiosRequestConfig): Promise<void> => {
  await api.delete(`${SAVED_SEARCH_API_PATH}/${id}`, {
    ...defaultConfig,
    ...config
  })
}

export const useDeleteSearchMutation = () => {
  const queryClient = useQueryClient()

  return useMutation({
    mutationFn: deleteSearch,

    onSuccess: async () => {
      await queryClient.invalidateQueries({ queryKey: savedSearchQueryKey })
    }
  })
}

/* CREATE NEW GROUP */

const createNewGroup = async (title: string, config?: AxiosRequestConfig): Promise<SavedSearch> => {
  const newGroup: SavedSearchCreate = {
    type: SavedSearchType.Folder,
    parentId: null,
    name: title
  }

  const { data } = await api.post<SavedSearch>(SAVED_SEARCH_API_PATH, newGroup, {
    ...defaultConfig,
    ...config
  })

  return data
}

export const useCreateGroupMutation = () => {
  const queryClient = useQueryClient()

  return useMutation({
    mutationFn: createNewGroup,

    onSuccess: async () => {
      await queryClient.invalidateQueries({ queryKey: savedSearchQueryKey })
    }
  })
}
