import type { UseQueryOptions } from '@tanstack/react-query'
import { useQuery } from '@tanstack/react-query'
import type { AxiosRequestConfig } from 'axios'

import api from '~/api'
import type { DocumentsPayload } from '~/api/search/documents'
import type { EmbeddingInfo } from '~/types/embeddings'
import type { Aggregations } from '~/types/search/aggregations'
import { AggregationType } from '~/types/search/aggregations'
import { SearchType } from '~/types/search/common'
import { getEmbeddingsVectors, getSourceAndSemanticIds } from '~/utils/search-utils'

const AGGREGATIONS_API_PATH = '/aggregations'

export type AggregationsPayload = Omit<DocumentsPayload, 'from_doc' | 'n_docs'> & {
  aggs?: AggregationType[]
}

export type AggregationsResponse = {
  aggregations: Aggregations
}

export const queryKey = {
  root: 'aggregations',
  // We need this many params to compose the query key
  // eslint-disable-next-line max-params
  docs: (
    query: string | undefined,
    embeddings: { name: string; chunks: number }[] | undefined,
    source: string[] | undefined,
    semanticIds: string[] | undefined,
    searchOptions?: AggregationsPayload
  ) => [queryKey.root, { query }, { embeddings }, { source, semanticIds }, { searchOptions }]
}

const getAggregations = async (payload: AggregationsPayload, config?: AxiosRequestConfig) => {
  const defaultPayload: AggregationsPayload = {
    aggs: [
      AggregationType.DateYear,
      AggregationType.CollectionReference
      // TODO: Re-enable this when the taxonomy aggregations are fixed
      // AggregationType.Taxonomies
    ],
    search_type: SearchType.CHUNK_SEARCH
  }

  const { data } = await api.post<AggregationsResponse, AggregationsPayload>(
    AGGREGATIONS_API_PATH,
    {
      ...defaultPayload,
      ...payload
    },
    config
  )

  return data
}

type UseAggregationsOptions = UseQueryOptions<AggregationsResponse> & {
  searchOptions?: AggregationsPayload
}

export const useAggregations = (
  query: string | undefined,
  embeddings: EmbeddingInfo[] | undefined,
  { searchOptions, ...options }: UseAggregationsOptions
) => {
  const vectors = getEmbeddingsVectors(embeddings)
  const [source, semanticIds] = getSourceAndSemanticIds(embeddings)

  const embeddingsKey = embeddings?.map(({ name, chunks }) => ({
    name,
    chunks: chunks?.length ?? 0
  }))

  return useQuery({
    // The vectors in the query are identified by the `embeddingsKey` array
    // eslint-disable-next-line @tanstack/query/exhaustive-deps
    queryKey: queryKey.docs(query, embeddingsKey, source, semanticIds, searchOptions),
    refetchOnWindowFocus: false,
    staleTime: 5 * 60 * 1000, // 5 minutes

    queryFn: ({ signal }) =>
      getAggregations(
        {
          term: query || undefined,
          source,
          sbert_embedding_list: vectors,
          semantic_sort_id_list: semanticIds,
          ...searchOptions
        },
        { signal }
      ),

    ...options
  })
}
