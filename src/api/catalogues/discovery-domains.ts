import { useQuery } from '@tanstack/react-query'
import type { AxiosRequestConfig } from 'axios'

import { environment } from '~/environments/environment'
import type {
  DiscoveryDomainsCatalogue,
  DiscoveryDomain
} from '~/types/catalogue/catalogue-discovery-domains'

import api from '../api'

const DISCOVERY_DOMAINS_API_PATH = '/catalogue/discovery-domains'

export const cacheKey = () => [DISCOVERY_DOMAINS_API_PATH]

const getCatalogueDiscoveryDomains = async (
  config?: AxiosRequestConfig
): Promise<DiscoveryDomain[]> => {
  const { data } = await api.get<DiscoveryDomainsCatalogue>(DISCOVERY_DOMAINS_API_PATH, {
    baseURL: environment.baseUrl,
    ...config
  })

  return data['domains']
}

export const useCatalogueDiscoveryDomains = () =>
  useQuery({
    queryKey: cacheKey(),
    queryFn: ({ signal }) => getCatalogueDiscoveryDomains({ signal })
  })
