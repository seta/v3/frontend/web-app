import { useMutation } from '@tanstack/react-query'
import type { AxiosRequestConfig } from 'axios'

import api from '~/api/api'
import { environment } from '~/environments/environment'

import type { AccessTokenRequest, AccessTokenResponse } from '../types/auth-keys-types'

const GPT_TOKENS_PATH = '/me/rag-tokens'

const apiConfig: AxiosRequestConfig = {
  baseURL: environment.baseUrl
}

const getFormattedDate = (date: Date): string => {
  return new Date(date.getTime() - date.getTimezoneOffset() * 60000).toISOString().split('T')[0]
}

const createAccessToken = async (request: AccessTokenRequest): Promise<AccessTokenResponse> => {
  const formattedDate = getFormattedDate(request.expires)

  const { data } = await api.post<AccessTokenResponse>(
    GPT_TOKENS_PATH,
    {
      expires: formattedDate
    },
    apiConfig
  )

  return data
}

export const useCreateAccessToken = () => {
  return useMutation({
    mutationFn: (request: AccessTokenRequest) => createAccessToken(request)
  })
}
