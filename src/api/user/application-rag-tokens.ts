import { useMutation } from '@tanstack/react-query'
import type { AxiosRequestConfig } from 'axios'

import api from '~/api/api'
import { environment } from '~/environments/environment'

import type { ApplicationAccessTokenRequest, AccessTokenResponse } from '../types/auth-keys-types'

const GPT_TOKENS_PATH = (name?: string): string => `/me/apps/${name}/rag-tokens`

const apiConfig: AxiosRequestConfig = {
  baseURL: environment.baseUrl
}

const getFormattedDate = (date: Date): string => {
  return new Date(date.getTime() - date.getTimezoneOffset() * 60000).toISOString().split('T')[0]
}

const createAccessToken = async (
  request: ApplicationAccessTokenRequest
): Promise<AccessTokenResponse> => {
  const formattedDate = getFormattedDate(request.expires)

  const { data } = await api.post<AccessTokenResponse>(
    GPT_TOKENS_PATH(request.name),
    {
      expires: formattedDate
    },
    apiConfig
  )

  return data
}

export const useCreateAccessToken = () => {
  return useMutation({
    mutationFn: (request: ApplicationAccessTokenRequest) => createAccessToken(request)
  })
}
