import type { ReactNode } from 'react'

// TODO: Move these types out of SearchPageNew
import type { FiltersStateBase } from '~/pages/SearchPageNew/types/filters-reducer'
import type { SearchValue } from '~/pages/SearchPageNew/types/search'

export enum SavedSearchType {
  Folder = 0,
  Search = 1
}

export type SerializedSearch = {
  search: SearchValue
  filters: FiltersStateBase
}

export type SavedSearchCreate = {
  type: SavedSearchType
  parentId?: string | null
  name?: string
  search?: SerializedSearch
}

export type SavedSearchUpdate = SavedSearchCreate & {
  id: string
}

export type SearchItem = SavedSearchUpdate & {
  type: SavedSearchType.Search
}

export type SearchGroup = SavedSearchUpdate & {
  type: SavedSearchType.Folder
  children: SavedSearch[]
}

export type SearchGroupWithIcon = SearchGroup & {
  icon: ReactNode
}

export type SavedSearch = SearchItem | SearchGroup
