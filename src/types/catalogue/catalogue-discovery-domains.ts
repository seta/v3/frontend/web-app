export type DiscoveryDomain = {
  code: string
  description: string
}

export type DiscoveryDomainsCatalogue = {
  domains: DiscoveryDomain[]
}
