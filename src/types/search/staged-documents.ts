import type { DocumentBrief } from '~/types/search/documents'

export type StagedDocument = {
  id: string
  title: string
  source: string
  link?: string | null
  originalDocument?: DocumentBrief
}
