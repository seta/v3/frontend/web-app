export enum SidebarState {
  COLLAPSED = 'collapsed',
  EXPANDED = 'expanded'
}
