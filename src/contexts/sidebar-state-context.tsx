import type { Dispatch, SetStateAction } from 'react'
import { createContext, useContext } from 'react'

import type { ChildrenProp } from '~/types/children-props'
import { SidebarState } from '~/types/sidebar'

type SidebarStateProviderProps = {
  sidebarState: SidebarState
  setSidebarState: Dispatch<SetStateAction<SidebarState>>
  toggleSidebarCollapsed: () => void
}

type SidebarStateContextProps = SidebarStateProviderProps & {
  sidebarCollapsed: boolean
  setSidebarCollapsed: () => void
  setSidebarExpanded: () => void
}

const SidebarStateContext = createContext<SidebarStateContextProps | undefined>(undefined)

export const SidebarStateProvider = ({
  sidebarState,
  setSidebarState,
  toggleSidebarCollapsed,
  children
}: SidebarStateProviderProps & ChildrenProp) => {
  const value: SidebarStateContextProps = {
    sidebarState,
    sidebarCollapsed: sidebarState === SidebarState.COLLAPSED,
    setSidebarState,
    toggleSidebarCollapsed,
    setSidebarCollapsed: () => setSidebarState(SidebarState.COLLAPSED),
    setSidebarExpanded: () => setSidebarState(SidebarState.EXPANDED)
  }

  return <SidebarStateContext.Provider value={value}>{children}</SidebarStateContext.Provider>
}

export const useSidebarState = () => {
  const context = useContext(SidebarStateContext)

  if (!context) {
    throw new Error('useSidebarState must be used within a SidebarStateProvider')
  }

  return context
}
