import { createContext, useContext } from 'react'

import type { ChildrenProp } from '~/types/children-props'

type PopupContextProps = {
  closePopup: () => void
}

const StagedPopupContext = createContext<PopupContextProps | undefined>(undefined)

export const PopupProvider = ({ children, ...props }: PopupContextProps & ChildrenProp) => {
  return <StagedPopupContext.Provider value={props}>{children}</StagedPopupContext.Provider>
}

export const usePopup = () => {
  const context = useContext(StagedPopupContext)

  if (!context) {
    throw new Error('usePopup must be used within a PopupProvider or a PopupOverlay component')
  }

  return context
}
