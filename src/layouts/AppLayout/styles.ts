import { css } from '@emotion/react'

export const container = css`
  display: grid;
  flex-grow: 1;
`
