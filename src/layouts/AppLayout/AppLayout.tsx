import { useEffect } from 'react'
import { Flex } from '@mantine/core'
import { Outlet, useSearchParams } from 'react-router-dom'

import Footer from '~/components/Footer/Footer'
import Header from '~/components/Header'
import Tour from '~/components/Tour/Tour'

import * as S from './styles'

const ACTION_PARAM = 'action'

const AppLayout = () => {
  const [searchParams, setSearchParams] = useSearchParams()

  useEffect(() => {
    if (searchParams.has(ACTION_PARAM)) {
      setSearchParams(prev => {
        prev.delete(ACTION_PARAM)

        return prev
      })
    }
  }, [searchParams, setSearchParams])

  return (
    <Flex direction="column" className="min-h-screen">
      <Header />

      <div css={S.container}>
        <Outlet />
      </div>

      <Tour />
      <Footer />
    </Flex>
  )
}

export default AppLayout
