export enum ActiveLink {
  DASHBOARD,
  AUTHKEY,
  APPLICATIONS,
  PERMISSIONS,
  ACCESS_TOKENS,
  CLOSE,
  NONE
}

export const getActiveLink = (path: string): ActiveLink => {
  switch (path) {
    case '/profile':
      return ActiveLink.DASHBOARD

    case path.startsWith('/profile/permissions') ? path : '':
      return ActiveLink.PERMISSIONS

    case path.startsWith('/profile/auth-key') ? path : '':
      return ActiveLink.AUTHKEY

    case path.startsWith('/profile/applications') ? path : '':
      return ActiveLink.APPLICATIONS

    case path.startsWith('/profile/close-account') ? path : '':
      return ActiveLink.CLOSE

    case path.startsWith('/profile/access-tokens') ? path : '':
      return ActiveLink.ACCESS_TOKENS

    default:
      return ActiveLink.NONE
  }
}
